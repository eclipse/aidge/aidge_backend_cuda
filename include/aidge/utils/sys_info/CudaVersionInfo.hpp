#ifndef AIDGE_UTILS_SYS_INFO_CUDA_VERSION_INFO_H
#define AIDGE_UTILS_SYS_INFO_CUDA_VERSION_INFO_H

#include "aidge/backend/cuda/utils/CudaUtils.hpp"  // CHECK_CUDA_STATUS
#include "aidge/utils/Log.hpp"
#include "aidge/backend/cuda_version.h"
namespace Aidge {

constexpr inline const char * getBackendCudaProjectVersion(){
    return PROJECT_VERSION;
}

constexpr inline const char * getBackendCudaGitHash(){
    return PROJECT_GIT_HASH;
}

void showBackendCudaProjectVersion() {
    Log::info("Aidge backend CUDA: {} ({}), {} {}", getBackendCudaProjectVersion(), getBackendCudaGitHash(), __DATE__, __TIME__);
    Log::info("CUDA compiler version: {}", CUDA_COMPILER_VERSION);
    Log::info("CuDNN version: {}.{}.{}\n", CUDNN_MAJOR, CUDNN_MINOR,
              CUDNN_PATCHLEVEL);

    int deviceCount = 0;
    CHECK_CUDA_STATUS(cudaGetDeviceCount(&deviceCount));

    if (deviceCount == 0) {
        Log::warn("There are no available device(s) that support CUDA");
    } else {
        Log::info("Detected {} CUDA Capable device(s)", deviceCount);
    }

    for (int dev = 0; dev < deviceCount; ++dev) {
        cudaSetDevice(dev);
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, dev);

        Log::info("\nDevice #{}: \"{}\"", dev, deviceProp.name);

        int driverVersion = 0;
        int runtimeVersion = 0;
        cudaDriverGetVersion(&driverVersion);
        cudaRuntimeGetVersion(&runtimeVersion);
        Log::info(
            "\tCUDA Driver Version / Runtime Version:          {}.{} / {}.{}",
            (driverVersion / 1000), ((driverVersion % 100) / 10),
            (runtimeVersion / 1000), ((runtimeVersion % 100) / 10));
        Log::info("\tCUDA Capability Major/Minor version number:     {}.{}",
                  deviceProp.major, deviceProp.minor);
    }
}
}  // namespace Aidge
#endif  // AIDGE_UTILS_SYS_INFO_CUDA_VERSION_INFO_H
