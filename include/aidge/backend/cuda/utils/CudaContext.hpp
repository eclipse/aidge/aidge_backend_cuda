#ifndef AIDGE_BACKEND_CUDA_CUDA_CONTEXT_H
#define AIDGE_BACKEND_CUDA_CUDA_CONTEXT_H

#include <vector>

#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
class CudaContext {
public:
    static int nbDevice(){
        int count = 1;
        CHECK_CUDA_STATUS(cudaGetDeviceCount(&count));
        return count;
    }
    static void setDevice(int device = -1)
    {
        static int prevDevice = 0;

        if (device >= 0)
            prevDevice = device;
        else
            device = prevDevice;

        CHECK_CUDA_STATUS(cudaSetDevice(device));
    }

    static std::pair<size_t, size_t> getMemInfo(){
        size_t free;
        size_t total;
        CHECK_CUDA_STATUS(cudaMemGetInfo (&free, &total));
        return std::make_pair(free, total);
    }
    

    static int getDevice(){
        int dev;
        CHECK_CUDA_STATUS(cudaGetDevice(&dev));
        return dev;
    }

    static const cudaDeviceProp& getDeviceProp()
    {
        static std::vector<cudaDeviceProp> deviceProp;
        static std::vector<bool> init;

        if (deviceProp.empty()) {
//#pragma omp critical(CudaContext__getDeviceProp)
            if (deviceProp.empty()) {
                int count = 1;
                CHECK_CUDA_STATUS(cudaGetDeviceCount(&count));

                deviceProp.resize(count);
                init.resize(count, false);
            }
        }

        int dev;
        CHECK_CUDA_STATUS(cudaGetDevice(&dev));

        if (!init[dev]) {
            CHECK_CUDA_STATUS(cudaGetDeviceProperties(&deviceProp[dev], dev));
            init[dev] = true;
        }

        return deviceProp[dev];
    }

    // Declare cublas handle
    static cublasHandle_t& cublasHandle()
    {
        static std::vector<cublasHandle_t> cublas_h;

        if (cublas_h.empty()) {
//#pragma omp critical(CudaContext__cublasHandle)
            if (cublas_h.empty()) {
                int count = 1;
                CHECK_CUDA_STATUS(cudaGetDeviceCount(&count));

                cublas_h.resize(count, NULL);
            }
        }

        int dev;
        CHECK_CUDA_STATUS(cudaGetDevice(&dev));

        if (cublas_h[dev] == NULL) {
            CHECK_CUBLAS_STATUS(cublasCreate(&cublas_h[dev]));
            fmt::print("CUBLAS initialized on device #{}\n", dev);
        }

        return cublas_h[dev];
    }

    // Declare cudnn handle
    static cudnnHandle_t& cudnnHandle()
    {
        static std::vector<cudnnHandle_t> cudnn_h;

        if (cudnn_h.empty()) {
//#pragma omp critical(CudaContext__cudnnHandle)
            if (cudnn_h.empty()) {
                int count = 1;
                CHECK_CUDA_STATUS(cudaGetDeviceCount(&count));

                cudnn_h.resize(count, NULL);
            }
        }

        int dev;
        CHECK_CUDA_STATUS(cudaGetDevice(&dev));

        if (cudnn_h[dev] == NULL) {
            CHECK_CUDNN_STATUS(cudnnCreate(&cudnn_h[dev]));
            fmt::print("CUDNN initialized on device #{}\n", dev);
        }

        return cudnn_h[dev];
    }

    template <class T>
    struct data_type {
        static const cudnnDataType_t value = CUDNN_DATA_FLOAT;
                                            // Dummy value by default
    };
};
}

namespace Aidge {
    template <>
    struct CudaContext::data_type<half_float::half> {
        static const cudnnDataType_t value = CUDNN_DATA_HALF;
    };

    template <>
    struct CudaContext::data_type<float> {
        static const cudnnDataType_t value = CUDNN_DATA_FLOAT;
    };

    template <>
    struct CudaContext::data_type<double> {
        static const cudnnDataType_t value = CUDNN_DATA_DOUBLE;
    };

    inline cudnnDataType_t DataTypeToCudnn(DataType type) {
        switch (type) {
        case DataType::Float64:
            return CUDNN_DATA_DOUBLE;
        case DataType::Float32:
            return CUDNN_DATA_FLOAT;
        case DataType::Float16:
            return CUDNN_DATA_HALF;
        case DataType::Int8:
            return CUDNN_DATA_INT8;
        case DataType::UInt8:
            return CUDNN_DATA_UINT8;
        case DataType::Int32:
            return CUDNN_DATA_INT32;
#if CUDNN_VERSION >= 8100
        case DataType::Int64:
            return CUDNN_DATA_INT64;
#endif
        default:
            assert(false && "Unsupported CuDNN type");
        }

        return CUDNN_DATA_FLOAT;  // TODO: undefined behavior
    }
}

#endif // AIDGE_BACKEND_CUDA_CUDA_CONTEXT_H
