/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_ADDIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_ADDIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class AddImpl_cuda : public OperatorImpl {
public:
    AddImpl_cuda(const Add_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<AddImpl_cuda> create(const Add_Op& op) {
        return std::make_unique<AddImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;
    ~AddImpl_cuda();

private:
    std::vector<cudnnTensorDescriptor_t> mTensorDesc;
    cudnnReduceTensorDescriptor_t mBwdReduceDesc = nullptr;
    size_t mBwdWorkspaceSize = 0;
    void* mBwdWorkspace = nullptr;
    std::vector<std::shared_ptr<Tensor>> mInputFallbacks;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const std::vector<std::reference_wrapper<Tensor>>& inputs);
    template <class T> void backward_(const Tensor& outGrad);
};

// Implementation entry point registration to Operator
REGISTRAR(Add_Op, "cuda", Aidge::AddImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_ADDIMPL_H_ */
