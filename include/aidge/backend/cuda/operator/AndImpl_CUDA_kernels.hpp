/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_ANDIMPL_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_ANDIMPL_KERNELS_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {

template <class T>
void AndForward(const T* input1, const T* input2, T* output,
                const std::vector<int>& input1Dims,const std::vector<int>& input2Dims,
                const std::vector<int>& inputStrides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                int outSize);
}
#endif /* AIDGE_CUDA_OPERATOR_ANDIMPL_KERNELS_H_ */





