/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_SHIFTMAXIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_SHIFTMAXIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ShiftMax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
class ShiftMaxImpl_cuda : public OperatorImpl {
public:
    ShiftMaxImpl_cuda(const ShiftMax_Op &op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<ShiftMaxImpl_cuda> create(const ShiftMax_Op &op) {
        return std::make_unique<ShiftMaxImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;

private:
    std::shared_ptr<Tensor> mInputFallback;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& output_grad);
    
};

// Implementation entry point registration to Operator
REGISTRAR(ShiftMax_Op, "cuda", Aidge::ShiftMaxImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_SHIFTMAXIMPL_H_ */
