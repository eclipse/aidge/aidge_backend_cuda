/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_ARGMAXIMPL_KERNEL_H_
#define AIDGE_CUDA_OPERATOR_ARGMAXIMPL_KERNEL_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge
{
    template <class T>
    void ArgMax_cuda_forward_kernel(const T* input, T* output,
                                    const std::vector<int>& inputDims, const std::vector<int>& inputStrides,
                                    int axis, int total_elems, std::size_t selectLastIdx);
}
#endif /* AIDGE_CUDA_OPERATOR_ARGMAXIMPL_KERNEL_H_ */