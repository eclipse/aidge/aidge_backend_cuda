/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_CUBLAS_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_CUBLAS_KERNELS_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {

template <class T>
cublasStatus_t cublasGemm(cublasHandle_t handle,
                          cublasOperation_t transa, cublasOperation_t transb,
                          int m, int n, int k,
                          const T *alpha,
                          const T *A, int lda,
                          const T *B, int ldb,
                          const T *beta,
                          T *C, int ldc);

template <class T>
cublasStatus_t cublasGemmStridedBatched(cublasHandle_t handle,
                          cublasOperation_t transa, cublasOperation_t transb,
                          int m, int n, int k,
                          const T *alpha,
                          const T *A, int lda, long long int strideA,
                          const T *B, int ldb, long long int strideB,
                          const T *beta,
                          T *C, int ldc, long long int strideC,
                          int batchCount);

template <class T>
cublasStatus_t cublasGemv(cublasHandle_t handle, cublasOperation_t trans,
                          int m, int n,
                          const T  *alpha,
                          const T *A, int lda,
                          const T *x, int incx,
                          const T *beta,
                          T *y, int incy);
}
#endif /* AIDGE_CUDA_OPERATOR_CUBLAS_KERNELS_H_ */