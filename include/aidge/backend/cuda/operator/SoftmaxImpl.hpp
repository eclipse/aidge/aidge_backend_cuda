/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_SOFTMAXIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_SOFTMAXIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class SoftmaxImpl_cuda : public OperatorImpl {
public:
    SoftmaxImpl_cuda(const Softmax_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<SoftmaxImpl_cuda> create(const Softmax_Op& op) {
        return std::make_unique<SoftmaxImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;

private:
    // CuDNN specific variables
    std::shared_ptr<Tensor> mInputFallback, mOutputGradFallback;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& output_grad, int axis);
};

// Implementation entry point registration to Operator
REGISTRAR(Softmax_Op, "cuda", Aidge::SoftmaxImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_SOFTMAXIMPL_H_ */
