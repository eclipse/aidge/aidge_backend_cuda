/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_PADIMPL_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_PADIMPL_KERNELS_H_

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge
{

    template <class T>
    void cudaPadding(const cudaDeviceProp &deviceProp,
                     unsigned int nbOutputs,
                     unsigned int outputsWidth,
                     unsigned int outputsHeight,
                     unsigned int nbChannels,
                     unsigned int batchSize,
                     unsigned int inputWidth,
                     unsigned int inputHeight,
                     int leftPad,
                     int topPad,
                     unsigned int padType,
                     T padValue,
                     const T *input,
                     T *outputs,
                     const T alpha,
                     const T beta);
}
#endif /* AIDGE_CUDA_OPERATOR_PADIMPL_KERNELS_H_ */