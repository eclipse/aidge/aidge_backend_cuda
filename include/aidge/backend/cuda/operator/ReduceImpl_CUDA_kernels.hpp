/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_REDUCEIMPL_KERNEL_H_
#define AIDGE_CUDA_OPERATOR_REDUCEIMPL_KERNEL_H_

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge
{

    template <class T>
    void ReduceBackward(const T* input,
                                   T* output,
                                   const std::vector<std::size_t>& inputDims,
                                   const std::vector<std::size_t>& outputDims,
                                   const std::vector<int>& axes,
                                   const std::vector<std::size_t>& factors,
                                   int outSize,
                                   const T alpha,
                                   const T beta);
}
#endif /* AIDGE_CUDA_OPERATOR_REDUCEIMPL_KERNEL_H_ */