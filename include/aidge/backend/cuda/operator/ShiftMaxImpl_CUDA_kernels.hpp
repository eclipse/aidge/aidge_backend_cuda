/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_SHIFTMAXIMPL_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_SHIFTMAXIMPL_KERNELS_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {

/**
    * @brief Compute the forward for ShiftMax
    * @param input: Input tensor
    * @param quantized_tensor: Quantized output tensor
    * @param factor: Pointer to an empty memory block allocated on the GPU (just use for computation)
    * @param dims: Dimensions of input tensor
    * @param SF: Scaling factor of input tensor
    * @param N: Arithmetic precision, currently set at 15 like I-ViT (the greater the N, the more precise the operation, but the greater the number of bits required)
    * @param output_bits: Desired bit precision (8 for int8, for example)
    * @param new_SF: Scaling factor of output that can be use to dequantify
*/
template <class T>
__global__ void ShiftMaxforward_(T* input,int* quantized_tensor,int* factor, int* dims, double SF, int N, int output_bits,double new_SF);

/**
    * @brief Wrapper function to execute ShiftMaxforward_
    * @note Output correspond to the non-quantized tensor, to obtain the quantized tensor we need to copy quantized_tensor and not input_cuda_tensor
    * @param input: Input tensor
    * @param output: Output tensor (not quantized)
    * @param SF: Scaling factor of input tensor
    * @param N: Arithmetic precision, currently set at 15 like I-ViT (the greater the N, the more precise the operation, but the greater the number of bits required)
    * @param output_bits: Desired bit precision (8 for int8, for example)
    * @param size: Number of elements in the input tensor
    * @param dims_input: Dimensions of input tensor
*/
template <class T>
void ShiftMaxforward(const T* input, T* output, double SF,int N, int output_bits, size_t size, std::vector<long unsigned int> dims_input);

/**
    * @brief Compute the backward for ShiftMax
    * @param input_grad: Gradient of input tensor (that we want to obtain)
    * @param output_tensor: Output tensor obtained after forward
    * @param output_grad: Gradient of output tensor
    * @param dims: Dimensions of input tensor
*/
template <class T>
__global__ void ShiftMaxbackward_(T* input_grad, const T* output_tensor, const T* output_grad, const int* dims);

/**
    * @brief Wrapper function to execute ShiftMaxbackward_
    * @param output_tensor: Output tensor obtained after forward
    * @param output_grad: Gradient of output tensor
    * @param input_grad: Gradient of input tensor (that we want to obtain)
    * @param size: Number of elements in the input tensor
    * @param dims: Dimensions of input tensor
*/
template <class T>
void ShiftMaxbackward(const T* output_tensor, const T* output_grad, T* input_grad, size_t size, std::vector<long unsigned int> dims);

}

#endif /* AIDGE_CUDA_OPERATOR_SHIFTMAXIMPL_FORWARD_KERNEL_H_ */
