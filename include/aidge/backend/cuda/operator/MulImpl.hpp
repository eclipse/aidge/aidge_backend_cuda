/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_MULIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_MULIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class MulImpl_cuda : public OperatorImpl {
public:
    MulImpl_cuda(const Mul_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<MulImpl_cuda> create(const Mul_Op& op) {
        return std::make_unique<MulImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;
    ~MulImpl_cuda();

private:
    std::vector<cudnnTensorDescriptor_t> mTensorDesc;
    cudnnOpTensorDescriptor_t mOpTensorDesc = nullptr;
    std::vector<std::shared_ptr<Tensor>> mInputFallbacks;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const std::vector<std::reference_wrapper<Tensor>>& inputs);
    template <class T> void backward_(const Tensor& outputGrad);
};

// Implementation entry point registration to Operator
REGISTRAR(Mul_Op, "cuda", Aidge::MulImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_MULIMPL_H_ */
