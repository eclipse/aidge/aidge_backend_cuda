/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_SIGMOIDIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_SIGMOIDIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Sigmoid.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class SigmoidImpl_cuda : public OperatorImpl {
public:
    SigmoidImpl_cuda(const Sigmoid_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<SigmoidImpl_cuda> create(const Sigmoid_Op& op) {
        return std::make_unique<SigmoidImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Any}
        };
    }

    void forward() override;
    void backward() override;
    ~SigmoidImpl_cuda();

private:
    // CuDNN specific variables
    #if CUDNN_VERSION >= 5000
        cudnnActivationDescriptor_t mSigmoidDesc = nullptr;
    #else
        cudnnActivationMode_t mSigmoidDesc = nullptr;
    #endif
    std::shared_ptr<Tensor> mInputFallback;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& output_grad);
};

// Implementation entry point registration to Operator
REGISTRAR(Sigmoid_Op, "cuda", Aidge::SigmoidImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_SIGMOIDIMPL_H_ */
