/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_AVGPOOLINGIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_AVGPOOLINGIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
template <DimIdx_t DIM>
class AvgPoolingImpl_cuda : public OperatorImpl {
public:
    AvgPoolingImpl_cuda(const AvgPooling_Op<DIM>& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<AvgPoolingImpl_cuda> create(const AvgPooling_Op<DIM>& op) {
        return std::make_unique<AvgPoolingImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;
    ~AvgPoolingImpl_cuda();

private:
    // CuDNN specific variables
    cudnnPoolingDescriptor_t mAvgPoolingDesc = nullptr;
    cudnnPoolingMode_t mMode = CUDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING;
    std::shared_ptr<Tensor> mInputFallback, mOutputGradFallback;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& output_grad);
};

// Implementation entry point registration to Operator
using AvgPooling2D_Op = AvgPooling_Op<2>;
REGISTRAR(AvgPooling2D_Op, "cuda", Aidge::AvgPoolingImpl_cuda<2>::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_AVGPOOLINGIMPL_H_ */
