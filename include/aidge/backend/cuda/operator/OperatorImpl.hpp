/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_IMPL_H_
#define AIDGE_CUDA_OPERATOR_IMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

template <class Op, class FwdFunc, class BwdFunc = void()>
class OperatorImpl_cuda : public OperatorImpl,
    public Registrable<OperatorImpl_cuda<Op, FwdFunc, BwdFunc>, ImplSpec, Impl<FwdFunc, BwdFunc>>
{
public:
    OperatorImpl_cuda(const Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<OperatorImpl_cuda<Op, FwdFunc, BwdFunc>> create(const Op& op) {
        return std::make_unique<OperatorImpl_cuda<Op, FwdFunc, BwdFunc>>(op);
    }

    virtual std::shared_ptr<ProdConso> getProdConso() const override {
        const auto impl = Registrar<OperatorImpl_cuda>::create(getBestMatch(getRequiredSpec()));
        return impl.prodConso(mOp);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        // return Registrar<OperatorImpl_cuda>::getKeys(); // Note: cannot return set due to python binding 
        std::set<ImplSpec> implSpecsSet = Registrar<OperatorImpl_cuda>::getKeys();
        return std::vector<ImplSpec>(implSpecsSet.begin(), implSpecsSet.end());
    }

    void forward() override;
    void backward() override;
};
}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_IMPL_H_ */
