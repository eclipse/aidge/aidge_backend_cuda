/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"


namespace Aidge {
// Operator implementation entry point for the backend
template <DimIdx_t DIM>
class ConvImpl_cuda : public OperatorImpl {
public:
    ConvImpl_cuda(const Operator&op, bool depthWise = false) : OperatorImpl(op, "cuda"), mDepthWise(depthWise) {}

    static std::unique_ptr<ConvImpl_cuda<DIM>> create(const Conv_Op<DIM>& op) {
        return std::make_unique<ConvImpl_cuda<DIM>>(op);
    }

    static std::unique_ptr<ConvImpl_cuda<DIM>> createDW(const ConvDepthWise_Op<DIM> &op) {
        return std::make_unique<ConvImpl_cuda<DIM>>(op, true);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Any}
        };
    }

    void forward() override;
    void backward() override;
    ~ConvImpl_cuda();

private:
    // CuDNN specific variables
    cudnnConvolutionDescriptor_t mConvDesc = nullptr;
    cudnnFilterDescriptor_t mFilterDesc = nullptr;
    cudnnConvolutionFwdAlgo_t mFwdAlgo = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
    cudnnConvolutionBwdFilterAlgo_t mBwdFilterAlgo;
    cudnnConvolutionBwdDataAlgo_t mBwdDataAlgo;
    size_t mWorkspaceSize = 0;
    void* mFwdWorkspace = nullptr;
    void* mBwdWorkspace = nullptr;
    std::shared_ptr<Tensor> mInput0Fallback;
    std::shared_ptr<Tensor> mInput1Fallback;
    std::shared_ptr<Tensor> mInput2Fallback;
    bool mDepthWise = false;

    template <class T> void forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2);
    template <class T> void backward_(const Tensor& input0, const Tensor& input1, const Tensor& input2);
};

// Implementation entry point registration to Operator
using Conv2D_Op = Conv_Op<2>;
using ConvDepthWise2D_Op = ConvDepthWise_Op<2>;
REGISTRAR(Conv2D_Op, "cuda", Aidge::ConvImpl_cuda<2>::create);
REGISTRAR(ConvDepthWise2D_Op, "cuda", Aidge::ConvImpl_cuda<2>::createDW);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_ */
