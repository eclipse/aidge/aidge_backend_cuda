/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_REDUCEMEANIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_REDUCEMEANIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class ReduceMeanImpl_cuda : public OperatorImpl {
public:
    ReduceMeanImpl_cuda(const ReduceMean_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<ReduceMeanImpl_cuda> create(const ReduceMean_Op& op) {
        return std::make_unique<ReduceMeanImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;
    ~ReduceMeanImpl_cuda();

private:
private:
    // CuDNN specific variables
    cudnnReduceTensorDescriptor_t mReduceDesc = nullptr;
    cudnnTensorDescriptor_t mOutputDesc = nullptr;
    size_t mWorkspaceSize = 0;
    void* mWorkspace = nullptr;
    std::shared_ptr<Tensor> mInputFallback;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const Tensor& input, const std::vector<int>& axes, bool keepDims);
    template <class T> void backward_(const Tensor& output_grad, const std::vector<int>& axes);
};

// Implementation entry point registration to Operator
REGISTRAR(ReduceMean_Op, "cuda", Aidge::ReduceMeanImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_REDUCEMEANIMPL_H_ */
