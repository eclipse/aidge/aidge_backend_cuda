/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_TANHIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_TANHIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Tanh.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class TanhImpl_cuda : public OperatorImpl {
public:
    TanhImpl_cuda(const Tanh_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<TanhImpl_cuda> create(const Tanh_Op& op) {
        return std::make_unique<TanhImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Any}
        };
    }

    void forward() override;
    void backward() override;
    ~TanhImpl_cuda();

private:
    // CuDNN specific variables
    #if CUDNN_VERSION >= 5000
        cudnnActivationDescriptor_t mTanhDesc = nullptr;
    #else
        cudnnActivationMode_t mTanhDesc = nullptr;
    #endif
    std::shared_ptr<Tensor> mInputFallback;
    std::shared_ptr<Tensor> mOutputGradFallback;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& output_grad);
};

// Implementation entry point registration to Operator
REGISTRAR(Tanh_Op, "cuda", Aidge::TanhImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_TANHIMPL_H_ */
