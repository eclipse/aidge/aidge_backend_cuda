/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class FCImpl_cuda : public OperatorImpl {
public:
    FCImpl_cuda(const FC_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<FCImpl_cuda> create(const FC_Op& op) {
        return std::make_unique<FCImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;

private:
    std::shared_ptr<Tensor> mInput0Fallback;
    std::shared_ptr<Tensor> mInput1Fallback;
    std::shared_ptr<Tensor> mInput2Fallback;

    template <class T> void forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2, std::size_t outChannels);
    template <class T> void backward_(const Tensor& input0, const Tensor& input1, const Tensor& input2, std::size_t outChannels);
};

// Implementation entry point registration to Operator
REGISTRAR(FC_Op, "cuda", Aidge::FCImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_ */
