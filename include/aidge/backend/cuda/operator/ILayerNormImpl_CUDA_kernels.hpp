/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_ILAYERNORMIMPL_FORWARD_KERNEL_H_
#define AIDGE_CUDA_OPERATOR_ILAYERNORMIMPL_FORWARD_KERNEL_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {

/**
    * @brief Compute the forward for ILayerNorm
    * @param input: Input tensor
    * @param SF: Scaling factor of input tensor
    * @param dims: Dimensions of input tensor
    * @param quantized_tensor: Quantized output tensor
    * @param square_tensor: Tensor use for computation
    * @param weight: weight of ILayerNorm layer 
    * @param bias: bias of ILayerNorm layer
    * @param new_SF: Scaling factor of output that can be use to dequantify
*/
template <class T>
__global__ void ILayerNormforward_(T* input, double SF, int* dims, int* quantized_tensor,long long int* square_tensor, T* weight, T* biase, double new_SF);

/**
    * @brief Wrapper function to execute ILayerNormforward_
    * @note Output correspond to the non-quantized tensor, to obtain the quantized tensor we need to copy quantized_tensor and not input_cuda_tensor
    * @param input: Input tensor
    * @param output: Output tensor (not quantized)
    * @param SF: Scaling factor of input tensor
    * @param weight_raw: weight of ILayerNorm layer 
    * @param bias_raw: bias of ILayerNorm layer
    * @param size: Number of elements in the input tensor
    * @param dims: Dimensions of input tensor
*/
template <class T>
void ILayerNormforward(const T* input, T* output, double SF, const T* weight_raw, const T* bias_raw, size_t size, std::vector<long unsigned int> dims_input);

/**
    * @brief Compute the backward for ILayerNorm
    * @param output_grad: Gradient of output tensor
    * @param input_tensor: Input tensor
    * @param output_tensor: Output tensor obtained after forward
    * @param mean: Arithmetic mean of input tensor
    * @param var: Arithmetic variance of input tensor
    * @param weight: weight of ILayerNorm layer 
    * @param bias: bias of ILayerNorm layer
    * @param input_grad: Gradient of input tensor 
    * @param weight_grad: Gradient of ILayerNorm weight 
    * @param bias_grad: Gradient of ILayerNorm bias 
    * @param size: Number of elements in the input tensor
*/
template <class T>
__global__ void ILayerNormbackward_(T* output_grad, T* input_tensor, T* output_tensor, T* mean, T* var, T* weight, T* bias, T* input_grad, T* weight_grad, T* bias_grad, int size);

/**
    * @brief Wrapper function to execute ILayerNormbackward_
    * @param input_tensor: Input tensor
    * @param output_grad: Gradient of output tensor
    * @param output_tensor: Output tensor obtained after forward
    * @param mean: Arithmetic mean of input tensor
    * @param var: Arithmetic variance of input tensor
    * @param weight: weight of ILayerNorm layer 
    * @param bias: bias of ILayerNorm layer
    * @param input_grad: Gradient of input tensor 
    * @param weight_grad: Gradient of ILayerNorm weight 
    * @param bias_grad: Gradient of ILayerNorm bias 
    * @param size: Number of elements in the input tensor
*/
template <class T>
void ILayerNormbackward(const T* input_tensor, const T* output_grad, const T* output_tensor,const T* mean,const T* var, const T* weight, const T* bias, T* input_grad, T* weight_grad, T* bias_grad, size_t size);

}

#endif /* AIDGE_CUDA_OPERATOR_ILAYERNORMIMPL_FORWARD_KERNEL_H_ */