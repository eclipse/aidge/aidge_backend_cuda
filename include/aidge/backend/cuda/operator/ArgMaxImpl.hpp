/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_ARGMAXIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_ARGMAXIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
class ArgMaxImpl_cuda : public OperatorImpl {
public:
    ArgMaxImpl_cuda(const ArgMax_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<ArgMaxImpl_cuda> create(const ArgMax_Op& op) {
        return std::make_unique<ArgMaxImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;

private:
    // CuDNN specific variables
    std::shared_ptr<Tensor> mInputFallback, mOutputGradFallback;

    template <class T> void forward_(const Tensor& input, std::int32_t axis, DimSize_t selectLastIdx);
};

// Implementation entry point registration to Operator
REGISTRAR(ArgMax_Op, "cuda", Aidge::ArgMaxImpl_cuda::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_ARGMAXIMPL_H_ */
