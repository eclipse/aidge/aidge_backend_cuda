/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_SQRTIMPL_KERNEL_H_
#define AIDGE_CUDA_OPERATOR_SQRTIMPL_KERNEL_H_

#include <stdexcept>
#include <cfloat>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>

#include "aidge/data/Data.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

template <class T>
void sqrtForward(const T* input,
                 T* output,
                 int size,
                 const T alpha,
                 const T beta);

template <class T>
void sqrtBackward(const T* input,
                  const T* outputGrad,
                  T* inputGrad,
                  int size,
                  const T alpha,
                  const T beta);
}
#endif /* AIDGE_CUDA_OPERATOR_SQRTIMPL_KERNEL_H_ */





