/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_BATCHNORMIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_BATCHNORMIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
template <DimIdx_t DIM>
class BatchNormImpl_cuda : public OperatorImpl {
public:
    BatchNormImpl_cuda(const BatchNorm_Op<DIM>& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<BatchNormImpl_cuda> create(const BatchNorm_Op<DIM>& op) {
        return std::make_unique<BatchNormImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;
    ~BatchNormImpl_cuda();

private:
    // CuDNN specific variables
    cudnnTensorDescriptor_t mBNDesc = nullptr;
    cudnnBatchNormMode_t mMode;
    double mEpsilon;

    template <class T> void forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2, const Tensor& input3, const Tensor& input4);
    template <class T> void backward_(const Tensor& input0, const Tensor& input1, const Tensor& input2);
};

// Implementation entry point registration to Operator
using BatchNorm2D_Op = BatchNorm_Op<2>;
REGISTRAR(BatchNorm2D_Op, "cuda", Aidge::BatchNormImpl_cuda<2>::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_BATCHNORMIMPL_H_ */
