/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_PADIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_PADIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
// Operator implementation entry point for the backend
template <DimIdx_t DIM>
class PadImpl_cuda : public OperatorImpl {
public:
    PadImpl_cuda(const Pad_Op<DIM>& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<PadImpl_cuda> create(const Pad_Op<DIM>& op) {
        return std::make_unique<PadImpl_cuda>(op);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        return {
            {DataType::Float64},
            {DataType::Float32},
            {DataType::Float16},
        };
    }

    void forward() override;
    void backward() override;

private:
    // CuDNN specific variables
    std::shared_ptr<Tensor> mInputFallback, mOutputGradFallback;
    int mLeftPad, mTopPad;
    double mPadVal;
    unsigned int mPadType;

    template <class T> void forward_(const Tensor& input);
    template <class T> void backward_(const Tensor& outGrad);
};

// Implementation entry point registration to Operator
using Pad2D_Op = Pad_Op<2>;
REGISTRAR(Pad2D_Op, "cuda", Aidge::PadImpl_cuda<2>::create);
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_PADIMPL_H_ */
