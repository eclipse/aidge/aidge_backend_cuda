/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include <stdio.h>

#include "aidge/backend/cuda/operator/Cublas_kernels.hpp"

namespace Aidge{

template <>
cublasStatus_t cublasGemm<__half>(cublasHandle_t handle,
                                  cublasOperation_t transa, cublasOperation_t transb,
                                  int m, int n, int k,
                                  const __half *alpha,
                                  const __half *A, int lda,
                                  const __half *B, int ldb,
                                  const __half *beta,
                                  __half *C, int ldc)
{
    return cublasHgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}

template <>
cublasStatus_t cublasGemm<float>(cublasHandle_t handle,
                                 cublasOperation_t transa, cublasOperation_t transb,
                                 int m, int n, int k,
                                 const float *alpha,
                                 const float *A, int lda,
                                 const float *B, int ldb,
                                 const float *beta,
                                 float *C, int ldc)
{
    return cublasSgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}

template <>
cublasStatus_t cublasGemm<double>(cublasHandle_t handle,
								  cublasOperation_t transa, cublasOperation_t transb,
								  int m, int n, int k,
								  const double *alpha,
								  const double *A, int lda,
								  const double *B, int ldb,
								  const double *beta,
								  double *C, int ldc)
{
    return cublasDgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}


template <>
cublasStatus_t cublasGemmStridedBatched<__half>(cublasHandle_t handle,
                                  cublasOperation_t transa, cublasOperation_t transb,
                                  int m, int n, int k,
                                  const __half *alpha,
                                  const __half *A, int lda, long long int strideA,
                                  const __half *B, int ldb, long long int strideB,
                                  const __half *beta,
                                  __half *C, int ldc, long long int strideC,
                                  int batchCount)
{
    return cublasHgemmStridedBatched(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda, strideA,
					   B, ldb, strideB,
					   beta,
					   C, ldc, strideC,
                       batchCount);
}

template <>
cublasStatus_t cublasGemmStridedBatched<float>(cublasHandle_t handle,
                                 cublasOperation_t transa, cublasOperation_t transb,
                                 int m, int n, int k,
                                 const float *alpha,
                                 const float *A, int lda, long long int strideA,
                                 const float *B, int ldb, long long int strideB,
                                 const float *beta,
                                 float *C, int ldc, long long int strideC,
                                 int batchCount)
{
    return cublasSgemmStridedBatched(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda, strideA,
					   B, ldb, strideB,
					   beta,
					   C, ldc, strideC,
                       batchCount);
}

template <>
cublasStatus_t cublasGemmStridedBatched<double>(cublasHandle_t handle,
								  cublasOperation_t transa, cublasOperation_t transb,
								  int m, int n, int k,
								  const double *alpha,
								  const double *A, int lda, long long int strideA,
								  const double *B, int ldb, long long int strideB,
								  const double *beta,
								  double *C, int ldc, long long int strideC,
                                  int batchCount)
{
    return cublasDgemmStridedBatched(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda, strideA,
					   B, ldb, strideB,
					   beta,
					   C, ldc, strideC,
                       batchCount);
}

template <>
cublasStatus_t cublasGemv<__half>(cublasHandle_t handle, cublasOperation_t trans,
                                 int m, int n,
                                 const __half          *alpha,
                                 const __half          *A, int lda,
                                 const __half          *x, int incx,
                                 const __half          *beta,
                                 __half          *y, int incy)
{
    // Using cublasHgemm() because there is no cublasHgemv() yet
    return cublasHgemm(handle,
        trans, CUBLAS_OP_N,
        m, 1, n,
        alpha,
        A, lda,
        x, incx,
        beta,
        y, incy);
}

template <>
cublasStatus_t cublasGemv<float>(cublasHandle_t handle, cublasOperation_t trans,
                                 int m, int n,
                                 const float          *alpha,
                                 const float          *A, int lda,
                                 const float          *x, int incx,
                                 const float          *beta,
                                 float          *y, int incy)
{
    return cublasSgemv(handle, trans,
        m, n,
        alpha,
        A, lda,
        x, incx,
        beta,
        y, incy);
}

template <>
cublasStatus_t cublasGemv<double>(cublasHandle_t handle, cublasOperation_t trans,
                                 int m, int n,
                                 const double          *alpha,
                                 const double          *A, int lda,
                                 const double          *x, int incx,
                                 const double          *beta,
                                 double          *y, int incy)
{
    return cublasDgemv(handle, trans,
        m, n,
        alpha,
        A, lda,
        x, incx,
        beta,
        y, incy);
}
}