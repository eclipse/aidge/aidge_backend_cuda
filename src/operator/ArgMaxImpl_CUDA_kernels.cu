/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/ArgMaxImpl_CUDA_kernels.hpp"
#define MAX_ERRORS 10
// Helper function for comparison
template <typename T>
__device__ bool compareGT(T a, T b) {
    return a > b;
}
template <>
__device__ bool compareGT<half>(half a, half b) {
    return __half2float(a) > __half2float(b);
}

// Helper function for comparison
template <typename T>
__device__ bool compareGE(T a, T b) {
    return a >= b;
}
template <>
__device__ bool compareGE<half>(half a, half b) {
    return __half2float(a) >= __half2float(b);
}
template <typename T>
__global__ void argmax_forward(const T* input, T* output, int* dims, int* strides, int axis, int total_elems, T minValue) {
    const unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int idx = index; idx < total_elems; idx += stride) {
        int axis_stride = strides[axis];
        int axis_dim = dims[axis];
        int outer_stride = idx / axis_stride;
        int inner_stride = idx % axis_stride;

        T max_val = minValue;
        int max_idx = 0;

        for (int i = 0; i < axis_dim; ++i) {
            int offset = outer_stride * axis_dim * axis_stride + i * axis_stride + inner_stride;
            if (offset >= total_elems) {
                return;
            }
            T val = input[offset];
            if (compareGT(val, max_val)) {
                max_val = val;
                max_idx = i;
            }
        }

        int output_index = outer_stride * axis_stride + inner_stride;
        if (output_index >= (total_elems / axis_dim)) {
            return;
        }
        output[output_index] = max_idx;
    }
}

template <typename T>
__global__ void argmax_forward_selectLastIdx(const T* input, T* output, int* dims, int* strides, int axis, int total_elems, T minValue) {
    const unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int idx = index; idx < total_elems; idx += stride) {
        int axis_stride = strides[axis];
        int axis_dim = dims[axis];
        int outer_stride = idx / axis_stride;
        int inner_stride = idx % axis_stride;

        T max_val = minValue;
        int max_idx = 0;

        for (int i = 0; i < axis_dim; ++i) {
            int offset = outer_stride * axis_dim * axis_stride + i * axis_stride + inner_stride;
            if (offset >= total_elems) {
                return;
            }
            T val = input[offset];
            if (compareGE(val, max_val)) {
                max_val = val;
                max_idx = i;
            }
        }

        int output_index = outer_stride * axis_stride + inner_stride;
        if (output_index >= (total_elems / axis_dim)) {
            return;
        }
        output[output_index] = max_idx;
    }
}

template <typename T>
T minValue();

template <>
double minValue<double>() {
    return  std::numeric_limits<double>::min();
}

template <>
float minValue<float>() {
    return std::numeric_limits<float>::min();
}

template <>
half minValue<half>() {
    return __float2half(std::numeric_limits<float>::min());
}

template <typename T>
void Aidge::ArgMax_cuda_forward_kernel(const T* input, T* output,
                                const std::vector<int>& inputDims, const std::vector<int>& inputStrides,
                                int axis, int total_elems, std::size_t selectLastIdx) {

    // Define block and grid sizes
    int blockSize = 256;
    int gridSize = (total_elems + blockSize - 1) / blockSize;


    int *d_input_strides, *d_input_shape;
    // Allocate device memory
    CHECK_CUDA_STATUS(cudaMalloc(&d_input_shape, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input_strides, inputDims.size() * sizeof(int)));

    // Copy data from host to device;
    CHECK_CUDA_STATUS(cudaMemcpy(d_input_shape, inputDims.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input_strides, inputStrides.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    // Launch the kernel
    if (selectLastIdx) {
        argmax_forward_selectLastIdx<<<gridSize, blockSize>>>(input, output, d_input_shape, d_input_strides, axis, total_elems, minValue<T>());
    }
    else {
        argmax_forward<<<gridSize, blockSize>>>(input, output, d_input_shape, d_input_strides, axis, total_elems, minValue<T>());
    }

    CHECK_CUDA_STATUS(cudaFree(d_input_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input_strides));
}



template void Aidge::ArgMax_cuda_forward_kernel(const double* input, double* output,
                                const std::vector<int>& inputDims, const std::vector<int>& inputStrides,
                                int axis, int total_elems, std::size_t selectLastIdx);

template void Aidge::ArgMax_cuda_forward_kernel(const float* input, float* output,
                                const std::vector<int>& inputDims, const std::vector<int>& inputStrides,
                                int axis, int total_elems, std::size_t selectLastIdx);

template void Aidge::ArgMax_cuda_forward_kernel(const half* input, half* output,
                                const std::vector<int>& inputDims, const std::vector<int>& inputStrides,
                                int axis, int total_elems, std::size_t selectLastIdx);