/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/RoundImpl_CUDA_kernels.hpp"

// Helper function for Round
#include <math.h>

template <typename T>
__device__ T round_util(T a) {
    if (a - floor(a) == 0.5) {
        if (fmod(floor(a), 2.0) == 0.0) {
            return floor(a);
        } else {
            return ceil(a);
        }
    }
    return round(a);
}
template <>
__device__ float round_util<float>(float a) {
    if (a - floor(a) == 0.5) {
        if (fmodf(floor(a), 2.0) == 0.0) {
            return floor(a);
        } else {
            return ceil(a);
        }
    }
    return roundf(a);
}


template <>
__device__ half round_util<half>(half a) {
#if __CUDA_ARCH__ >= 530 && defined(CUDART_VERSION) && CUDART_VERSION >= 8000
    return __float2half_rn(__half2float(a));
#else
    float af = __half2float(a);
    return __float2half(round_util(af));
#endif
}





template <class T>
__global__ void roundKernel(const T* input, T* output, int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;
    output[idx] = round_util(input[idx]);
}

template <class T>

void Aidge::roundForward(const T* input, T* output,int size)
{
    int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    roundKernel<<<numBlocks, blockSize>>>(input, output, size);

    CHECK_CUDA_STATUS(cudaGetLastError());
    CHECK_CUDA_STATUS(cudaDeviceSynchronize());
};

template void Aidge::roundForward<double>(const double* input, double* output, int size);

template void Aidge::roundForward<float>(const float* input, float* output, int size);

template void Aidge::roundForward<half>(const half* input, half* output, int size);

