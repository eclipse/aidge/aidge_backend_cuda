/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/DivImpl_CUDA_kernels.hpp"


// Helper function for Div
template <typename T>
__device__ T div(T a, T b) {
    return a / b;
}
template <>
__device__ half div<half>(half a, half b) {
#if __CUDA_ARCH__ >= 530 && defined(CUDART_VERSION) && CUDART_VERSION >= 8000
    return __hdiv(a, b);
#else
    return __float2half(__half2float(a) / __half2float(b));
#endif
}

template <class T>
__global__ void divKernel(const T* input1, T* output, const T* input2,
                          int* input1_shape, int* input2_shape, int* output_shape,
                          int* input1_strides, int* input2_strides, int* output_strides,
                          int num_dims, int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;

    int input1_idx = 0, input2_idx = 0;
    int temp_idx = idx;
    for (int i = 0; i < num_dims; ++i) {
        int dim = temp_idx / output_strides[i];
        temp_idx %= output_strides[i];
        input1_idx += (input1_shape[i] == 1 ? 0 : dim) * input1_strides[i];
        input2_idx += (input2_shape[i] == 1 ? 0 : dim) * input2_strides[i];
    }
    output[idx] = div(input1[input1_idx], input2[input2_idx]);
}

template <class T>
void Aidge::divForward(const T* input1, T* output, const T* input2,
                        const std::vector<int>& input1Dims,const std::vector<int>& input2Dims, const std::vector<int>& outputDims,
                        const std::vector<int>& input1Strides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                        int outSize)
{
    int *d_input1_strides, *d_input2_strides, *d_output_strides, *d_input1_shape, *d_input2_shape, *d_output_shape;
    // Allocate device memory
    CHECK_CUDA_STATUS(cudaMalloc(&d_input1_shape, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input2_shape, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_output_shape, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input1_strides, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input2_strides, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_output_strides, input1Dims.size() * sizeof(int)));

    // Copy data from host to device;
    CHECK_CUDA_STATUS(cudaMemcpy(d_input1_shape, input1Dims.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input2_shape, input2Dims.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_output_shape, outputDims.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input1_strides, input1Strides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input2_strides, input2Strides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_output_strides, outputStrides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    int blockSize = 256;
    int numBlocks = (outSize + blockSize - 1) / blockSize;

    int num_dims = input1Dims.size();
    // Launch the kernel
    divKernel<<<numBlocks, blockSize>>>(input1, output, input2,
                                        d_input1_shape, d_input2_shape, d_output_shape,
                                        d_input1_strides, d_input2_strides, d_output_strides,
                                        num_dims, outSize);
    CHECK_CUDA_STATUS(cudaFree(d_input1_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input2_shape));
    CHECK_CUDA_STATUS(cudaFree(d_output_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input1_strides));
    CHECK_CUDA_STATUS(cudaFree(d_input2_strides));
    CHECK_CUDA_STATUS(cudaFree(d_output_strides));
};

template void Aidge::divForward<double>(const double* input1, double* output, const double* input2,
                                const std::vector<int>& input1Dims,const std::vector<int>& input2Dims, const std::vector<int>& outputDims,
                                const std::vector<int>& input1Strides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                                int outSize);

template void Aidge::divForward<float>(const float* input1, float* output, const float* input2,
                                const std::vector<int>& input1Dims,const std::vector<int>& input2Dims, const std::vector<int>& outputDims,
                                const std::vector<int>& input1Strides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                                int outSize);

template void Aidge::divForward<half>(const half* input1, half* output, const half* input2,
                                const std::vector<int>& input1Dims,const std::vector<int>& input2Dims, const std::vector<int>& outputDims,
                                const std::vector<int>& input1Strides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                                int outSize);
