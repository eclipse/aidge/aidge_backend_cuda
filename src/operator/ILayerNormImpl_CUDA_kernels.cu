/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/ILayerNormImpl_CUDA_kernels.hpp"

#include <algorithm>  // std::min
#include <cmath>      // std::sqrt
#include <cstddef>    // std::size_t
#include <vector>

#include <cuda.h>
#include <cuda_runtime.h>
#include <fmt/core.h>

#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define CLAMP(X) (((X) < (0)) ? (0) : (X))

namespace Aidge{

template <class T>
__global__ void ILayerNormforward_(T* input, double SF, int* dims, int* quantized_tensor,long long int* square_tensor, T* weight, T* biase, double new_SF) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int z = blockIdx.z * blockDim.z + threadIdx.z;

    int k = 1 << 16;
    long long int sum = 0;
    if (x < dims[0] && y < dims[1] && z < dims[2]) {
        int maxIdx = x * dims[1] * dims[2] * dims[3] + y * dims[2] * dims[3] + z * dims[3];
        int val;
        int mean_val = 0;
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            val = roundf(input[idx] / SF);
            quantized_tensor[idx] = val;
            mean_val += val;
        }
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            quantized_tensor[idx] -= (mean_val/dims[3]) ;
            square_tensor[idx] = (quantized_tensor[idx] * quantized_tensor[idx]); // I-ViT code implementation
            //square_tensor[idx] = (quantized_tensor[idx] * quantized_tensor[idx])/dims[3]; // I-ViT paper implementation
        }
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            sum += square_tensor[idx];
            biase[i] =  (biase[i]/weight[i])/new_SF;
            weight[i] = weight[i] * new_SF;
        }
        for(int h = 0; h < 10 ; h++)
        {
            k = floorf((k + floorf(sum / k))/2);
        }
        int factor = (((1 << 31) - 1) / k);
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            square_tensor[idx] =  (biase[idx]/weight[idx])/new_SF;
            quantized_tensor[idx] = (quantized_tensor[idx] * factor / 2) + biase[maxIdx];
            input[idx] = quantized_tensor[idx] * new_SF;
        }

    }
}

template <>
void ILayerNormforward<float>(const float* input, float* output, double SF, const float* weight_raw, const float* bias_raw, std::size_t size, std::vector<long unsigned int> dims_input)
{
    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims_input.size(), std::size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims_input[i]);
    }

    double new_SF = std::sqrt(dims_input_cuda[3]) / (1 << 30);

    float* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(float));
    cudaMemcpy(input_cuda_tensor,input, size * sizeof(float),cudaMemcpyHostToDevice);

    int *quantized_tensor;
    cudaMalloc(&quantized_tensor, size * sizeof(int));

    int *dims;
    cudaMalloc(&dims, 4 * sizeof(int));
    cudaMemcpy(dims, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    float *weight;
    cudaMalloc(&weight,dims_input_cuda[3]*sizeof(float));
    cudaMemcpy(weight,weight_raw,dims_input_cuda[3]*sizeof(float),cudaMemcpyHostToDevice);

    float *bias;
    cudaMalloc(&bias,dims_input_cuda[3]*sizeof(float));
    cudaMemcpy(bias,bias_raw,dims_input_cuda[3]*sizeof(float),cudaMemcpyHostToDevice);

    long long int* Squaretensor;
    cudaMalloc(&Squaretensor,(size)*sizeof(long long int));

    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks((dims_input_cuda[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
                   (dims_input_cuda[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
                   (dims_input_cuda[2] + threadsPerBlock.z - 1) / threadsPerBlock.z);

    ILayerNormforward_<float><<<numBlocks,threadsPerBlock>>>(input_cuda_tensor,SF,dims,quantized_tensor,Squaretensor,weight,bias,new_SF);
    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
       fmt::print(stderr, "CUDA Error: {}", cudaGetErrorString(err));
    }
    cudaMemcpy(output,input_cuda_tensor, (size ) * sizeof(float), cudaMemcpyDeviceToHost);


    cudaFree(input_cuda_tensor);
    cudaFree(weight);
    cudaFree(bias);
    cudaFree(dims);
    cudaFree(quantized_tensor);
}

template <>
void ILayerNormforward<double>(const double* input, double* output, double SF, const double* weight_raw, const double* bias_raw, std::size_t size, std::vector<long unsigned int> dims_input)
{
    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims_input.size(), std::size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims_input[i]);
    }

    double new_SF = std::sqrt(dims_input_cuda[3]) / (1 << 30);

    double* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(double));
    cudaMemcpy(input_cuda_tensor,input, size * sizeof(double),cudaMemcpyHostToDevice);

    int *quantized_tensor;
    cudaMalloc(&quantized_tensor, size * sizeof(int));

    int *dims;
    cudaMalloc(&dims, 4 * sizeof(int));
    cudaMemcpy(dims, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    double *weight;
    cudaMalloc(&weight,dims_input_cuda[3]*sizeof(double));
    cudaMemcpy(weight,weight_raw,dims_input_cuda[3]*sizeof(double),cudaMemcpyHostToDevice);

    double *bias;
    cudaMalloc(&bias,dims_input_cuda[3]*sizeof(double));
    cudaMemcpy(bias,bias_raw,dims_input_cuda[3]*sizeof(double),cudaMemcpyHostToDevice);

    long long int* Squaretensor;
    cudaMalloc(&Squaretensor,(size)*sizeof(long long int));

    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks((dims_input_cuda[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
                   (dims_input_cuda[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
                   (dims_input_cuda[2] + threadsPerBlock.z - 1) / threadsPerBlock.z);

    ILayerNormforward_<double><<<numBlocks,threadsPerBlock>>>(input_cuda_tensor,SF,dims,quantized_tensor,Squaretensor,weight,bias,new_SF);
    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        fmt::print(stderr, "CUDA Error: {}", cudaGetErrorString(err));
    }

    cudaMemcpy(output,input_cuda_tensor, (size ) * sizeof(double), cudaMemcpyDeviceToHost);

    cudaFree(input_cuda_tensor);
    cudaFree(weight);
    cudaFree(bias);
    cudaFree(dims);
    cudaFree(quantized_tensor);
}

template <class T>
__global__ void ILayerNormbackward_(T* output_grad, T* input_tensor, T* output_tensor, T* mean, T* var, T* weight, T* bias, T* input_grad, T* weight_grad, T* bias_grad, int size)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        T d_norm = output_grad[i] * weight[i];
        T d_var = d_norm * (input_tensor[i] - mean[i]) * -0.5 * powf(var[i] + 1e-6, -1.5);
        T d_mean = d_norm * -1 / sqrtf(var[i] + 1e-6) + d_var * -2 * mean[i] / size;
        T d_input = d_norm / sqrtf(var[i] + 1e-6) + d_var * 2 * (input_tensor[i] - mean[i]) / size + d_mean / size;

        input_grad[i] = d_input;
        weight_grad[i] = output_grad[i] * output_tensor[i];
        bias_grad[i] = output_grad[i];
    }
}

template <>
void ILayerNormbackward<float>(const float* input_tensor, const float* output_grad, const float* output_tensor,const float* mean,const float* var, const float* weight, const float* bias, float* input_grad, float* weight_grad, float* bias_grad, std::size_t size)
{
    float* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(float));
    cudaMemcpy(input_cuda_tensor,input_tensor,size*sizeof(float),cudaMemcpyHostToDevice);

    float* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(float));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(float),cudaMemcpyHostToDevice);

    float* output_tensor_;
    cudaMalloc(&output_tensor_,size*sizeof(float));
    cudaMemcpy(output_tensor_,output_tensor,size*sizeof(float),cudaMemcpyHostToDevice);

    float* mean_;
    cudaMalloc(&mean_,size*sizeof(float));
    cudaMemcpy(mean_,mean,size*sizeof(float),cudaMemcpyHostToDevice);

    float* var_;
    cudaMalloc(&var_,size*sizeof(float));
    cudaMemcpy(var_,var,size*sizeof(float),cudaMemcpyHostToDevice);

    float* weight_;
    cudaMalloc(&weight_,size*sizeof(float));
    cudaMemcpy(weight_,weight,size*sizeof(float),cudaMemcpyHostToDevice);

    float* bias_;
    cudaMalloc(&bias_,size*sizeof(float));
    cudaMemcpy(bias_,bias,size*sizeof(float),cudaMemcpyHostToDevice);


    float* input_grad_;
    cudaMalloc(&input_grad_,size*sizeof(float));

    float* weight_grad_;
    cudaMalloc(&weight_grad_,size*sizeof(float));

    float* bias_grad_;
    cudaMalloc(&bias_grad_,size*sizeof(float));


    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ILayerNormbackward_<<<Blocks,threadParBlock>>>(output_grad_,input_cuda_tensor,output_tensor_,mean_,var_,weight_,bias_,input_grad_, weight_grad_, bias_grad_, size);

    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        fmt::print(stderr, "CUDA Error: {}", cudaGetErrorString(err));
    }

    cudaMemcpy(input_grad , input_grad_, (size) * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(weight_grad , weight_grad_, (size) * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(bias_grad , bias_grad_, (size) * sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(input_cuda_tensor);
    cudaFree(output_grad_);
    cudaFree(mean_);
    cudaFree(var_);
    cudaFree(weight_);
    cudaFree(bias_);
    cudaFree(input_grad_);
    cudaFree(weight_grad_);
    cudaFree(bias_grad_);

}

template <>
void ILayerNormbackward<double>(const double* input_tensor, const double* output_grad, const double* output_tensor,const double* mean,const double* var, const double* weight, const double* bias, double* input_grad, double* weight_grad, double* bias_grad, std::size_t size)
{
    double* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(double));
    cudaMemcpy(input_cuda_tensor,input_tensor,size*sizeof(double),cudaMemcpyHostToDevice);

    double* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(double));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(double),cudaMemcpyHostToDevice);

    double* output_tensor_;
    cudaMalloc(&output_tensor_,size*sizeof(double));
    cudaMemcpy(output_tensor_,output_tensor,size*sizeof(double),cudaMemcpyHostToDevice);

    double* mean_;
    cudaMalloc(&mean_,size*sizeof(double));
    cudaMemcpy(mean_,mean,size*sizeof(double),cudaMemcpyHostToDevice);

    double* var_;
    cudaMalloc(&var_,size*sizeof(double));
    cudaMemcpy(var_,var,size*sizeof(double),cudaMemcpyHostToDevice);

    double* weight_;
    cudaMalloc(&weight_,size*sizeof(double));
    cudaMemcpy(weight_,weight,size*sizeof(double),cudaMemcpyHostToDevice);

    double* bias_;
    cudaMalloc(&bias_,size*sizeof(double));
    cudaMemcpy(bias_,bias,size*sizeof(double),cudaMemcpyHostToDevice);


    double* input_grad_;
    cudaMalloc(&input_grad_,size*sizeof(double));

    double* weight_grad_;
    cudaMalloc(&weight_grad_,size*sizeof(double));

    double* bias_grad_;
    cudaMalloc(&bias_grad_,size*sizeof(double));


    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ILayerNormbackward_<<<Blocks,threadParBlock>>>(output_grad_,input_cuda_tensor,output_tensor_,mean_,var_,weight_,bias_,input_grad_, weight_grad_, bias_grad_, size);

    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        fmt::print(stderr, "CUDA Error: {}", cudaGetErrorString(err));

    }


    cudaMemcpy(input_grad , input_grad_, (size) * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(weight_grad , weight_grad_, (size) * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(bias_grad , bias_grad_, (size) * sizeof(double), cudaMemcpyDeviceToHost);

    cudaFree(input_cuda_tensor);
    cudaFree(output_grad_);
    cudaFree(mean_);
    cudaFree(var_);
    cudaFree(weight_);
    cudaFree(bias_);
    cudaFree(input_grad_);
    cudaFree(weight_grad_);
    cudaFree(bias_grad_);
}

}