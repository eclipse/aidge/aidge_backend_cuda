/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define CLAMP(X) (((X) < (0)) ? (0) : (X))

#include "aidge/backend/cuda/operator/ShiftMaxImpl_CUDA_kernels.hpp"

#include <algorithm>  // std::min
#include <math.h>
#include <cstddef>    // std::size_t
#include <vector>

#include <cuda.h>
#include <cuda_runtime.h>
#include <fmt/core.h>

__device__ inline int ExpShift(int I,int N, double SF)
{
    int Ip = I + (I >> 1) - (I >> 4);
    int I0 = floorf(-1.0/SF);
    Ip = MAX(Ip,N*I0);
    int q = floorf(Ip / (I0));
    int r = Ip -(I0*q);
    int Ib = r/2 - I0;
    Ib = CLAMP(Ib * powf(2,N-q));
    return (int)Ib;
}

namespace Aidge{

template <class T>
__global__ void ShiftMaxforward_(T* input,int* quantized_tensor,int* factor, int* dims, double SF, int N, int output_bits,double new_SF)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int z = blockIdx.z * blockDim.z + threadIdx.z;
    int sum = 0;

    if (x < dims[0] && y < dims[1] && z < dims[2]) {
        int maxIdx = x * dims[1] * dims[2] * dims[3] + y * dims[2] * dims[3] + z * dims[3];
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            quantized_tensor[idx] = roundf(input[idx] / SF);
        }
        int maxVal = quantized_tensor[maxIdx];
        for (int i = 1; i < dims[3]; i++) {
            int idx = maxIdx + i;
            maxVal = MAX(maxVal, quantized_tensor[idx]);
        }
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            quantized_tensor[idx] = ExpShift(quantized_tensor[idx]-maxVal,N,SF);
        }
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            if(quantized_tensor[idx] > 0 && sum > INT_MAX - quantized_tensor[idx])
            {
                sum = INT_MAX;
                break;
            }
            else {
                sum += quantized_tensor[idx];
            }
        }
        factor[x * dims[1] * dims[2] + y * dims[2] + z] = floorf(INT_MAX/sum);
        for(int i= 0; i < dims[3]; ++i)
        {
            int idx = maxIdx + i;
            quantized_tensor[idx] = (quantized_tensor[idx] * factor[x * dims[1] * dims[2] + y * dims[2] + z]) >> (31-(2*output_bits-1));
            input[idx] =quantized_tensor[idx]*new_SF;
        }
    }
}

template <>
void ShiftMaxforward<float>(const float* input, float* output, double SF, int N, int output_bits, size_t size, std::vector<long unsigned int> dims_input) {

    double new_SF = 1 / powf(2, 2 * output_bits - 1); // New scaling factor

    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims_input.size(), size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims_input[i]);
    }

    // Allocate memory on the GPU
    float* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor, size * sizeof(float));
    cudaMemcpy(input_cuda_tensor, input, size * sizeof(float), cudaMemcpyHostToDevice);

    int* quantized_tensor;
    cudaMalloc(&quantized_tensor, size * sizeof(int));

    int* factor;
    cudaMalloc(&factor, size * sizeof(int));

    int* dims;
    cudaMalloc(&dims, 4 * sizeof(int));
    cudaMemcpy(dims, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    // Calculate grid and block dimensions
    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks(
        (dims_input_cuda[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
        (dims_input_cuda[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
        (dims_input_cuda[2] + threadsPerBlock.z - 1) / threadsPerBlock.z
    );

    // Launch the kernel (assuming a templated ShiftMaxWholeKernel function exists)
    ShiftMaxforward_<float><<<numBlocks, threadsPerBlock>>>(input_cuda_tensor, quantized_tensor, factor, dims, SF, N, output_bits, new_SF);
    cudaDeviceSynchronize();

    // Check for CUDA errors
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        fmt::print(stderr, "CUDA Error: {}\n", cudaGetErrorString(err));
    }

    // Copy the result back to host
    cudaMemcpy(output, input_cuda_tensor, size * sizeof(float), cudaMemcpyDeviceToHost);

    // Free allocated memory on GPU
    cudaFree(quantized_tensor);
    cudaFree(factor);
    cudaFree(dims);
    cudaFree(input_cuda_tensor);
}

template <>
void ShiftMaxforward<double>(const double* input, double* output, double SF, int N, int output_bits, size_t size, std::vector<long unsigned int> dims_input) {

    double new_SF = 1 / powf(2, 2 * output_bits - 1);

    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims_input.size(), size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims_input[i]);
    }

    // Allocate memory on the GPU
    double* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor, size * sizeof(double));
    cudaMemcpy(input_cuda_tensor, input, size * sizeof(double), cudaMemcpyHostToDevice);

    int* quantized_tensor;
    cudaMalloc(&quantized_tensor, size * sizeof(int));

    int* factor;
    cudaMalloc(&factor, size * sizeof(int));

    int* dims;
    cudaMalloc(&dims, 4 * sizeof(int));
    cudaMemcpy(dims, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    // Calculate grid and block dimensions
    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks(
        (dims_input_cuda[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
        (dims_input_cuda[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
        (dims_input_cuda[2] + threadsPerBlock.z - 1) / threadsPerBlock.z
    );

    // Launch the kernel (assuming a templated ShiftMaxWholeKernel function exists)
    ShiftMaxforward_<double><<<numBlocks, threadsPerBlock>>>(input_cuda_tensor, quantized_tensor, factor, dims, SF, N, output_bits, new_SF);
    cudaDeviceSynchronize();

    // Check for CUDA errors
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        fmt::print(stderr, "CUDA Error: {}\n", cudaGetErrorString(err));
    }

    // Copy the result back to host
    cudaMemcpy(output, input_cuda_tensor, size * sizeof(double), cudaMemcpyDeviceToHost);

    // Free allocated memory on GPU
    cudaFree(quantized_tensor);
    cudaFree(factor);
    cudaFree(dims);
    cudaFree(input_cuda_tensor);
}


template <class T>
__global__ void ShiftMaxbackward_(T* input_grad, const T* output_tensor, const T* output_grad, const int* dims) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    if (index < dims[0] * dims[1] * dims[2] * dims[3]) {
        int w = (index / dims[3]) % dims[2];
        int h = (index / dims[3] / dims[2]) % dims[1];
        int n = index / dims[3] / dims[2] / dims[1];

        float sum = 0.0f;
        for (int i = 0; i < dims[3]; ++i) {
            sum += output_tensor[n * dims[1] * dims[2] * dims[3] + h * dims[2] * dims[3] + w * dims[3] + i] * output_grad[n * dims[1] * dims[2] * dims[3] + h * dims[2] * dims[3] + w * dims[3] + i];
        }
        input_grad[index] = output_tensor[index] * (output_grad[index] - sum);
    }
}

template <>
void ShiftMaxbackward<float>(const float* output_tensor, const float* output_grad, float* input_grad, size_t size, std::vector<long unsigned int> dims)
{
    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims.size(), size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims[i]);
    }

    float* output_cuda_tensor;
    cudaMalloc(&output_cuda_tensor,size*sizeof(float));
    cudaMemcpy(output_cuda_tensor,output_tensor,size*sizeof(float),cudaMemcpyHostToDevice);

    float* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(float));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(float),cudaMemcpyHostToDevice);

    float *input_grad_;
    cudaMalloc(&input_grad_, size * sizeof(float));

    int *dims_;
    cudaMalloc(&dims_, 4 * sizeof(int));
    cudaMemcpy(dims_, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ShiftMaxbackward_<float><<<Blocks,threadParBlock>>>(input_grad_,output_cuda_tensor,output_grad_,dims_);
    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        fmt::print(stderr, "CUDA Error: {}\n", cudaGetErrorString(err));
    }

    cudaMemcpy(input_grad, input_grad_, (size) * sizeof(float), cudaMemcpyDeviceToHost);
    cudaFree(output_cuda_tensor);
    cudaFree(input_grad_);
    cudaFree(dims_);
    cudaFree(output_grad_);
}

template <>
void ShiftMaxbackward<double>(const double* output_tensor, const double* output_grad, double* input_grad, size_t size, std::vector<long unsigned int> dims)
{
    int dims_input_cuda[4] = {1, 1, 1, 1};
    for (std::size_t i = 0; i < std::min(dims.size(), size_t(4)); ++i) {
        dims_input_cuda[i] = static_cast<int>(dims[i]);
    }

    double* output_cuda_tensor;
    cudaMalloc(&output_cuda_tensor,size*sizeof(double));
    cudaMemcpy(output_cuda_tensor,output_tensor,size*sizeof(double),cudaMemcpyHostToDevice);

    double* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(double));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(double),cudaMemcpyHostToDevice);

    double *input_grad_;
    cudaMalloc(&input_grad_, size * sizeof(double));

    int *dims_;
    cudaMalloc(&dims_, 4 * sizeof(int));
    cudaMemcpy(dims_, dims_input_cuda, 4 * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ShiftMaxbackward_<double><<<Blocks,threadParBlock>>>(input_grad_,output_cuda_tensor,output_grad_,dims_);
    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        fmt::print(stderr, "CUDA Error: {}\n", cudaGetErrorString(err));
    }

    cudaMemcpy(input_grad,input_grad_, (size) * sizeof(double), cudaMemcpyDeviceToHost);
    cudaFree(output_cuda_tensor);
    cudaFree(input_grad_);
    cudaFree(dims_);
    cudaFree(output_grad_);
}



}