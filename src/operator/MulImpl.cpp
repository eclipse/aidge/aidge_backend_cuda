/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>
#include <chrono>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/MulImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/utils/Types.h"

void Aidge::MulImpl_cuda::forward() {
   const Mul_Op& op = static_cast<const Mul_Op&>(mOp);
    // Check inputs
    AIDGE_ASSERT(op.getInput(0), "missing input in Mul operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run Mul forward because the 0-th input has no implementation.");
    DataType datatypeFirstInput = op.getInput(0)->dataType();
    for (IOIndex_t i = 1; i < op.nbInputs(); ++i) {
        AIDGE_ASSERT(op.getInput(i), "missing input in Mul operator");
        AIDGE_ASSERT(op.getInput(i)->hasImpl(), "cannot run Mul forward because the {}-th input has no implementation.", i);
        AIDGE_ASSERT(op.getInput(i)->dataType() == datatypeFirstInput, "Cannot Mul inputs with two differents data type.");
    }

    if (mInputFallbacks.empty()) {
        mInputFallbacks.resize(op.nbInputs());
    }

    std::vector<std::reference_wrapper<Tensor>> inputs;
    for (IOIndex_t i = 0; i < op.nbInputs(); ++i) {
        inputs.push_back(op.getInput(i)->refCastFrom(mInputFallbacks[i], *op.getOutput(0)));

        if (mTensorDesc.size() <= i) {
            // Get tensor dims and broadcast them
            std::vector<int> dims(inputs[i].get().dims().begin(), inputs[i].get().dims().end());
            dims.insert(dims.cbegin(), op.getOutput(0)->nbDims() - dims.size(), int(1));

            if (dims.size() < 4) {
                dims.resize(4, 1);
            }

            // Compute the corresponding strides
            std::vector<int> strides(dims.size());
            int product = 1;
            for (size_t j = dims.size(); j > 0; --j) {
                strides[j - 1] = product;
                product *= dims[j - 1];
            }
            
            mTensorDesc.push_back(nullptr);
            CHECK_CUDNN_STATUS(cudnnCreateTensorDescriptor(&mTensorDesc[i]));
            CHECK_CUDNN_STATUS(cudnnSetTensorNdDescriptor(mTensorDesc[i], DataTypeToCudnn(op.getOutput(0)->dataType()), dims.size(), dims.data(), strides.data()));
        }
    }

    if (mOpTensorDesc == nullptr) {
        CHECK_CUDNN_STATUS(cudnnCreateOpTensorDescriptor(&mOpTensorDesc));
        CHECK_CUDNN_STATUS(cudnnSetOpTensorDescriptor(mOpTensorDesc, CUDNN_OP_TENSOR_MUL, DataTypeToCudnn(op.getOutput(0)->dataType()), CUDNN_PROPAGATE_NAN));
    }

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(inputs);
            break;
        case DataType::Float32:
            forward_<float>(inputs);
            break;
        case DataType::Float16:
            forward_<half>(inputs);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::MulImpl_cuda::forward_(const std::vector<std::reference_wrapper<Tensor>>& inputs) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    if(inputs[0].get().size()>inputs[1].get().size()) {
        CHECK_CUDNN_STATUS(cudnnOpTensor(CudaContext::cudnnHandle(),
                                        mOpTensorDesc,
                                        &alpha,
                                        mTensorDesc[0],
                                        inputs[0].get().getImpl()->rawPtr(),
                                        &alpha,
                                        mTensorDesc[1],
                                        inputs[1].get().getImpl()->rawPtr(),
                                        &beta,
                                        std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                                        std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()));
    }
    else {
        CHECK_CUDNN_STATUS(cudnnOpTensor(CudaContext::cudnnHandle(),
                                mOpTensorDesc,
                                &alpha,
                                mTensorDesc[1],
                                inputs[1].get().getImpl()->rawPtr(),
                                &alpha,
                                mTensorDesc[0],
                                inputs[0].get().getImpl()->rawPtr(),
                                &beta,
                                std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                                std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()));
    }
}

void Aidge::MulImpl_cuda::backward() {
    const Mul_Op& op = static_cast<const Mul_Op&>(mOp);
    // Check output
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output gradient in Mul operator");
    AIDGE_ASSERT(op.getOutput(0)->grad()->hasImpl(), "cannot run Mul backward because the output gradient has no implementation.");

    const auto& outputGrad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            backward_<double>(outputGrad);
            break;
        case DataType::Float32:
            backward_<float>(outputGrad);
            break;
        case DataType::Float16:
            backward_<half>(outputGrad);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::MulImpl_cuda::backward_(const Tensor& outputGrad) {

    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 1.0f; // accumulate

    // Input0_grad = output_grad * Input1
    CHECK_CUDNN_STATUS(cudnnOpTensor(CudaContext::cudnnHandle(),
                            mOpTensorDesc,
                            &alpha,
                            std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                            outputGrad.getImpl()->rawPtr(),
                            &alpha,
                            mTensorDesc[1],
                            op.getInput(1)->getImpl()->rawPtr(),
                            &beta,
                            mTensorDesc[0],
                            op.getInput(0)->grad()->getImpl()->rawPtr()));

    // Input1_grad = output_grad * Input0
    CHECK_CUDNN_STATUS(cudnnOpTensor(CudaContext::cudnnHandle(),
                            mOpTensorDesc,
                            &alpha,
                            std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                            outputGrad.getImpl()->rawPtr(),
                            &alpha,
                            mTensorDesc[0],
                            op.getInput(0)->getImpl()->rawPtr(),
                            &beta,
                            mTensorDesc[1],
                            op.getInput(1)->grad()->getImpl()->rawPtr()));
}

Aidge::MulImpl_cuda::~MulImpl_cuda() {
    for (auto tensorDesc : mTensorDesc) {
        if (tensorDesc != nullptr) {
            cudnnDestroyTensorDescriptor(tensorDesc);
        }
    }

    if (mOpTensorDesc != nullptr) {
        cudnnDestroyOpTensorDescriptor(mOpTensorDesc);
    }
}
