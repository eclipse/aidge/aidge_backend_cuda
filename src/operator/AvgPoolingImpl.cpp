/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AvgPoolingImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
void Aidge::AvgPoolingImpl_cuda<DIM>::forward() {
    const AvgPooling_Op<DIM>& op = dynamic_cast<const AvgPooling_Op<DIM>&>(mOp);

    AIDGE_ASSERT(mOp.getRawInput(0), "missing input #0");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    // Lazy-initialize CuDNN AvgPooling descriptor
    if (mAvgPoolingDesc == nullptr) {
        const std::vector<int> strides(op.strideDims().begin(), op.strideDims().end());
        const std::vector<int> paddings(DIM, 0);
        const std::vector<int> window_dims(op.kernelDims().begin(), op.kernelDims().end());

        CHECK_CUDNN_STATUS(cudnnCreatePoolingDescriptor(&mAvgPoolingDesc));
        CHECK_CUDNN_STATUS(
            cudnnSetPoolingNdDescriptor(mAvgPoolingDesc,
                                        mMode,
                                        CUDNN_NOT_PROPAGATE_NAN,
                                        DIM,
                                        &window_dims[0],
                                        &paddings[0],
                                        &strides[0]));
    }

    // Do the actual forward computation
    // Template is only for scaling parameters, which are always in float
    // excepted when the convolution is performed in double precision.
    if (op.getOutput(0)->dataType() == DataType::Float64) {
        forward_<double>(input);
    }
    else {
        forward_<float>(input);
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::AvgPoolingImpl_cuda<DIM>::forward_(const Tensor& input) {
    const AvgPooling_Op<DIM>& op = dynamic_cast<const AvgPooling_Op<DIM>&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;
    CHECK_CUDNN_STATUS(
        cudnnPoolingForward(
            CudaContext::cudnnHandle(),
            mAvgPoolingDesc,
            &alpha,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
            input.getImpl()->rawPtr(),
            &beta,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
            op.getOutput(0)->getImpl()->rawPtr()
        )
    );
}

template <Aidge::DimIdx_t DIM>
void Aidge::AvgPoolingImpl_cuda<DIM>::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(mAvgPoolingDesc != nullptr, "AvgPool descriptor must be created during forward!");
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output grad #0");

    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    // Do the actual backward computation
    // Template is only for scaling parameters, which are always in float
    // excepted when the convolution is performed in double precision.
    if (op.getInput(0)->grad()->dataType() == DataType::Float64) {
        backward_<double>(output_grad);
    }
    else {
        backward_<float>(output_grad);
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::AvgPoolingImpl_cuda<DIM>::backward_(const Tensor& output_grad) 
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    const T alpha = 1.0f;
    const T beta = 1.0f; // accumulate

    CHECK_CUDNN_STATUS(
        cudnnPoolingBackward(CudaContext::cudnnHandle(),
                             mAvgPoolingDesc,
                             &alpha,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                             std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(output_grad.getImpl())->getCudnnTensorDesc(output_grad),
                             output_grad.getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getInput(0)->getImpl())->getCudnnTensorDesc(*op.getInput(0)),
                             op.getInput(0)->getImpl()->rawPtr(),
                             &beta,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getInput(0)->grad()->getImpl())->getCudnnTensorDesc(*op.getInput(0)),
                             op.getInput(0)->grad()->getImpl()->rawPtr()));
}

template <Aidge::DimIdx_t DIM>
Aidge::AvgPoolingImpl_cuda<DIM>::~AvgPoolingImpl_cuda() {
    if(mAvgPoolingDesc != nullptr)
        cudnnDestroyPoolingDescriptor(mAvgPoolingDesc);
}

// Template declarations
template class Aidge::AvgPoolingImpl_cuda<2>;
