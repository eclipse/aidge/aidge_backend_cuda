/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/PadImpl_CUDA_kernels.hpp"

template <typename T>
__global__ void cudaPadding_kernel(unsigned int nbOutputs,
                                   unsigned int outputWidth,
                                   unsigned int outputHeight,
                                   unsigned int nbChannels,
                                   unsigned int inputWidth,
                                   unsigned int inputHeight,
                                   int leftPad,
                                   int topPad,
                                   unsigned int padType,
                                   T padValue,
                                   const T *input,
                                   T *outputs,
                                   const T alpha,
                                   const T beta)
{
    const unsigned int inputOffset = (blockIdx.z * blockDim.z + threadIdx.z) * nbChannels * inputWidth * inputHeight;

    const unsigned int outputOffset = (blockIdx.z * blockDim.z + threadIdx.z) * nbOutputs * outputWidth * outputHeight;

    // nbCh = nbChannels for propagate
    //      = nbOutputs for back-propagate
    const unsigned int nbCh = min(nbChannels, nbOutputs);

    for (unsigned int ch = blockIdx.x; ch < nbCh; ch += gridDim.x)
    {
        for (unsigned int oy = threadIdx.y; oy < outputHeight; oy += blockDim.y)
        {
            for (unsigned int ox = threadIdx.x; ox < outputWidth; ox += blockDim.x)
            {
                T outputValue = padValue;

                if (padType == 0) // Const padding
                {
                    int ix = (int)ox - leftPad;
                    int iy = (int)oy - topPad;

                    if (ix >= 0 && ix < (int)inputWidth && iy >= 0 && iy < (int)inputHeight)
                    {
                        int inputIndex = ix + iy * inputWidth + ch * inputWidth * inputHeight + inputOffset;
                        outputValue = input[inputIndex];
                    }
                }
                else if (padType == 1) // Edge padding
                {
                    int ix = max(0, min((int)inputWidth - 1, (int)ox - leftPad));
                    int iy = max(0, min((int)inputHeight - 1, (int)oy - topPad));

                    int inputIndex = ix + iy * inputWidth + ch * inputWidth * inputHeight + inputOffset;
                    outputValue = input[inputIndex];
                }
                else if (padType == 2) // Reflect padding
                {
                    int ix = (int)ox - leftPad;
                    int iy = (int)oy - topPad;

                    if (ix < 0)
                        ix = 0 - ix;
                    if (iy < 0)
                        iy = 0 - iy;
                    if (ix >= (int)inputWidth)
                        ix = (int)inputWidth - ix;
                    if (iy >= (int)inputHeight)
                        iy = (int)inputHeight - iy;

                    int inputIndex = ix + iy * inputWidth + ch * inputWidth * inputHeight + inputOffset;
                    outputValue = input[inputIndex];
                }
                else if (padType == 3) // Wrap padding
                {
                    int ix = (inputWidth + (int)ox - leftPad) % inputWidth;
                    int iy = (inputHeight + (int)oy - topPad) % inputHeight;

                    int inputIndex = ix + iy * inputWidth + ch * inputWidth * inputHeight + inputOffset;
                    outputValue = input[inputIndex];
                }

                int outputIndex = ox + oy * outputWidth + ch * outputWidth * outputHeight + outputOffset;

                // old : outputs[outputIndex] = outputValue;
                outputs[outputIndex] = alpha * outputValue + beta * outputs[outputIndex];
            }
        }
    }
}

template <> // double
void Aidge::cudaPadding(const cudaDeviceProp &deviceProp,
                        unsigned int nbOutputs,
                        unsigned int outputsWidth,
                        unsigned int outputsHeight,
                        unsigned int nbChannels,
                        unsigned int batchSize,
                        unsigned int inputWidth,
                        unsigned int inputHeight,
                        int leftPad,
                        int topPad,
                        unsigned int padType,
                        double padValue,
                        const double *input,
                        double *outputs,
                        const double alpha,
                        const double beta)
{
    const unsigned int maxSize = (unsigned int)deviceProp.maxThreadsPerBlock;
    const unsigned int prefMultiple = (unsigned int)deviceProp.warpSize;

    const unsigned int groupSize = (outputsWidth * outputsHeight < maxSize)
                                       ? outputsWidth * outputsHeight
                                       : maxSize;

    const unsigned int reqWidth = (unsigned int)ceilf((float)groupSize / (float)outputsWidth);

    const unsigned int groupWidth = min(prefMultiple, reqWidth);
    const dim3 blocksPerGrid = {nbChannels, 1, batchSize};
    const dim3 threadsPerBlocks = {groupWidth, groupSize / groupWidth, 1};

    cudaPadding_kernel<<<blocksPerGrid, threadsPerBlocks>>>(nbOutputs,
                                                            outputsWidth,
                                                            outputsHeight,
                                                            nbChannels,
                                                            inputWidth,
                                                            inputHeight,
                                                            leftPad,
                                                            topPad,
                                                            padType,
                                                            padValue,
                                                            input,
                                                            outputs,
                                                            alpha,
                                                            beta);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <> // float
void Aidge::cudaPadding(const cudaDeviceProp &deviceProp,
                        unsigned int nbOutputs,
                        unsigned int outputsWidth,
                        unsigned int outputsHeight,
                        unsigned int nbChannels,
                        unsigned int batchSize,
                        unsigned int inputWidth,
                        unsigned int inputHeight,
                        int leftPad,
                        int topPad,
                        unsigned int padType,
                        float padValue,
                        const float *input,
                        float *outputs,
                        const float alpha,
                        const float beta)
{
    const unsigned int maxSize = (unsigned int)deviceProp.maxThreadsPerBlock;
    const unsigned int prefMultiple = (unsigned int)deviceProp.warpSize;

    const unsigned int groupSize = (outputsWidth * outputsHeight < maxSize)
                                       ? outputsWidth * outputsHeight
                                       : maxSize;

    const unsigned int reqWidth = (unsigned int)ceilf((float)groupSize / (float)outputsWidth);

    const unsigned int groupWidth = min(prefMultiple, reqWidth);
    const dim3 blocksPerGrid = {nbChannels, 1, batchSize};
    const dim3 threadsPerBlocks = {groupWidth, groupSize / groupWidth, 1};

    cudaPadding_kernel<<<blocksPerGrid, threadsPerBlocks>>>(nbOutputs,
                                                            outputsWidth,
                                                            outputsHeight,
                                                            nbChannels,
                                                            inputWidth,
                                                            inputHeight,
                                                            leftPad,
                                                            topPad,
                                                            padType,
                                                            padValue,
                                                            input,
                                                            outputs,
                                                            alpha,
                                                            beta);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <> // half
void Aidge::cudaPadding(const cudaDeviceProp &deviceProp,
                        unsigned int nbOutputs,
                        unsigned int outputsWidth,
                        unsigned int outputsHeight,
                        unsigned int nbChannels,
                        unsigned int batchSize,
                        unsigned int inputWidth,
                        unsigned int inputHeight,
                        int leftPad,
                        int topPad,
                        unsigned int padType,
                        half padValue,
                        const half *input,
                        half *outputs,
                        const half alpha,
                        const half beta)
{
    const unsigned int maxSize = (unsigned int)deviceProp.maxThreadsPerBlock;
    const unsigned int prefMultiple = (unsigned int)deviceProp.warpSize;

    const unsigned int groupSize = (outputsWidth * outputsHeight < maxSize)
                                       ? outputsWidth * outputsHeight
                                       : maxSize;

    const unsigned int reqWidth = (unsigned int)ceilf((float)groupSize / (float)outputsWidth);

    const unsigned int groupWidth = min(prefMultiple, reqWidth);
    const dim3 blocksPerGrid = {nbChannels, 1, batchSize};
    const dim3 threadsPerBlocks = {groupWidth, groupSize / groupWidth, 1};

    cudaPadding_kernel<<<blocksPerGrid, threadsPerBlocks>>>(nbOutputs,
                                                            outputsWidth,
                                                            outputsHeight,
                                                            nbChannels,
                                                            inputWidth,
                                                            inputHeight,
                                                            leftPad,
                                                            topPad,
                                                            padType,
                                                            padValue,
                                                            input,
                                                            outputs,
                                                            alpha,
                                                            beta);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}