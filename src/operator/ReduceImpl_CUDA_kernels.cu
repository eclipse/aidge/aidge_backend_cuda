/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/ReduceImpl_CUDA_kernels.hpp"

template <typename T>
__global__ void duplicateElements(
    const T* input, 
    T* output, 
    const std::size_t* shape, 
    const std::size_t* new_shape, 
    const int* axes, 
    const std::size_t* factors, 
    int num_dims, 
    int num_axes,
    const T alpha,
    const T beta)     
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int input_size = 1;
    int output_size = 1;

    for (int i = 0; i < num_dims; ++i) {
        input_size *= shape[i];
        output_size *= new_shape[i];
    }

    if (idx >= output_size) return;

    int* out_idx = new int[num_dims];
    int* in_idx = new int[num_dims];
    int remaining_idx = idx;

    for (int i = num_dims - 1; i >= 0; --i) {
        out_idx[i] = remaining_idx % new_shape[i];
        remaining_idx /= new_shape[i];
    }

    for (int i = 0; i < num_dims; ++i) {
        in_idx[i] = out_idx[i];
    }

    for (int i = 0; i < num_axes; ++i) {
        int axis = axes[i];
        int factor = factors[i];
        in_idx[axis] = out_idx[axis] / factor;
    }

    int in_linear_idx = 0;
    int out_linear_idx = 0;
    int input_stride = 1;
    int output_stride = 1;

    for (int i = num_dims - 1; i >= 0; --i) {
        in_linear_idx += in_idx[i] * input_stride;
        out_linear_idx += out_idx[i] * output_stride;
        input_stride *= shape[i];
        output_stride *= new_shape[i];
    }

    // old : output[out_linear_idx] = input[in_linear_idx];
    output[out_linear_idx] = alpha * input[in_linear_idx] + beta * output[out_linear_idx];

    delete[] out_idx;
    delete[] in_idx;
}

template <typename T>
void Aidge::ReduceBackward(
    const T* input, 
    T* output, 
    const std::vector<std::size_t>& inputDims, 
    const std::vector<std::size_t>& outputDims, 
    const std::vector<int>& axes, 
    const std::vector<std::size_t>& factors, 
    int outSize,
    const T alpha,
    const T beta)    
{
    std::size_t* d_shape;
    std::size_t* d_new_shape;
    int* d_axes;
    std::size_t* d_factors;
    cudaMalloc(&d_shape, inputDims.size() * sizeof(std::size_t));
    cudaMalloc(&d_new_shape, outputDims.size() * sizeof(std::size_t));
    cudaMalloc(&d_axes, axes.size() * sizeof(int));
    cudaMalloc(&d_factors, axes.size() * sizeof(std::size_t));

    cudaMemcpy(d_shape, inputDims.data(), inputDims.size() * sizeof(std::size_t), cudaMemcpyHostToDevice);
    cudaMemcpy(d_new_shape, outputDims.data(), outputDims.size() * sizeof(std::size_t), cudaMemcpyHostToDevice);
    cudaMemcpy(d_axes, axes.data(), axes.size() * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_factors, factors.data(), axes.size() * sizeof(std::size_t), cudaMemcpyHostToDevice);

    int blockSize = 256;
    int numBlocks = (outSize + blockSize - 1) / blockSize;

    duplicateElements<<<numBlocks, blockSize>>> (
        input, 
        output, 
        d_shape, 
        d_new_shape, 
        d_axes, 
        d_factors, 
        static_cast<int>(inputDims.size()), 
        static_cast<int>(axes.size()),
        alpha,
        beta);

    cudaFree(d_shape);
    cudaFree(d_new_shape);
    cudaFree(d_axes);
    cudaFree(d_factors);
}


template void Aidge::ReduceBackward(const double* input,
                               double* output,
                               const std::vector<std::size_t>& inputDims,
                               const std::vector<std::size_t>& outputDims,
                               const std::vector<int>& axes,
                               const std::vector<std::size_t>& factors,
                               int outSize,                       
                               const double alpha,
                               const double beta);

template void Aidge::ReduceBackward(const float* input,
                               float* output,
                               const std::vector<std::size_t>& inputDims,
                               const std::vector<std::size_t>& outputDims,
                               const std::vector<int>& axes,
                               const std::vector<std::size_t>& factors,
                               int outSize,                       
                               const float alpha,
                               const float beta);


template void Aidge::ReduceBackward(const half* input,
                               half* output,
                               const std::vector<std::size_t>& inputDims,
                               const std::vector<std::size_t>& outputDims,
                               const std::vector<int>& axes,
                               const std::vector<std::size_t>& factors,
                               int outSize,                       
                               const half alpha,
                               const half beta);
