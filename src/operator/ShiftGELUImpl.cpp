/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>
#include <algorithm>  // For std::max
#include <cmath>      // For pow
#include <typeinfo>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ShiftGELUImpl.hpp"
#include "aidge/backend/cuda/operator/ShiftGELUImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/ShiftGELU.hpp"
#include "aidge/utils/Types.h"

void Aidge::ShiftGELUImpl_cuda::forward() {

    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    assert(mOp.getRawInput(0) && "missing input #0");
    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input);
            break;
        case DataType::Float32:
            forward_<float>(input);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template<class T>
void Aidge::ShiftGELUImpl_cuda::forward_(const Tensor& input)
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const T * input_raw = static_cast<const T*>(input.getImpl()->rawPtr());
    T * output = static_cast<T*>(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());

    int N = 15;
    int output_bits = 8;
    size_t size = input.size();
    std::vector<DimSize_t> dims_input = input.dims();

    // maybe find a most efficient way to compute scaling factor (a max and min function could help to retrieve scaling factor value)

    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::min();
    for(std::size_t i = 0; i < dims_input[0]; i++) {
        for(std::size_t j = 0; j < dims_input[1]; j++) {
            for(std::size_t k = 0; k < dims_input[2]; k++) {
                for(std::size_t l = 0; l < dims_input[3]; l++) {
                    std::vector<std::size_t> coordIdx = {i, j, k, l};
                    std::size_t newFlatIdx = input.getIdx(coordIdx);
                    if (newFlatIdx < min) {
                        min = newFlatIdx;
                    }
                    if (newFlatIdx > max) {
                        max = newFlatIdx;
                    }
               }
            }     
        }
    }

    double m = std::max(std::abs(min), std::abs(max));
    double normalization_factor = static_cast<double>(1 << (output_bits - 1)) - 1;
    double scaling_factor =  m / normalization_factor;

    // The new scaling factor that we can use to dequantify the returned tensor (not used here)
    // double new_SF = 1/std::pow(2,2*output_bits-1);

    ShiftGELUforward(input_raw, output, scaling_factor,N, output_bits, size, dims_input);
}

void Aidge::ShiftGELUImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    assert(op.getOutput(0)->grad() && "missing output #0");

    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    if (op.getInput(0)->grad()->dataType() == DataType::Float64) {
        backward_<double>(output_grad);
    }
    else {
        backward_<float>(output_grad);
    }
}

template <class T>
void Aidge::ShiftGELUImpl_cuda::backward_(const Tensor& output_grad) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const T * input = static_cast<const T*>(std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr());

    size_t size = output_grad.size();

    T * output = static_cast<T*>(op.getInput(0)->grad()->getImpl()->rawPtr());

    const T * output_grad_raw = static_cast<const T*>(output_grad.getImpl()->rawPtr());
    ShiftGELUbackward(input, output_grad_raw, output, size);

}