/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/SoftmaxImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/utils/Types.h"

void Aidge::SoftmaxImpl_cuda::forward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(op.getInput(0), "Missing input in Softmax operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "Cannot run Softmax forward because the input has no implementation.");

    const auto& input = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->refCastFrom(mInputFallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));

    const Softmax_Op& softmaxOp = static_cast<const Softmax_Op&>(mOp);
    auto axis =  softmaxOp.axis();

    AIDGE_ASSERT(axis == 1, "Softmax is only supported on Channels currently for Backend Cuda.");

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input);
            break;
        case DataType::Float32:
            forward_<float>(input);
            break;
        case DataType::Float16:
            forward_<half>(input);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda.");
    }
}


template <class T>
void Aidge::SoftmaxImpl_cuda::forward_(const Tensor& input) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    // Set the algorithm and mode for softmax
    cudnnSoftmaxAlgorithm_t algo = CUDNN_SOFTMAX_ACCURATE;
    cudnnSoftmaxMode_t mode = CUDNN_SOFTMAX_MODE_CHANNEL;

    AIDGE_ASSERT(input.nbDims() > 1 && input.nbDims() < 6,
                "Tensor dimension {}  is not yet supported, dimensions range supported for Softmax Backend Cuda is [2,5].",
                input.nbDims());

    // Call the softmax forward function
    CHECK_CUDNN_STATUS(cudnnSoftmaxForward(
        CudaContext::cudnnHandle(),
        algo,
        mode,
        &alpha,
        std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
        input.getImpl()->rawPtr(),
        &beta,
        std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
        std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()));
}

void Aidge::SoftmaxImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing outputGrad in Softmax operator");
    AIDGE_ASSERT(op.getOutput(0)->grad()->hasImpl(), "cannot run Softmax backward because the output grad has no implementation.");

    const auto& outGrad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getInput(0)->grad());

    const Softmax_Op& softmaxOp = static_cast<const Softmax_Op&>(mOp);
    auto axis =  softmaxOp.axis();
    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            backward_<double>(outGrad, axis);
            break;
        case DataType::Float32:
            backward_<float>(outGrad, axis);
            break;
        case DataType::Float16:
            backward_<half>(outGrad, axis);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::SoftmaxImpl_cuda::backward_(const Tensor& outGrad, int axis) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    // const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    // const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;
    const T * outputGrad = static_cast<const T*>(op.getOutput(0)->grad()->getImpl()->rawPtr());
    T * inputGrad = static_cast<T*>(op.getInput(0)->grad()->getImpl()->rawPtr());
    //TODO
}
