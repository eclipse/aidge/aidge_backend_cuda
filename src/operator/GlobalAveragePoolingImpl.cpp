/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/GlobalAveragePoolingImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/GlobalAveragePooling.hpp"
#include "aidge/utils/Types.h"

void Aidge::GlobalAveragePoolingImpl_cuda::forward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(mOp.getRawInput(0), "missing input #0");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    // Lazy-initialize CuDNN GlobalAveragePooling descriptor
    if (mGlobalAveragePoolingDesc == nullptr) {
        int poolingDims = 2; // Assuming 2D pooling
        int windowDims[2] = {static_cast<int>(input.dims().at(2)), static_cast<int>(input.dims().at(3))}; // Pooling window dimensions matching spatial dimensions of input tensor
        int padding[2] = {0, 0}; // No padding
        int stride[2] = {1, 1}; // Stride of 1
        CHECK_CUDNN_STATUS(cudnnCreatePoolingDescriptor(&mGlobalAveragePoolingDesc));
        CHECK_CUDNN_STATUS(
            cudnnSetPoolingNdDescriptor(mGlobalAveragePoolingDesc, mMode, CUDNN_NOT_PROPAGATE_NAN, poolingDims, windowDims, padding, stride)
            // cudnnSetPooling2dDesccomputedOutputriptor(mGlobalAveragePoolingDesc, mMode, CUDNN_NOT_PROPAGATE_NAN, 1, 1, 0, 0, 1, 1)
        );
    }

    if (op.getOutput(0)->dataType() == DataType::Float64) {
        forward_<double>(input);
    }
    else {
        forward_<float>(input);
    }
}

template <class T>
void Aidge::GlobalAveragePoolingImpl_cuda::forward_(const Tensor& input) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    CHECK_CUDNN_STATUS(
        cudnnPoolingForward(
            CudaContext::cudnnHandle(),
            mGlobalAveragePoolingDesc,
            &alpha,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
            input.getImpl()->rawPtr(),
            &beta,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
            std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()
        )
    );
}

void Aidge::GlobalAveragePoolingImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(mGlobalAveragePoolingDesc != nullptr, "GlobalAvgPool descriptor must be created during forward!");
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output grad #0");

    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    if (op.getOutput(0)->dataType() == DataType::Float64) {
        backward_<double>(output_grad);
    }
    else {
        backward_<float>(output_grad);
    }
}

template <class T>
void Aidge::GlobalAveragePoolingImpl_cuda::backward_(const Tensor& output_grad) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    const T alpha = 1.0f;
    const T beta = 1.0f;  // accumulate
    CHECK_CUDNN_STATUS(
        cudnnPoolingBackward(CudaContext::cudnnHandle(),
                             mGlobalAveragePoolingDesc,
                             &alpha,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                             std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(output_grad.getImpl())->getCudnnTensorDesc(output_grad),
                             output_grad.getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getInput(0)->getImpl())->getCudnnTensorDesc(*op.getInput(0)),
                             op.getInput(0)->getImpl()->rawPtr(),
                             &beta,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getInput(0)->grad()->getImpl())->getCudnnTensorDesc(*op.getInput(0)),
                             op.getInput(0)->grad()->getImpl()->rawPtr()));
}

Aidge::GlobalAveragePoolingImpl_cuda::~GlobalAveragePoolingImpl_cuda() {
    if(mGlobalAveragePoolingDesc != nullptr)
        cudnnDestroyPoolingDescriptor(mGlobalAveragePoolingDesc);
}

