/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/BatchNormImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
void Aidge::BatchNormImpl_cuda<DIM>::forward() {
    // FIXME: uncomment the following code once memory handling will work
    AIDGE_ASSERT(mOp.getRawInput(0), "missing input #0");
    AIDGE_ASSERT(mOp.getRawInput(1), "missing input #1");
    AIDGE_ASSERT(mOp.getRawInput(2), "missing input #2");
    AIDGE_ASSERT(mOp.getRawInput(3), "missing input #3");
    AIDGE_ASSERT(mOp.getRawInput(4), "missing input #4");


    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback, input3Fallback, input4Fallback;
    const auto& input0 = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->refCastFrom(input0Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& input1 = std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->refCastFrom(input1Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& input2 = std::static_pointer_cast<Tensor>(mOp.getRawInput(2))->refCastFrom(input2Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& input3 = std::static_pointer_cast<Tensor>(mOp.getRawInput(3))->refCastFrom(input3Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& input4 = std::static_pointer_cast<Tensor>(mOp.getRawInput(4))->refCastFrom(input4Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));

   if (mBNDesc == nullptr)
    {
        const BatchNorm_Op<DIM>& bnOp = static_cast<const BatchNorm_Op<DIM>&>(mOp);
        mEpsilon = static_cast<double>(bnOp.epsilon());
        mMode = CUDNN_BATCHNORM_SPATIAL;

        // CUDNN_BN_MIN_EPSILON is set to 0.0 since cuDNN 7.5.0
        if (CUDNN_BN_MIN_EPSILON > 0.0 && mEpsilon < CUDNN_BN_MIN_EPSILON) {
            mEpsilon = CUDNN_BN_MIN_EPSILON;
        }

        CHECK_CUDNN_STATUS(cudnnCreateTensorDescriptor(&mBNDesc));
        CHECK_CUDNN_STATUS(cudnnDeriveBNTensorDescriptor(
            mBNDesc, std::dynamic_pointer_cast<TensorImpl_cuda_>(input0.getImpl())->getCudnnTensorDesc(input0), mMode));


        cudnnDataType_t dataType;
        const unsigned int nbDimsRequested = DIM;
        std::vector<int> dims(nbDimsRequested);
        std::vector<int> strides(nbDimsRequested);
        int nbDims;
        CHECK_CUDNN_STATUS(cudnnGetTensorNdDescriptor(mBNDesc,
                                                    nbDimsRequested,
                                                    &dataType,
                                                    &nbDims,
                                                    &dims[0],
                                                    &strides[0]));
        dims.resize(nbDims);
        strides.resize(nbDims);
    }

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input0, input1, input2, input3, input4);
            break;
        case DataType::Float32:
            forward_<float>(input0, input1, input2, input3, input4);
            break;
        case DataType::Float16:
            forward_<half>(input0, input1, input2, input3, input4);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::BatchNormImpl_cuda<DIM>::forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2, const Tensor& input3, const Tensor& input4) {

    const BatchNorm_Op<DIM>& op = static_cast<const BatchNorm_Op<DIM>&>(mOp);
    
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    cudnnTensorDescriptor_t tensorDesc;
    // For scale, bias, var and mean, if we have a 1D tensor, the dim should go on the channels
    if (input1.nbDims() == 1)
    {
        CHECK_CUDNN_STATUS(cudnnCreateTensorDescriptor(&tensorDesc));
        const std::vector<int> dims = {1, static_cast<int>(input1.size()),1, 1};
        const std::vector<int> strides = {static_cast<int>(input1.size()), 1, 1, 1};
        CHECK_CUDNN_STATUS(cudnnSetTensorNdDescriptor(tensorDesc, CudaContext::data_type<T>::value, dims.size(), dims.data(), strides.data()));
    }
    else {
        tensorDesc = std::dynamic_pointer_cast<TensorImpl_cuda_>(input1.getImpl())->getCudnnTensorDesc(input1);
    }

    if (op.trainingMode())
    {
        CHECK_CUDNN_STATUS(
            cudnnBatchNormalizationForwardTraining(
                CudaContext::cudnnHandle(),
                mMode,
                &alpha,
                &beta,
                std::dynamic_pointer_cast<TensorImpl_cuda_>(input0.getImpl())->getCudnnTensorDesc(input0),
                input0.getImpl()->rawPtr(),
                std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr(),
                tensorDesc,
                input1.getImpl()->rawPtr(),
                input2.getImpl()->rawPtr(), 
                op.momentum(),
                input3.getImpl()->rawPtr(),
                input4.getImpl()->rawPtr(),                
                mEpsilon,
                nullptr,
                nullptr) // TODO add savedMean and savedVar?
        );
    }
    else
    {
        CHECK_CUDNN_STATUS(
            cudnnBatchNormalizationForwardInference(
                    CudaContext::cudnnHandle(),
                    mMode,
                    &alpha,
                    &beta,
                    std::dynamic_pointer_cast<TensorImpl_cuda_>(input0.getImpl())->getCudnnTensorDesc(input0),
                    input0.getImpl()->rawPtr(),
                    std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                    std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr(),
                    tensorDesc,
                    input1.getImpl()->rawPtr(),
                    input2.getImpl()->rawPtr(),
                    input3.getImpl()->rawPtr(),
                    input4.getImpl()->rawPtr(),
                    mEpsilon)
        );
    }

    if (input1.nbDims() == 1)
    {
        CHECK_CUDNN_STATUS(cudnnDestroyTensorDescriptor(tensorDesc));
    }
}

template <Aidge::DimIdx_t DIM>
void Aidge::BatchNormImpl_cuda<DIM>::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(mBNDesc != nullptr, "BatchNorm descriptor must be created during forward!");
    for (IOIndex_t i = 0; i < (op.nbInputs() - 2); ++i) {
        AIDGE_ASSERT(op.getInput(i), "missing input # {} in BatchNorm operator", i);
        AIDGE_ASSERT(op.getInput(i)->hasImpl(), "cannot run BatchNorm backward because the {}-th input has no implementation.", i);
    }
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing outputGrad in BatchNorm operator");
    AIDGE_ASSERT(op.getOutput(0)->grad()->hasImpl(), "cannot run BatchNorm backward because the output grad has no implementation.");

    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback, outputGradFallback;
    const auto& input0 = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->refCastFrom(input0Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& weights = std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->refCastFrom(input1Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& bias = std::static_pointer_cast<Tensor>(mOp.getRawInput(2))->refCastFrom(input2Fallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const auto& outputGrad = op.getOutput(0)->grad()->refCastFrom(outputGradFallback, *op.getOutput(0)->grad());


    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            backward_<double>(input0, outputGrad, weights);
            break;
        case DataType::Float32:
            backward_<float>(input0, outputGrad, weights);
            break;
        case DataType::Float16:
            backward_<half>(input0, outputGrad, weights);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::BatchNormImpl_cuda<DIM>::backward_(const Tensor& input0, const Tensor& outputGrad, const Tensor& weights) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 1.0f;     // accumulate
    const typename Cuda::cudnn_scaling_type<T>::type alphaData = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type betaData = 1.0f; // accumulate

    cudnnTensorDescriptor_t scaleBiasDesc;
    // For scale, bias, var and mean, if we have a 1D tensor, the dim should go on the channels
    if (weights.nbDims() == 1)
    {
        CHECK_CUDNN_STATUS(cudnnCreateTensorDescriptor(&scaleBiasDesc));
        const std::vector<int> dims = {1, static_cast<int>(weights.size()),1, 1};
        const std::vector<int> strides = {static_cast<int>(weights.size()), 1, 1, 1};
        CHECK_CUDNN_STATUS(cudnnSetTensorNdDescriptor(scaleBiasDesc, CudaContext::data_type<T>::value, dims.size(), dims.data(), strides.data()));
    }
    else {
        scaleBiasDesc = std::dynamic_pointer_cast<TensorImpl_cuda_>(weights.getImpl())->getCudnnTensorDesc(weights);
    }

    CHECK_CUDNN_STATUS(
        cudnnBatchNormalizationBackward(
                CudaContext::cudnnHandle(),
                mMode,
                &alphaData,
                &betaData,
                &alpha,
                &beta,
                std::dynamic_pointer_cast<TensorImpl_cuda_>(input0.getImpl())->getCudnnTensorDesc(input0),
                input0.getImpl()->rawPtr(),
                std::dynamic_pointer_cast<TensorImpl_cuda_>(outputGrad.getImpl())->getCudnnTensorDesc(outputGrad),
                outputGrad.getImpl()->rawPtr(),
                std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getInput(0)->grad()->getImpl())->getCudnnTensorDesc(*op.getInput(0)),
                op.getInput(0)->grad()->getImpl()->rawPtr(),
                scaleBiasDesc,
                weights.getImpl()->rawPtr(),
                op.getInput(1)->grad()->getImpl()->rawPtr(),
                op.getInput(2)->grad()->getImpl()->rawPtr(),
                mEpsilon,
                nullptr,
                nullptr) // TODO add savedMean and savedVar?
    );
    if (weights.nbDims() == 1)
    {
        CHECK_CUDNN_STATUS(cudnnDestroyTensorDescriptor(scaleBiasDesc));
    }
}

template <Aidge::DimIdx_t DIM>
Aidge::BatchNormImpl_cuda<DIM>::~BatchNormImpl_cuda() {
    if(mBNDesc != nullptr)
    {
        cudnnDestroyTensorDescriptor(mBNDesc);
    }
}

// Template declarations
template class Aidge::BatchNormImpl_cuda<2>;
