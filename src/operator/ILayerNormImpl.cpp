/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>
#include <algorithm>  // For std::max
#include <cmath>      // For pow
#include <typeinfo>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ILayerNormImpl.hpp"
#include "aidge/backend/cuda/operator/ILayerNormImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/ILayerNorm.hpp"
#include "aidge/utils/Types.h"

void Aidge::ILayerNormImpl_cuda::forward() {


    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    assert(mOp.getRawInput(0) && "missing input #0");
    assert(mOp.getRawInput(1) && "missing input #1");
    assert(mOp.getRawInput(2) && "missing input #2");

    const auto& input0 = op.getInput(0)->refCastFrom(mInput0Fallback, *op.getOutput(0));
    const auto& input1 = op.getInput(1)->refCastFrom(mInput1Fallback, *op.getOutput(0));
    const auto& input2 = op.getInput(2)->refCastFrom(mInput2Fallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input0, input1, input2);
            break;
        case DataType::Float32:
            forward_<float>(input0, input1, input2);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}


template<class T>
void Aidge::ILayerNormImpl_cuda::forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2)
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const T * input_raw = static_cast<const T*>(input0.getImpl()->rawPtr());
    const T * weight = static_cast<const T*>(input1.getImpl()->rawPtr());
    const T * bias = static_cast<const T*>(input2.getImpl()->rawPtr());
    T * output = static_cast<T*>(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());

    int N = 15;
    int output_bits = 8;
    size_t size = input0.size();
    std::vector<DimSize_t> dims_input = input0.dims();

    // maybe find a most efficient way to compute scaling factor (a max and min function could help to retrieve scaling factor value)

    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::min();
    for(std::size_t i = 0; i < dims_input[0]; i++) {
        for(std::size_t j = 0; j < dims_input[1]; j++) {
            for(std::size_t k = 0; k < dims_input[2]; k++) {
                for(std::size_t l = 0; l < dims_input[3]; l++) {
                    std::vector<std::size_t> coordIdx = {i, j, k, l};
                    std::size_t newFlatIdx = input0.getIdx(coordIdx);
                    if (newFlatIdx < min) {
                        min = newFlatIdx;
                    }
                    if (newFlatIdx > max) {
                        max = newFlatIdx;
                    }
               }
            }     
        }
    }
    double m = std::max(std::abs(min), std::abs(max));
    double normalization_factor = static_cast<double>(1 << (output_bits - 1)) - 1;
    double scaling_factor =  m / normalization_factor;
    
    // The new scaling factor that we can use to dequantify the returned tensor (not used here)
    // double new_SF = 1/std::pow(2,2*output_bits-1); 

    ILayerNormforward(input_raw, output, scaling_factor, weight, bias, size, dims_input);
}

void Aidge::ILayerNormImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    assert(op.getOutput(0)->grad() && "missing output #0");

    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    if (op.getInput(0)->grad()->dataType() == DataType::Float64) {
        backward_<double>(output_grad);
    }
    else {
        backward_<float>(output_grad);
    }
}

template <class T>
void Aidge::ILayerNormImpl_cuda::backward_(const Tensor& output_grad) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    size_t size = output_grad.size();
    std::vector<DimSize_t> dims_input = output_grad.dims();

    const T * output = static_cast<const T*>(std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr());

    T * input_grad = static_cast<T*>(op.getInput(0)->grad()->getImpl()->rawPtr());
    T * weight_grad = static_cast<T*>(op.getInput(1)->grad()->getImpl()->rawPtr());
    T * bias_grad = static_cast<T*>(op.getInput(2)->grad()->getImpl()->rawPtr());
    
    const T * input = static_cast<const T*>(op.getInput(0)->getImpl()->rawPtr());
    const T * weight = static_cast<const T*>(op.getInput(1)->getImpl()->rawPtr());
    const T * bias = static_cast<const T*>(op.getInput(2)->getImpl()->rawPtr());

    // maybe find a most efficient way to compute mean and variance tensor

    std::vector<std::vector<std::vector<std::vector<T>>>> means(dims_input[0],
        std::vector<std::vector<std::vector<T>>>(dims_input[1],
            std::vector<std::vector<T>>(dims_input[2],
                std::vector<T>(dims_input[3], 0.0f))));

    for (std::size_t i = 0; i < dims_input[0]; i++) {
        for (std::size_t j = 0; j < dims_input[1]; j++) {
            for (std::size_t k = 0; k < dims_input[2]; k++) {
                T sum = 0.0f;

                for (std::size_t l = 0; l < dims_input[3]; l++) {
                    std::vector<std::size_t> coordIdx = {i, j, k, l};
                    sum += output_grad.getIdx(coordIdx);
                }
                for (std::size_t l = 0; l < dims_input[3]; l++) {
                    std::vector<std::size_t> coordIdx = {i, j, k, l};
                    means[i][j][k][l] = sum / static_cast<T>(dims_input[3]);
                }
            }
        }
    }
    std::vector<T> flat_means;

    for (const auto &vec3d : means) {
        for (const auto &vec2d : vec3d) {
            for (const auto &vec1d : vec2d) {
                flat_means.insert(flat_means.end(), vec1d.begin(), vec1d.end());
            }
        }
    }

    std::vector<std::vector<std::vector<std::vector<T>>>> vars(dims_input[0],
        std::vector<std::vector<std::vector<T>>>(dims_input[1],
            std::vector<std::vector<T>>(dims_input[2],
                std::vector<T>(dims_input[3], 0.0f))));
    
    for (std::size_t i = 0; i < dims_input[0]; i++) {
        for (std::size_t j = 0; j < dims_input[1]; j++) {
            for (std::size_t k = 0; k < dims_input[2]; k++) {
                T sum_sq_diff = 0.0f;

                for (std::size_t l = 0; l < dims_input[3]; l++) {
                    std::vector<std::size_t> coordIdx = {i, j, k, l};
                    T value = static_cast<T>(output_grad.getIdx(coordIdx));
                    T diff = value - means[i][j][k][l];
                    sum_sq_diff += diff * diff;
                }
                T variance = sum_sq_diff / static_cast<T>(dims_input[3]);
                for (std::size_t l = 0; l < dims_input[3]; l++) {
                    vars[i][j][k][l] = variance;
                }
            }
        }
    }

    std::vector<T> flat_vars;

    for (const auto &vec3d : vars) {
        for (const auto &vec2d : vec3d) {
            for (const auto &vec1d : vec2d) {
                flat_vars.insert(flat_vars.end(), vec1d.begin(), vec1d.end());
            }
        }
    }

    const T* mean_ = flat_means.data();
    const T* var_ = flat_vars.data();
    const T * output_grad_raw = static_cast<const T*>(output_grad.getImpl()->rawPtr());

    ILayerNormbackward(output, output_grad_raw, input, mean_, var_, weight, bias, input_grad, weight_grad, bias_grad, size);
}
