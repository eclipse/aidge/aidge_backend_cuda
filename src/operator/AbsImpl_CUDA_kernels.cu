/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/AbsImpl_CUDA_kernels.hpp"

// Base template for floating-point types (float, double)
template<typename T>
__device__ T abs_helper(T x) {
    return std::abs(x);  // std::log works for both float and double
}

// Specialization for half-precision type using CUDA's half
template<>
__device__ half abs_helper<half>(half x) {
    float x_float = __half2float(x);  
    return __float2half(std::abs(x_float));  
}

template <class T>
__global__ void absKernel(const T* input, T* output, int size) 
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;
    output[idx] = abs_helper(input[idx]);
}

template <class T>
void Aidge::absForward(const T* input, T* output, int size)
{
    int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;
    absKernel<<<numBlocks, blockSize>>>(input, output, size);
};

template void Aidge::absForward<double>(const double* input, double* output, int size);

template void Aidge::absForward<float>(const float* input, float* output, int size);

template void Aidge::absForward<half>(const half* input, half* output, int size);