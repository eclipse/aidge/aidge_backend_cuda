/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/SqrtImpl.hpp"
#include "aidge/backend/cuda/operator/SqrtImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Sqrt.hpp"
#include "aidge/utils/Types.h"

void Aidge::SqrtImpl_cuda::forward()
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(op.getInput(0), "missing input #0");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input);
            break;
        case DataType::Float32:
            forward_<float>(input);
            break;
        case DataType::Float16:
            forward_<half>(input);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::SqrtImpl_cuda::forward_(const Tensor& input)
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    const T alpha = 1.0f;
    const T beta = 0.0f;

    const T * inputPtr = static_cast<const T*>(input.getImpl()->rawPtr());
    T * outputPtr = static_cast<T*>(op.getOutput(0)->getImpl()->rawPtr());

    Aidge::sqrtForward<T>(inputPtr,
                          outputPtr,
                          static_cast<int>(op.getOutput(0)->size()),
                          alpha,
                          beta);
}

void Aidge::SqrtImpl_cuda::backward()
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(op.getInput(0), "missing input #0");
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output #0 grad");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));
    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    switch(op.getInput(0)->grad()->dataType()) {
        case DataType::Float64:
            backward_<double>(input, output_grad);
            break;
        case DataType::Float32:
            backward_<float>(input, output_grad);
            break;
        case DataType::Float16:
            backward_<half>(input, output_grad);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::SqrtImpl_cuda::backward_(const Tensor& input, const Tensor& output_grad)
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    const T alpha = 1.0f;
    const T beta = 1.0f; // accumulate

    const T * inputPtr = static_cast<const T*>(input.getImpl()->rawPtr());
    const T * outputGradPtr = static_cast<const T*>(output_grad.getImpl()->rawPtr());
    T * inputGradPtr = static_cast<T*>(op.getInput(0)->grad()->getImpl()->rawPtr());

    Aidge::sqrtBackward<T>(inputPtr,
                           outputGradPtr,
                           inputGradPtr,
                           static_cast<int>(op.getOutput(0)->size()),
                           alpha,
                           beta);
}
