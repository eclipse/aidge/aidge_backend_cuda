/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/MaxPoolingImpl.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
void Aidge::MaxPoolingImpl_cuda<DIM>::forward() {
    const MaxPooling_Op<DIM>& op_ = static_cast<const MaxPooling_Op<DIM>&>(mOp);

    AIDGE_ASSERT(mOp.getRawInput(0), "missing input #0");

    const auto& input = op_.getInput(0)->refCastFrom(mInputFallback, *op_.getOutput(0));

    // Lazy-initialize CuDNN MaxPooling descriptor
    if (mMaxPoolingDesc == nullptr) {
        const std::vector<int> strides(op_.strideDims().begin(), op_.strideDims().end());
        const std::vector<int> paddings(DIM, 0);
        const std::vector<int> window_dims(op_.kernelDims().begin(), op_.kernelDims().end());

        CHECK_CUDNN_STATUS(cudnnCreatePoolingDescriptor(&mMaxPoolingDesc));
        CHECK_CUDNN_STATUS(
            cudnnSetPoolingNdDescriptor(mMaxPoolingDesc,
                                        mMode,
                                        CUDNN_NOT_PROPAGATE_NAN,
                                        DIM,
                                        &window_dims[0],
                                        &paddings[0],
                                        &strides[0]));
    }


    // Do the actual forward computation
    // Template is only for scaling parameters, which are always in float
    // excepted when the convolution is performed in double precision.
    if (op_.getOutput(0)->dataType() == DataType::Float64) {
        forward_<double>(input);
    }
    else {
        forward_<float>(input);
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::MaxPoolingImpl_cuda<DIM>::forward_(const Tensor& input) {
    const MaxPooling_Op<DIM>& op_ = static_cast<const MaxPooling_Op<DIM>&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;
    CHECK_CUDNN_STATUS(
        cudnnPoolingForward(
            CudaContext::cudnnHandle(),
            mMaxPoolingDesc,
            &alpha,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
            input.getImpl()->rawPtr(),
            &beta,
            std::dynamic_pointer_cast<TensorImpl_cuda_>(op_.getOutput(0)->getImpl())->getCudnnTensorDesc(*op_.getOutput(0)),
            op_.getOutput(0)->getImpl()->rawPtr()
        )
    );
}

template <Aidge::DimIdx_t DIM>
void Aidge::MaxPoolingImpl_cuda<DIM>::backward() {
    const MaxPooling_Op<DIM>& op_ = static_cast<const MaxPooling_Op<DIM>&>(mOp);

    AIDGE_ASSERT(mMaxPoolingDesc != nullptr, "MaxPool descriptor must be created during forward!");
    AIDGE_ASSERT(op_.getOutput(0)->grad(), "missing output grad #0");

    const auto& output_grad = op_.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op_.getOutput(0)->grad());

    // Do the actual backward computation
    // Template is only for scaling parameters, which are always in float
    // excepted when the convolution is performed in double precision.
    if (op_.getOutput(0)->dataType() == DataType::Float64) {
        backward_<double>(output_grad);
    }
    else {
        backward_<float>(output_grad);
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::MaxPoolingImpl_cuda<DIM>::backward_(const Tensor& output_grad) {
    const MaxPooling_Op<DIM>& op_ = static_cast<const MaxPooling_Op<DIM>&>(mOp);

    const T alpha = 1.0f;
    const T beta = 1.0f; // accumulate
    CHECK_CUDNN_STATUS(
        cudnnPoolingBackward(CudaContext::cudnnHandle(),
                             mMaxPoolingDesc,
                             &alpha,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op_.getOutput(0)->getImpl())->getCudnnTensorDesc(*op_.getOutput(0)),
                             op_.getOutput(0)->getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(output_grad.getImpl())->getCudnnTensorDesc(output_grad),
                             output_grad.getImpl()->rawPtr(),
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op_.getInput(0)->getImpl())->getCudnnTensorDesc(*op_.getInput(0)),
                             op_.getInput(0)->getImpl()->rawPtr(),
                             &beta,
                             std::dynamic_pointer_cast<TensorImpl_cuda_>(op_.getInput(0)->grad()->getImpl())->getCudnnTensorDesc(*op_.getInput(0)),
                             op_.getInput(0)->grad()->getImpl()->rawPtr()));
}

template <Aidge::DimIdx_t DIM>
Aidge::MaxPoolingImpl_cuda<DIM>::~MaxPoolingImpl_cuda() {
    if(mMaxPoolingDesc != nullptr)
        cudnnDestroyPoolingDescriptor(mMaxPoolingDesc);
}


// Template declarations
template class Aidge::MaxPoolingImpl_cuda<2>;
