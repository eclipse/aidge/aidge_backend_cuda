/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/SqrtImpl_CUDA_kernels.hpp"
// Base template for floating-point types (float, double)
template<typename T>
__device__ T sqrt_helper(T x) {
    return sqrt(x);
}

// Specialization for half-precision type using CUDA's half
template<>
__device__ half sqrt_helper<half>(half x) {
#if __CUDA_ARCH__ >= 530 && defined(CUDART_VERSION) && CUDART_VERSION >= 9000
    return hsqrt(x);
#else
    float x_float = __half2float(x);  // Convert __half to float
    return __float2half(sqrt(x_float));  // Compute log and convert back to half
#endif
}

// Base template for floating-point types (float, double)
template<typename T>
__device__ T mul_helper(T a, T b) {
    return a * b;
}

// Specialization for half-precision type using CUDA's half
template<>
__device__ half mul_helper<half>(half a, half b) {
#if __CUDA_ARCH__ >= 530 && defined(CUDART_VERSION) && CUDART_VERSION >= 9000
    return __hmul(a, b);
#else
    float a_float = __half2float(a);  // Convert __half to float
    float b_float = __half2float(b);  // Convert __half to float
    return __float2half(a_float * b_float);  // Compute log and convert back to half
#endif
}

// Forward Kernel
template <class T>
__global__ void sqrtCUDAForwardKernel(const T* input,
                                      T* output,
                                      int size,
                                      const T alpha,
                                      const T beta)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx >= size) return;

    output[idx] = alpha * sqrt_helper(input[idx]) + beta * output[idx];
}

template <class T>
void Aidge::sqrtForward(const T* input,
                        T* output,
                        int size,
                        const T alpha,
                        const T beta)
{
    const int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    // Launch the kernel
    sqrtCUDAForwardKernel<<<numBlocks, blockSize>>>(input, output, size, alpha, beta);
};

// Backward Kernel
template <class T>
__global__ void sqrtCUDABackwardKernel(const T* input,
                                       const T* outputGrad,
                                       T* inputGrad,
                                       int size,
                                       const T alpha,
                                       const T beta)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx >= size) return;

    T val = outputGrad[idx] / mul_helper(static_cast<T>(2), sqrt_helper(input[idx]));

    inputGrad[idx] = alpha * val + beta * inputGrad[idx];
}

template <class T>
void Aidge::sqrtBackward(const T* input,
                         const T* outputGrad,
                         T* inputGrad,
                         int size,
                         const T alpha,
                         const T beta)
{
    const int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    // Launch the kernel
    sqrtCUDABackwardKernel<<<numBlocks, blockSize>>>(input, outputGrad, inputGrad, size, alpha, beta);
};

template void Aidge::sqrtForward<double>(const double* input, double* output, int size, const double alpha, const double beta);
template void Aidge::sqrtForward<float>(const float* input, float* output, int size, const float alpha, const float beta);
template void Aidge::sqrtForward<half>(const half* input, half* output, int size, const half alpha, const half beta);

template void Aidge::sqrtBackward<double>(const double* input, const double* outputGrad, double* inputGrad, int size, const double alpha, const double beta);
template void Aidge::sqrtBackward<float>(const float* input, const float* outputGrad, float* inputGrad, int size, const float alpha, const float beta);
template void Aidge::sqrtBackward<half>(const half* input, const half* outputGrad, half* inputGrad, int size, const half alpha, const half beta);
