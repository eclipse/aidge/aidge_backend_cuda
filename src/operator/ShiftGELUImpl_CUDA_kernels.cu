/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define CLAMP(X) (((X) < (0)) ? (0) : (X))

#include <stdio.h>
#include <cuda_runtime.h>

#include "aidge/backend/cuda/operator/ShiftGELUImpl_CUDA_kernels.hpp"

__device__ inline int ExpShift(int I,int N, double SF)
{
    int Ip = I + (I >> 1) - (I >> 4);
    int I0 = floorf(-1.0/SF);
    Ip = MAX(Ip,N*I0);
    int q = floorf(Ip / (I0));
    int r = Ip -(I0*q);
    int Ib = r/2 - I0;
    Ib = CLAMP(Ib * powf(2,N-q));
    return (int)Ib;
}

namespace Aidge{

template <class T>
__global__ void ShiftGELUforward_(T* input,int* quantized_tensor,int* GELUtensor,int* SumTensor, int* dims, double SF, int N, int output_bits) {

    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int z = blockIdx.z * blockDim.z + threadIdx.z;

    double SF_sig = SF * 1.702;
    double Final_SF = SF / powf(2,(output_bits-1));

    if (x < dims[0] && y < dims[1] && z < dims[2]) {
        int maxIdx = x * dims[1] * dims[2] * dims[3] + y * dims[2] * dims[3] + z * dims[3];
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            quantized_tensor[idx] = roundf(input[idx] / SF);
        }
        int maxVal = quantized_tensor[maxIdx];
        for (int i = 1; i < dims[3]; i++) {
            int idx = maxIdx + i;
            maxVal = MAX(maxVal, quantized_tensor[idx]);
        }
        int Max_Exp = ExpShift(-maxVal,N,SF_sig);
        for (int i = 0; i < dims[3]; i++) {
            int idx = maxIdx + i;
            GELUtensor[idx] = ExpShift(quantized_tensor[idx] - maxVal,N,SF_sig);
            if(GELUtensor[idx] > INT_MAX - Max_Exp) {
                SumTensor[idx] = 1;
            }
            else
            {
                SumTensor[idx] = floorf(INT_MAX/(GELUtensor[idx] + Max_Exp));
            }
            SumTensor[idx] = floorf((GELUtensor[idx] * SumTensor[idx]) >> (31 - output_bits + 1));
            quantized_tensor[idx] *= SumTensor[idx];
            input[idx] = quantized_tensor[idx] * Final_SF;
        }
    }
}

template <>
void ShiftGELUforward<float>(const float* input, float* output, double SF,int N, int output_bits, size_t size, std::vector<long unsigned int> dims_input) {

    double new_SF = 1/std::pow(2,2*output_bits-1);

    int dims_input_cuda[4];
    if (dims_input.size() >= 4) {
        for (std::size_t i = 0; i < 4; ++i) {
            dims_input_cuda[i] = static_cast<int>(dims_input[i]);
        }
    } 

    float* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(float));
    cudaMemcpy(input_cuda_tensor,input,size*sizeof(float),cudaMemcpyHostToDevice);
                                                                                                                        
    int* quantized_tensor;
    cudaMalloc(&quantized_tensor,size*sizeof(int));
    
    int* GELUtensor;
    cudaMalloc(&GELUtensor,size*sizeof(int));

    int* SumTensor;
    cudaMalloc(&SumTensor,size*sizeof(int));

    int* dims;
    cudaMalloc(&dims,4*sizeof(int));

    cudaMemcpy(dims,dims_input_cuda,4*sizeof(int),cudaMemcpyHostToDevice);     

    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks((dims_input[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
                   (dims_input[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
                   (dims_input[2] + threadsPerBlock.z - 1) / threadsPerBlock.z);

    ShiftGELUforward_<float><<<numBlocks, threadsPerBlock>>>(input_cuda_tensor, quantized_tensor,GELUtensor,SumTensor, dims, SF,N,8);
    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        printf("CUDA Error: %s\n", cudaGetErrorString(err));
    }

    cudaMemcpy(output,input_cuda_tensor,size*sizeof(float),cudaMemcpyDeviceToHost);

    cudaFree(quantized_tensor);
    cudaFree(GELUtensor);
    cudaFree(SumTensor);
    cudaFree(dims);
    cudaFree(input_cuda_tensor);
}

template <>
void ShiftGELUforward<double>(const double* input, double* output, double SF,int N, int output_bits, size_t size, std::vector<long unsigned int> dims_input) {

    double new_SF = 1/std::pow(2,2*output_bits-1);

    int dims_input_cuda[4];
    if (dims_input.size() >= 4) {
        for (std::size_t i = 0; i < 4; ++i) {
            dims_input_cuda[i] = static_cast<int>(dims_input[i]);
        }
    } 

    double* input_cuda_tensor;
    cudaMalloc(&input_cuda_tensor,size*sizeof(double));
    cudaMemcpy(input_cuda_tensor,input,size*sizeof(double),cudaMemcpyHostToDevice);
    
    int* quantized_tensor;
    cudaMalloc(&quantized_tensor,size*sizeof(int));

    int* GELUtensor;
    cudaMalloc(&GELUtensor,size*sizeof(int));

    int* SumTensor;
    cudaMalloc(&SumTensor,size*sizeof(int));

    int* dims;
    cudaMalloc(&dims,4*sizeof(int));

    cudaMemcpy(dims,dims_input_cuda,4*sizeof(int),cudaMemcpyHostToDevice);     

    dim3 threadsPerBlock(10, 10, 10);
    dim3 numBlocks((dims_input[0] + threadsPerBlock.x - 1) / threadsPerBlock.x,
                   (dims_input[1] + threadsPerBlock.y - 1) / threadsPerBlock.y,
                   (dims_input[2] + threadsPerBlock.z - 1) / threadsPerBlock.z);

    ShiftGELUforward_<double><<<numBlocks, threadsPerBlock>>>(input_cuda_tensor, quantized_tensor,GELUtensor,SumTensor, dims, SF,N,8);
    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        printf("CUDA Error: %s\n", cudaGetErrorString(err));
    }

    cudaMemcpy(output,input_cuda_tensor,size*sizeof(double),cudaMemcpyDeviceToHost);

    cudaFree(quantized_tensor);
    cudaFree(GELUtensor);
    cudaFree(SumTensor);
    cudaFree(dims);
    cudaFree(input_cuda_tensor);
}

template <class T>
__global__ void ShiftGELUbackward_(T* input_grad, const T* output_tensor, const T* output_grad, int size) {

    int index = blockIdx.x * blockDim.x + threadIdx.x;
    if (index < size) {
        float x = output_tensor[index];
        float grad = output_grad[index];

        float cdf = 0.5 * (1.0 + tanh(sqrt(2.0 / M_PI) * (x + 0.044715 * pow(x, 3))));
        float pdf = exp(-0.5 * x * x) / sqrt(2.0 * M_PI);
        float dx = pdf + x * cdf;
        float backprop_grad = grad * dx;
        input_grad[index] = backprop_grad;
    }
}

template <>
void ShiftGELUbackward<float>(const float* output_tensor, const float* output_grad, float* input_grad, size_t size)
{
    float* output_cuda_tensor;
    cudaMalloc(&output_cuda_tensor,size*sizeof(float));
    cudaMemcpy(output_cuda_tensor,output_tensor,size*sizeof(float),cudaMemcpyHostToDevice);

    float* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(float));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(float),cudaMemcpyHostToDevice);

    float *input_grad_;
    cudaMalloc(&input_grad_, size * sizeof(float));

    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ShiftGELUbackward_<float><<<Blocks,threadParBlock>>>(input_grad_,output_cuda_tensor,output_grad_,size);
    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        printf("CUDA Error: %s\n", cudaGetErrorString(err));
    }
    cudaMemcpy(input_grad,input_grad_, (size) * sizeof(float), cudaMemcpyDeviceToHost);
    cudaFree(output_cuda_tensor);
    cudaFree(input_grad_);
    cudaFree(output_grad_);
}

template <>
void ShiftGELUbackward<double>(const double* output_tensor, const double* output_grad, double* input_grad, size_t size)
{
    double* output_cuda_tensor;
    cudaMalloc(&output_cuda_tensor,size*sizeof(double));
    cudaMemcpy(output_cuda_tensor,output_tensor,size*sizeof(double),cudaMemcpyHostToDevice);

    double* output_grad_;
    cudaMalloc(&output_grad_,size*sizeof(double));
    cudaMemcpy(output_grad_,output_grad,size*sizeof(double),cudaMemcpyHostToDevice);

    double *input_grad_;
    cudaMalloc(&input_grad_, size * sizeof(double));

    dim3 threadParBlock(256);
    dim3 Blocks((size + threadParBlock.x -1) / threadParBlock.x);

    ShiftGELUbackward_<double><<<Blocks,threadParBlock>>>(input_grad_,output_cuda_tensor,output_grad_,size);
    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
    {
        printf("CUDA Error: %s\n", cudaGetErrorString(err));
    }
    cudaMemcpy(input_grad,input_grad_, (size) * sizeof(double), cudaMemcpyDeviceToHost);
    cudaFree(output_cuda_tensor);
    cudaFree(input_grad_);
    cudaFree(output_grad_);
}

}