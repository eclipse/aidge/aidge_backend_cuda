/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cuda_fp16.h>

#include "aidge/backend/cuda/operator/PowImpl_CUDA_kernels.hpp"

// Helper function for pow
template <typename T>
__device__ T pow(T x, T exponent) {
    return std::pow(x, exponent);
}
template <>
__device__ half pow<half>(half x, half exponent) {
    return __float2half(powf(__half2float(x), __half2float(exponent)));
}

template <class T>
__global__ void pow_kernel(const T* input, T* output, const T* exponent,
                          int* input_shape, int* exponent_shape, int* output_shape,
                          int* input_strides, int* exponent_strides, int* output_strides,
                          int num_dims, int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;

    int input_idx = 0, exponent_idx = 0;
    int temp_idx = idx;
    for (int i = 0; i < num_dims; ++i) {
        int dim = temp_idx / output_strides[i];
        temp_idx %= output_strides[i];
        input_idx += (input_shape[i] == 1 ? 0 : dim) * input_strides[i];
        exponent_idx += (exponent_shape[i] == 1 ? 0 : dim) * exponent_strides[i];
    }

    output[idx] = pow(input[input_idx], exponent[exponent_idx]);
}


template <class T>
void Aidge::powForward<T>(const T* input, T* output, const T* exponent,
                                const std::vector<int>& inputDims,const std::vector<int>& exponentDims, const std::vector<int>& outputDims,
                                const std::vector<int>& inputStrides, const std::vector<int>& exponentStrides,const std::vector<int>& outputStrides,
                                int outSize)
{
    int *d_input_strides, *d_exponent_strides, *d_output_strides, *d_input_shape, *d_exponent_shape, *d_output_shape;
    // Allocate device memory
    CHECK_CUDA_STATUS(cudaMalloc(&d_input_shape, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_exponent_shape, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_output_shape, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input_strides, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_exponent_strides, inputDims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_output_strides, inputDims.size() * sizeof(int)));

    // Copy data from host to device;
    CHECK_CUDA_STATUS(cudaMemcpy(d_input_shape, inputDims.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_exponent_shape, exponentDims.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_output_shape, outputDims.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input_strides, inputStrides.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_exponent_strides, exponentStrides.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_output_strides, outputStrides.data(), inputDims.size() * sizeof(int), cudaMemcpyHostToDevice));
    int blockSize = 256;
    int numBlocks = (outSize + blockSize - 1) / blockSize;

    int num_dims = inputDims.size();
    // Launch the kernel
    pow_kernel<<<numBlocks, blockSize>>>(input, output, exponent,
                                        d_input_shape, d_exponent_shape, d_output_shape,
                                        d_input_strides, d_exponent_strides, d_output_strides,
                                        num_dims, outSize);
    CHECK_CUDA_STATUS(cudaFree(d_input_shape));
    CHECK_CUDA_STATUS(cudaFree(d_exponent_shape));
    CHECK_CUDA_STATUS(cudaFree(d_output_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input_strides));
    CHECK_CUDA_STATUS(cudaFree(d_exponent_strides));
    CHECK_CUDA_STATUS(cudaFree(d_output_strides));
};

template void Aidge::powForward<double>(const double* input, double* output, const double* exponent,
                                        const std::vector<int>& inputDims,const std::vector<int>& exponentDims, const std::vector<int>& outputDims,
                                        const std::vector<int>& inputStrides, const std::vector<int>& exponentStrides,const std::vector<int>& outputStrides,
                                        int outSize);

template void Aidge::powForward<float>(const float* input, float* output, const float* exponent,
                                        const std::vector<int>& inputDims,const std::vector<int>& exponentDims, const std::vector<int>& outputDims,
                                        const std::vector<int>& inputStrides, const std::vector<int>& exponentStrides,const std::vector<int>& outputStrides,
                                        int outSize);

template void Aidge::powForward<half>(const half* input, half* output, const half* exponent,
                                        const std::vector<int>& inputDims,const std::vector<int>& exponentDims, const std::vector<int>& outputDims,
                                        const std::vector<int>& inputStrides, const std::vector<int>& exponentStrides,const std::vector<int>& outputStrides,
                                        int outSize);
