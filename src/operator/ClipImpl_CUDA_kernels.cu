/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/ClipImpl_CUDA_kernels.hpp"


// Helper function for Clip
template <typename T>
__device__ T clip(T a, T min_val, T max_val) {
    return min(max(a, min_val), max_val);
}

template <>
__device__ half clip<half>(half a, half min_val, half max_val) {
#if __CUDA_ARCH__ >= 530 && defined(CUDART_VERSION) && CUDART_VERSION >= 8000
    return __hmax(min_val, __hmin(a, max_val));
#else
    return __float2half(fmaxf(__half2float(min_val), fminf(__half2float(a), __half2float(max_val))));
#endif
}


template <class T>
__global__ void clipKernel(const T* input, T* output, int size, T min_val, T max_val) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;
    output[idx] = clip(input[idx], min_val, max_val);
}

template <class T>

void Aidge::clipForward(const T* input, T* output,int size,T min_val, T max_val)
{
    int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    clipKernel<<<numBlocks, blockSize>>>(input, output, size, min_val, max_val);

    CHECK_CUDA_STATUS(cudaGetLastError());
    CHECK_CUDA_STATUS(cudaDeviceSynchronize());
};

template void Aidge::clipForward<double>(const double* input, double* output, int size, double min_val, double max_val);

template void Aidge::clipForward<float>(const float* input, float* output, int size, float min_val, float max_val);

template void Aidge::clipForward<half>(const half* input, half* output, int size, half min_val, half max_val);

