/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/MatMulImpl.hpp"
#include "aidge/backend/cuda/operator/Cublas_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/utils/Types.h"

void Aidge::MatMulImpl_cuda::forward() {
    const MatMul_Op& op = static_cast<const MatMul_Op&>(mOp);
    // Check inputs
    AIDGE_ASSERT(op.getInput(0), "missing input in MatMul operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run MatMul forward because the 0-th input has no implementation.");
    DataType datatypeFirstInput = op.getInput(0)->dataType();
    for (IOIndex_t i = 1; i < op.nbInputs(); ++i) {
        AIDGE_ASSERT(op.getInput(i), "missing input in MatMul operator");
        AIDGE_ASSERT(op.getInput(i)->hasImpl(), "cannot run MatMul forward because the {}-th input has no implementation.", i);
        AIDGE_ASSERT(op.getInput(i)->dataType() == datatypeFirstInput, "Cannot MatMul inputs with two differents data type.");
    }


    const auto& input0 = op.getInput(0)->refCastFrom(mInput0Fallback, *op.getOutput(0));
    const auto& input1 = op.getInput(1)->refCastFrom(mInput1Fallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input0, input1);
            break;
        case DataType::Float32:
            forward_<float>(input0, input1);
            break;
        case DataType::Float16:
            forward_<half>(input0, input1);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::MatMulImpl_cuda::forward_(const Tensor& input0, const Tensor& input1) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const T * input0Ptr = static_cast<const T*>(input0.getImpl()->rawPtr());
    const T * input1Ptr = static_cast<const T*>(input1.getImpl()->rawPtr());
    T * outputPtr = static_cast<T*>(op.getOutput(0)->getImpl()->rawPtr());

    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    AIDGE_ASSERT(input0.nbDims()>=2 , "input#0 must be at least 2D");

    if (input1.nbDims()>=2) {
        int k = input0.dims()[input0.nbDims()-1];  // Number of columns of input0, rows of output
        int m = input0.dims()[input0.nbDims()-2];  // Number of rows of input0 and output
        int n = input1.dims()[input1.nbDims()-1];  // Number of columns of input1 and output

        int batchCount = op.getOutput(0)->size() / ( m * n );
        int batchCount0 = input0.size() / ( m * k );
        int batchCount1 = input1.size() / ( k * n );

        AIDGE_ASSERT((batchCount0==1 || batchCount0==batchCount)&&
                     (batchCount1==1 || batchCount1==batchCount),
                     "Broadcasting case not yes suppported by Backend Cuda!");

        long long int strideA = batchCount0 == 1 ? 0 : m * k;
        long long int strideB = batchCount1 == 1 ? 0 : k * n;
        long long int strideC = m * n;

        CHECK_CUBLAS_STATUS(cublasGemmStridedBatched(CudaContext::cublasHandle(),
                                        CUBLAS_OP_N,
                                        CUBLAS_OP_N,
                                        n,
                                        m,
                                        k,
                                        reinterpret_cast<const typename Cuda::cuda_type<T>::type*>(&alpha),
                                        input1Ptr, n, strideB,
                                        input0Ptr, k, strideA,
                                        reinterpret_cast<const typename Cuda::cuda_type<T>::type*>(&beta),
                                        outputPtr, n, strideC,
                                        batchCount));
    }
    else {
        int k = input0.dims()[input0.nbDims()-1];  // Number of columns of input0
        int m = input0.size() / k;  // Number of rows of input0 and output

        CHECK_CUBLAS_STATUS(cublasGemm(CudaContext::cublasHandle(),
                                        CUBLAS_OP_N,
                                        CUBLAS_OP_N,
                                        1,
                                        m,
                                        k,
                                        reinterpret_cast<const typename Cuda::cuda_type<T>::type*>(&alpha),
                                        input1Ptr, 1,
                                        input0Ptr, k,
                                        reinterpret_cast<const typename Cuda::cuda_type<T>::type*>(&beta),
                                        outputPtr, 1));
    }
}

void Aidge::MatMulImpl_cuda::backward() {
    // TODO
}

template <class T>
void Aidge::MatMulImpl_cuda::backward_(const Tensor& outGrad) {
    // TODO
}