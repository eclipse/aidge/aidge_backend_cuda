/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ReduceMeanImpl.hpp"
#include "aidge/backend/cuda/operator/ReduceImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Types.h"

void Aidge::ReduceMeanImpl_cuda::forward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(op.getInput(0), "missing input in ReduceMean operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run ReduceMean forward because the input has no implementation.");

    const auto& input = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->refCastFrom(mInputFallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));

    const ReduceMean_Op& rmOp = static_cast<const ReduceMean_Op&>(mOp);
    bool keepDims = rmOp.keepDims();
    auto axes =  rmOp.axes();
    if (input.size() == std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->size()) { // No reduction to be done
        std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->copy(input.getImpl()->rawPtr(), input.size());
    }
    else {
        if (!keepDims && mOutputDesc == nullptr) {
            std::vector<int> outputDims;
            std::copy(input.dims().begin(), input.dims().end(), std::back_inserter(outputDims));
            for (const auto axis:axes) {
                outputDims[axis] = 1;
            }
            if (outputDims.size() < 4) {
                outputDims.resize(4, 1);
            }
            // Compute the corresponding strides
            std::vector<int> outputStrides(outputDims.size());
            int product = 1;
            for (size_t i = outputDims.size(); i > 0; --i) {
                outputStrides[i - 1] = product;
                product *= outputDims[i - 1];
            }

            CHECK_CUDNN_STATUS(cudnnCreateTensorDescriptor(&mOutputDesc));
            CHECK_CUDNN_STATUS(cudnnSetTensorNdDescriptor(mOutputDesc, DataTypeToCudnn(op.getOutput(0)->dataType()), outputDims.size(), outputDims.data(), outputStrides.data()));
        }

        if (mReduceDesc == nullptr) {
            CHECK_CUDNN_STATUS(cudnnCreateReduceTensorDescriptor(&mReduceDesc));
            CHECK_CUDNN_STATUS(cudnnSetReduceTensorDescriptor(mReduceDesc,
                                                                CUDNN_REDUCE_TENSOR_AVG,
                                                                DataTypeToCudnn(op.getOutput(0)->dataType()),
                                                                CUDNN_PROPAGATE_NAN,
                                                                CUDNN_REDUCE_TENSOR_NO_INDICES,
                                                                CUDNN_32BIT_INDICES));
        }

        if (mWorkspace == nullptr) {
            const auto outputDesc = (keepDims)
                ? std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0))
                : mOutputDesc;

            CHECK_CUDNN_STATUS(cudnnGetReductionWorkspaceSize(CudaContext::cudnnHandle(),
                                mReduceDesc,
                                std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
                                outputDesc,
                                &mWorkspaceSize));

            CHECK_CUDA_STATUS(cudaMalloc(&mWorkspace, mWorkspaceSize));
        }

        switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
            case DataType::Float64:
                forward_<double>(input, axes, keepDims);
                break;
            case DataType::Float32:
                forward_<float>(input, axes, keepDims);
                break;
            case DataType::Float16:
                forward_<half>(input, axes, keepDims);
                break;
            default:
                AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
        }
    }
}


template <class T>
void Aidge::ReduceMeanImpl_cuda::forward_(const Tensor& input, const std::vector<int>& axes,  bool keepDims) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    if (keepDims) {
        CHECK_CUDNN_STATUS(cudnnReduceTensor(CudaContext::cudnnHandle(),
                            mReduceDesc,
                            NULL,
                            0,
                            mWorkspace,
                            mWorkspaceSize,
                            &alpha,
                            std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
                            input.getImpl()->rawPtr(),
                            &beta,
                            std::dynamic_pointer_cast<TensorImpl_cuda_>(op.getOutput(0)->getImpl())->getCudnnTensorDesc(*op.getOutput(0)),
                            std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()));    }
    else {
        CHECK_CUDNN_STATUS(cudnnReduceTensor(CudaContext::cudnnHandle(),
                            mReduceDesc,
                            NULL,
                            0,
                            mWorkspace,
                            mWorkspaceSize,
                            &alpha,
                            std::dynamic_pointer_cast<TensorImpl_cuda_>(input.getImpl())->getCudnnTensorDesc(input),
                            input.getImpl()->rawPtr(),
                            &beta,
                            mOutputDesc,
                            std::static_pointer_cast<Tensor>(op.getRawOutput(0))->getImpl()->rawPtr()));
    }
}

void Aidge::ReduceMeanImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing outputGrad in ReduceMean operator");
    AIDGE_ASSERT(op.getOutput(0)->grad()->hasImpl(), "cannot run ReduceMean backward because the output grad has no implementation.");

    const auto& outGrad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getInput(0)->grad());

    const ReduceMean_Op& rmOp = static_cast<const ReduceMean_Op&>(mOp);
    auto axes =  rmOp.axes();
    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            backward_<double>(outGrad, axes);
            break;
        case DataType::Float32:
            backward_<float>(outGrad, axes);
            break;
        case DataType::Float16:
            backward_<half>(outGrad, axes);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::ReduceMeanImpl_cuda::backward_(const Tensor& outGrad, const std::vector<int>& axes) {

    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    // const typename Cuda::cudnn_scaling_type<T>::type alpha = 1.0f;
    // const typename Cuda::cudnn_scaling_type<T>::type beta = 0.0f;

    const T alpha = 1.0f;
    const T beta = 1.0f; // accumulate

    const T * outputGrad = static_cast<const T*>(op.getOutput(0)->grad()->getImpl()->rawPtr());
    T * inputGrad = static_cast<T*>(op.getInput(0)->grad()->getImpl()->rawPtr());

    std::vector<std::size_t> factors;
    for (auto axis:axes) {
        factors.push_back(op.getInput(0)->grad()->dims()[axis]);
    }
    
    Aidge::ReduceBackward(outputGrad,
                            inputGrad,
                            outGrad.dims(),
                            op.getInput(0)->grad()->dims(),
                            axes,
                            factors,
                            static_cast<int>(op.getInput(0)->grad()->size()),
                            alpha,
                            beta);
}

Aidge::ReduceMeanImpl_cuda::~ReduceMeanImpl_cuda() {
    if (mReduceDesc != nullptr) {
        cudnnDestroyReduceTensorDescriptor(mReduceDesc);
    }

    if (mOutputDesc != nullptr) {
        cudnnDestroyTensorDescriptor(mOutputDesc);
    }

    if (mWorkspace != nullptr) {
        cudaFree(mWorkspace);
    }
}
