/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ErfImpl.hpp"
#include "aidge/backend/cuda/operator/ErfImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Erf.hpp"
#include "aidge/utils/Types.h"

void Aidge::ErfImpl_cuda::forward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(op.getInput(0), "missing input #0");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input);
            break;
        case DataType::Float32:
            forward_<float>(input);
            break;
        case DataType::Float16:
            forward_<half>(input);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::ErfImpl_cuda::forward_(const Tensor& input) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const T * inputPtr = static_cast<const T*>(input.getImpl()->rawPtr());
    T * outputPtr = static_cast<T*>(op.getOutput(0)->getImpl()->rawPtr());

    Aidge::ErfForward<T>(inputPtr, outputPtr, static_cast<int>(op.getOutput(0)->size()));
}

void Aidge::ErfImpl_cuda::backward() {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);

    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output #0 grad");

    const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());

    switch(op.getInput(0)->grad()->dataType()) {
        case DataType::Float64:
            backward_<double>(output_grad);
            break;
        case DataType::Float32:
            backward_<float>(output_grad);
            break;
        case DataType::Float16:
            backward_<half>(output_grad);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <class T>
void Aidge::ErfImpl_cuda::backward_(const Tensor& output_grad) {
    //TODO
}