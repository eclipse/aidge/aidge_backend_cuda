/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include <cuda_fp16.h>
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AbsImpl.hpp"
#include "aidge/backend/cuda/operator/AbsImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Abs.hpp"
#include "aidge/utils/Types.h"

void Aidge::AbsImpl_cuda::forward() {

    const Abs_Op& op = static_cast<const Abs_Op&>(mOp);

    AIDGE_ASSERT(op.getInput(0), "missing input #0");

    const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input);
            break;
        case DataType::Float32:
            forward_<float>(input);
            break;
        case DataType::Float16:
            forward_<half>(input);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by the CUDA backend");
    }
}

template <class T>
void Aidge::AbsImpl_cuda::forward_(const Tensor& input) 
{
    const Abs_Op& op = static_cast<const Abs_Op&>(mOp);

    const T* inputPtr = static_cast<T*>(input.getImpl()->rawPtr());
    T* outputPtr = static_cast<T*>(op.getOutput(0)->getImpl()->rawPtr());

    int size = op.getInput(0)->size();
    
    Aidge::absForward<T>(inputPtr, outputPtr, size);
}

void Aidge::AbsImpl_cuda::backward() {
    // TODO
}

template <class T>
void Aidge::AbsImpl_cuda::backward_(const Tensor& input, const Tensor& outputGrad) {
    // TODO
}