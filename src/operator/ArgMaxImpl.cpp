/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ArgMaxImpl.hpp"
#include "aidge/backend/cuda/operator/ArgMaxImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/Types.h"

void Aidge::ArgMaxImpl_cuda::forward() {
    const ArgMax_Op& op = dynamic_cast<const ArgMax_Op&>(mOp);
    AIDGE_ASSERT(mOp.getRawInput(0), "missing input in ArgMax operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run ArgMax forward because the input has no implementation.");

    const auto& input = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->refCastFrom(mInputFallback, *std::static_pointer_cast<Tensor>(mOp.getRawOutput(0)));
    const std::int32_t axis = op.axis();
    const DimSize_t selectLastIdx = op.selectLastIndex();
    switch(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()) {
        case DataType::Float64:
            forward_<double>(input, axis, selectLastIdx);
            break;
        case DataType::Float32:
            forward_<float>(input, axis, selectLastIdx);
            break;
        case DataType::Float16:
            forward_<half>(input, axis, selectLastIdx);
            break;
        default:
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}


template <class T>
void Aidge::ArgMaxImpl_cuda::forward_(const Tensor& input, std::int32_t axis, DimSize_t selectLastIdx) {
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);


    const T * inputPtr = static_cast<const T*>(input.getImpl()->rawPtr());
    T * outputPtr = static_cast<T*>(op.getOutput(0)->getImpl()->rawPtr());

    std::vector<int> inputStrides(op.getInput(0)->nbDims(), 1);
    if(op.getInput(0)->nbDims()>1) {
        for (int i = op.getInput(0)->nbDims()-2; i >= 0; i--) {
            inputStrides[i] = inputStrides[i+1] *  op.getInput(0)->dims()[i+1];
        }
    }

    std::vector<int> inputShape(input.nbDims());

    // Use std::transform to convert each element
    std::transform(input.dims().begin(), input.dims().end(), inputShape.begin(),
                   [](size_t value) {
                       return static_cast<int>(value);
                   });
    Aidge::ArgMax_cuda_forward_kernel<T>(inputPtr, outputPtr,
                                      inputShape, inputStrides,
                                      axis, static_cast<int>(op.getInput(0)->size()), selectLastIdx);
}
