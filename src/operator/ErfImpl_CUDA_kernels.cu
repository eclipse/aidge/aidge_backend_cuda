/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/ErfImpl_CUDA_kernels.hpp"
// Base template for floating-point types (float, double)
template<typename T>
__device__ T Erf_helper(T x) {
    return erff(x);
}

// Specialization for half-precision type using CUDA's half
template<>
__device__ half Erf_helper<half>(half x) {
    float x_float = __half2float(x);  // Convert __half to float
    return __float2half(erff(x_float));  // Compute log and convert back to half
}

template <class T>
__global__ void ErfKernel(const T* input, T* output, int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;

    output[idx] = Erf_helper(input[idx]);
}

template <class T>
void Aidge::ErfForward(const T* input, T* output, int size)
{
    int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    // Launch the kernel
    ErfKernel<<<numBlocks, blockSize>>>(input, output, size);
};

template void Aidge::ErfForward<double>(const double* input, double* output, int size);

template void Aidge::ErfForward<float>(const float* input, float* output, int size);

template void Aidge::ErfForward<half>(const half* input, half* output, int size);
