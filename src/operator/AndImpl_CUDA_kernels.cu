/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cuda_fp16.h>

#include "aidge/backend/cuda/operator/AndImpl_CUDA_kernels.hpp"

// Helper function for comparison
template <typename T>
__device__ bool compareE(T a, T b) {
    return a == b;
}
template <>
__device__ bool compareE<half>(half a, half b) {
    return __half2float(a) == __half2float(b);
}

template <typename T>
__global__ void and_cuda_Kernel(const T* input1, const T* input2, T* output,
                          int* input1_shape, int* input2_shape,
                          int* input1_strides, int* input2_strides, int* output_strides,
                          int num_dims, int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= size) return;

    int input1_idx = 0, input2_idx = 0;
    int temp_idx = idx;
    for (int i = 0; i < num_dims; ++i) {
        int dim = temp_idx / output_strides[i];
        temp_idx %= output_strides[i];
        input1_idx += (input1_shape[i] == 1 ? 0 : dim) * input1_strides[i];
        input2_idx += (input2_shape[i] == 1 ? 0 : dim) * input2_strides[i];
    }

    output[idx] = static_cast<T>(compareE(input1[input1_idx], input2[input2_idx]));
}

template <typename T>
void Aidge::AndForward(const T* input1, const T* input2, T* output,
                        const std::vector<int>& input1Dims,const std::vector<int>& input2Dims,
                        const std::vector<int>& input1Strides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                        int outSize)
{
    int *d_input1_strides, *d_input2_strides, *d_output_strides, *d_input1_shape, *d_input2_shape;
    // Allocate device memory
    CHECK_CUDA_STATUS(cudaMalloc(&d_input1_shape, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input2_shape, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input1_strides, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_input2_strides, input1Dims.size() * sizeof(int)));
    CHECK_CUDA_STATUS(cudaMalloc(&d_output_strides, input1Dims.size() * sizeof(int)));

    // Copy data from host to device;
    CHECK_CUDA_STATUS(cudaMemcpy(d_input1_shape, input1Dims.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input2_shape, input2Dims.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input1_strides, input1Strides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_input2_strides, input2Strides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    CHECK_CUDA_STATUS(cudaMemcpy(d_output_strides, outputStrides.data(), input1Dims.size() * sizeof(int), cudaMemcpyHostToDevice));
    int blockSize = 256;
    int numBlocks = (outSize + blockSize - 1) / blockSize;

    int num_dims = input1Dims.size();
    // Launch the kernel
    and_cuda_Kernel<<<numBlocks, blockSize>>>(input1, input2, output,
                                            d_input1_shape, d_input2_shape,
                                            d_input1_strides, d_input2_strides, d_output_strides,
                                            num_dims, outSize);
    CHECK_CUDA_STATUS(cudaFree(d_input1_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input2_shape));
    CHECK_CUDA_STATUS(cudaFree(d_input1_strides));
    CHECK_CUDA_STATUS(cudaFree(d_input2_strides));
    CHECK_CUDA_STATUS(cudaFree(d_output_strides));
};

template void Aidge::AndForward(const double* input1, const double* input2, double* output,
                        const std::vector<int>& input1Dims,const std::vector<int>& input2Dims,
                        const std::vector<int>& inputStrides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                        int outSize);

template void Aidge::AndForward(const float* input1, const float* input2, float* output,
                        const std::vector<int>& input1Dims,const std::vector<int>& input2Dims,
                        const std::vector<int>& inputStrides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                        int outSize);

template void Aidge::AndForward(const half* input1, const half* input2, half* output,
                        const std::vector<int>& input1Dims,const std::vector<int>& input2Dims,
                        const std::vector<int>& inputStrides, const std::vector<int>& input2Strides,const std::vector<int>& outputStrides,
                        int outSize);