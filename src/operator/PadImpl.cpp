/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/PadImpl.hpp"

#include <vector>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/PadImpl_CUDA_kernels.hpp"
#include "aidge/backend/cuda/utils/CudaContext.hpp"
#include "aidge/backend/cuda/utils/CudaUtils.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
void Aidge::PadImpl_cuda<DIM>::forward()
{
    const Pad_Op<DIM> &op = static_cast<const Pad_Op<DIM> &>(mOp);

    AIDGE_ASSERT(op.getInput(0), "missing input in Pad operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run Pad forward input has no implementation.");

    const auto &input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));

    auto paddingBorders = op.beginEndBorders();

    mLeftPad = paddingBorders[2];
    mTopPad = paddingBorders[0];
    mPadVal = op.borderValue();
    mPadType = static_cast<unsigned int>(op.borderType());

    switch (op.getOutput(0)->dataType())
    {
    case DataType::Float64:
        forward_<double>(input);
        break;
    case DataType::Float32:
        forward_<float>(input);
        break;
    case DataType::Float16:
        forward_<half>(input);
        break;
    default:
        AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::PadImpl_cuda<DIM>::forward_(const Tensor &input)
{
    const auto outDims = std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dims();
    const T *inputPtr = static_cast<const T *>(input.getImpl()->rawPtr());
    
    const T alpha = 1.0f;
    const T beta = 0.0f;

    T *output = static_cast<T *>(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());

    Aidge::cudaPadding(CudaContext::getDeviceProp(),
                       outDims[1],
                       outDims[3],
                       outDims[2],
                       input.dims()[1],
                       input.dims()[0],
                       input.dims()[3],
                       input.dims()[2],
                       mLeftPad,
                       mTopPad,
                       mPadType,
                       static_cast<T>(mPadVal),
                       inputPtr,
                       output,
                       alpha,
                       beta);
}

template <Aidge::DimIdx_t DIM>
void Aidge::PadImpl_cuda<DIM>::backward()
{
    const Pad_Op<DIM> &op = static_cast<const Pad_Op<DIM> &>(mOp);

    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing output gradient in Pad operator");
    AIDGE_ASSERT(op.getOutput(0)->grad(), "cannot run Pad backward, output gradient has no implementation.");

    const auto &outGrad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getInput(0));

    auto paddingBorders = op.beginEndBorders();

    mLeftPad = paddingBorders[2];
    mTopPad = paddingBorders[0];
    mPadVal = op.borderValue();
    mPadType = static_cast<unsigned int>(op.borderType());

    switch (std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType())
    {
    case DataType::Float64:
        backward_<double>(outGrad);
        break;
    case DataType::Float32:
        backward_<float>(outGrad);
        break;
    case DataType::Float16:
        backward_<half>(outGrad);
        break;
    default:
        AIDGE_THROW_OR_ABORT(std::runtime_error, "Data type is not supported by Backend Cuda");
    }
}

template <Aidge::DimIdx_t DIM>
template <class T>
void Aidge::PadImpl_cuda<DIM>::backward_(const Tensor &outGrad)
{
    const OperatorTensor& op = static_cast<const OperatorTensor&>(mOp);
    const auto inputGradDims = op.getInput(0)->grad()->dims();

    const T alpha = 1.0f;
    const T beta = 1.0f; // accumulate

    T *inputGrad = static_cast<T *>(op.getInput(0)->grad()->getImpl()->rawPtr());

    Aidge::cudaPadding(CudaContext::getDeviceProp(),
                       inputGradDims[1],
                       inputGradDims[3],
                       inputGradDims[2],
                       outGrad.dims()[1],
                       outGrad.dims()[0],
                       outGrad.dims()[3],
                       outGrad.dims()[2],
                       -mLeftPad,
                       -mTopPad,
                       mPadType,
                       static_cast<T>(mPadVal),
                       static_cast<const T *>(outGrad.getImpl()->rawPtr()),
                       inputGrad,
                       alpha,
                       beta);
}

// Template declarations
template class Aidge::PadImpl_cuda<2>;
