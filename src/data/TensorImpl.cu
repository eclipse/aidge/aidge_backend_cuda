/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/data/TensorImpl.hpp"

#include <thrust/equal.h>
#include <thrust/device_ptr.h>

template <typename SRC_T, typename DST_T>
void Aidge::thrust_copy(const SRC_T* srcData, DST_T* dstData, size_t size)
{
    const thrust::device_ptr<const SRC_T> thrustSrcPtr(srcData);
    thrust::device_ptr<DST_T> thrustDstPtr(dstData);
    thrust::copy(thrustSrcPtr, thrustSrcPtr + size, thrustDstPtr);
}

template <typename SRC_T>
__global__ void
cudaCopyToH_kernel(const SRC_T* srcData,
                    __half* dstData,
                    size_t size)
{
    const unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = index; i < size; i += stride) {
        dstData[i] = __float2half(static_cast<float>(srcData[i]));
    }
}

template <typename SRC_T, typename std::enable_if<!std::is_same<half_float::half, SRC_T>::value>::type*>
void Aidge::thrust_copy(const SRC_T* srcData, half_float::half* dstData, size_t size)
{
    cudaCopyToH_kernel<SRC_T><<<(size + 255) / 256, 256>>>
        (srcData, reinterpret_cast<__half*>(dstData), size);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <typename DST_T>
__global__ void
cudaCopyFromH_kernel(const __half* srcData,
                    DST_T* dstData,
                    size_t size)
{
    const unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = index; i < size; i += stride) {
        dstData[i] = static_cast<DST_T>(__half2float(srcData[i]));
    }
}

template <typename DST_T, typename std::enable_if<!std::is_same<half_float::half, DST_T>::value>::type*>
void Aidge::thrust_copy(const half_float::half* srcData, DST_T* dstData, size_t size)
{
    cudaCopyFromH_kernel<DST_T><<<(size + 255) / 256, 256>>>
        (reinterpret_cast<const __half*>(srcData), dstData, size);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

__global__ void
cudaCopyHToH_kernel(const __half* srcData,
                    __half* dstData,
                    size_t size)
{
    const unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = index; i < size; i += stride) {
        dstData[i] = srcData[i];
    }
}

template <>
void Aidge::thrust_copy(const half_float::half* srcData, half_float::half* dstData, size_t size)
{
    cudaCopyHToH_kernel<<<(size + 255) / 256, 256>>>
        (reinterpret_cast<const __half*>(srcData), reinterpret_cast<__half*>(dstData), size);
    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <class T>
bool Aidge::TensorImpl_cuda<T>::operator==(const TensorImpl &otherImpl) const {
    const auto& otherImplCuda = static_cast<const TensorImpl_cuda<T>&>(otherImpl);

    if (mNbElts != otherImplCuda.size())
        return false;

    thrust::device_ptr<T> thrustData(mData.data());
    thrust::device_ptr<T> thrustOtherData(otherImplCuda.mData.data());
    return thrust::equal(thrustData, thrustData + mNbElts, thrustOtherData);
}

// double
template void Aidge::thrust_copy<>(double const*, double*, size_t);
template void Aidge::thrust_copy<>(double const*, float*, size_t);
template void Aidge::thrust_copy<>(double const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(double const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(double const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(double const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(double const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(double const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(double const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(double const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(double const*, uint8_t*, size_t);
// float
template void Aidge::thrust_copy<>(float const*, double*, size_t);
template void Aidge::thrust_copy<>(float const*, float*, size_t);
template void Aidge::thrust_copy<>(float const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(float const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(float const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(float const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(float const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(float const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(float const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(float const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(float const*, uint8_t*, size_t);
// half_float::half
template void Aidge::thrust_copy<>(half_float::half const*, double*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, float*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(half_float::half const*, uint8_t*, size_t);
// int64_t
template void Aidge::thrust_copy<>(int64_t const*, double*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, float*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(int64_t const*, uint8_t*, size_t);
// int32_t
template void Aidge::thrust_copy<>(int32_t const*, double*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, float*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(int32_t const*, uint8_t*, size_t);
// int16_t
template void Aidge::thrust_copy<>(int16_t const*, double*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, float*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(int16_t const*, uint8_t*, size_t);
// int8_t
template void Aidge::thrust_copy<>(int8_t const*, double*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, float*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(int8_t const*, uint8_t*, size_t);
// uint64_t
template void Aidge::thrust_copy<>(uint64_t const*, double*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, float*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(uint64_t const*, uint8_t*, size_t);
// uint32_t
template void Aidge::thrust_copy<>(uint32_t const*, double*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, float*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(uint32_t const*, uint8_t*, size_t);
// uint16_t
template void Aidge::thrust_copy<>(uint16_t const*, double*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, float*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(uint16_t const*, uint8_t*, size_t);
// uint8_t
template void Aidge::thrust_copy<>(uint8_t const*, double*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, float*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, half_float::half*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, int64_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, int32_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, int16_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, int8_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, uint64_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, uint32_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, uint16_t*, size_t);
template void Aidge::thrust_copy<>(uint8_t const*, uint8_t*, size_t);