![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/pipeline.svg?ignore_skipped=true) ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge CUDA library

You can find in this folder the library that implements the CUDA operators.
[TOC]

## Installation

### Dependencies
- [CUDA ToolKit 12.4](https://developer.nvidia.com/cuda-12-4-0-download-archive)
- [CUDnn9](https://developer.nvidia.com/cudnn-downloads) make sure to install the CUDA 12 compatible version
- `GCC`
- `Make`/`Ninja`
- `CMake`
- `Python` (optional, if you have no intend to use this library in python with pybind)

#### Aidge dependencies
 - `aidge_core`
 - `aidge_backend_cpu`

### Pip installation
``` bash
pip install . -v
```
> **TIPS:** Use environment variables to change compilation options:
> - `AIDGE_INSTALL`: to set the installation folder. Defaults to /usr/local/lib. :warning: This path must be identical to aidge_core install path.
> - `AIDGE_PYTHON_BUILD_TYPE`: to set the compilation mode to **Debug** or **Release** 
> - `AIDGE_BUILD_GEN`: to set the build backend with 

## Standard C++ Compilation

You will need to compile first the Core library before compiling the CUDA one.
The makefile is designed to do it for you.
