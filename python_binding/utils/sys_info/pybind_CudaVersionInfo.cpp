#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/CudaVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_cuda_sys_info(py::module& m){
    m.def("show_version", &showBackendCudaProjectVersion);
    m.def("get_project_version", &getBackendCudaProjectVersion);
    m.def("get_git_hash", &getBackendCudaGitHash);
}
}
