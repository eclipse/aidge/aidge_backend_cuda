function(generate_python_binding pybind_module_name target_to_bind) 
    add_definitions(-DPYBIND)
    Include(FetchContent)

    set(PYBIND_VERSION v2.10.4)
    set(PYBIND11_FINDPYTHON ON)
    message(STATUS "Retrieving pybind ${PYBIND_VERSION} from git")

    FetchContent_Declare(
        PyBind11
        GIT_REPOSITORY https://github.com/pybind/pybind11.git
        GIT_TAG        ${PYBIND_VERSION} # or a later release
    )

    # Use the New FindPython mode, recommanded. Requires CMake 3.15+
    find_package(Python COMPONENTS Interpreter Development.Module)
    FetchContent_MakeAvailable(PyBind11)

    message(STATUS "Creating binding for module ${pybind_module_name}")
    file(GLOB_RECURSE pybind_src_files "python_binding/*.cpp")

    pybind11_add_module(${pybind_module_name} MODULE ${pybind_src_files} "NO_EXTRAS") # NO EXTRA recquired for pip install
    target_include_directories(${pybind_module_name} PRIVATE "python_binding")
    target_link_libraries(${pybind_module_name} 
        PRIVATE 
            ${target_to_bind}
            CUDA::cublas
            cudnn
            CUDA::cudart
    )
    set_property(TARGET ${pybind_module_name} PROPERTY POSITION_INDEPENDENT_CODE ON)
    set_target_properties(${pybind_module_name} PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
    set_target_properties(${pybind_module_name} PROPERTIES
        CMAKE_SHARED_LINKER_FLAGS "-Wl,--exclude-libs,ALL"
    )
    set_target_properties(${module} PROPERTIES INSTALL_RPATH "")
endfunction()
