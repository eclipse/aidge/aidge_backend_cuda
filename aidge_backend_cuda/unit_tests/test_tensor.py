import unittest
import aidge_core
import aidge_backend_cpu
import aidge_backend_cuda
import numpy as np


class test_tensor(unittest.TestCase):
    """Test tensor binding"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_getavailable_backends(self):
        self.assertTrue("cuda" in aidge_core.Tensor.get_available_backends())


if __name__ == "__main__":
    unittest.main()
