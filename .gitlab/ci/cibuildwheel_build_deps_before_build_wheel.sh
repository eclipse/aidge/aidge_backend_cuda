#!/bin/bash
set -e
if [[ "$1" == "" ]]; then 
  echo "build aidge deps in cibuildwheel container before building wheel."
  echo "search path defines where the dependencies will be searched."
  echo "Hint : In wheel containers, files are mounted on /host by default."
  echo "\nusage : ./cibuildwheel_build_deps_before_build_wheel.sh $search_path"
fi
set -x
if [[ $AIDGE_DEPENDENCIES ==  "" ]]; then # case for aidge_ core
  mkdir -p build # creating build if its not already there to hold the build of cpp files
  rm -rf build/* # build from scratch
else 
  for repo in $AIDGE_DEPENDENCIES ; do # case for other projects
    search_path=$1
    REPO_PATH=$(find $search_path ! -writable -prune -o  -type d     \
                                    -name "$repo"                    \
                                    -not -path "*/install/*"         \
                                    -not -path "*/.git/*"            \
                                    -not -path "*/.mypy_cache/*"     \
                                    -not -path "*/miniconda/*"       \
                                    -not -path "*/conda/*"           \
                                    -not -path "*/.local/*"          \
                                    -not -path "*/lib/*"             \
                                    -not -path "*/$repo/$repo/*"     \
                                    -not -path "*/proc/*"            \
                                    -print -quit)
    if [[ -z "$REPO_PATH" ]]; then 
      echo "ERROR : dependency $repo not found in search_path \"$search_path\". ABORTING."
      exit -1
    fi

    cd $REPO_PATH
    mkdir -p build # creating build if its not already there to hold the build of cpp files
    rm -rf build/* # build from scratch
    pip install . -v
    cd -
  done
fi
set +x
set +e
