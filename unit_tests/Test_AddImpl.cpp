/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>  // cudaMemcpy, cudaMemcpyDeviceToHost

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/AddImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AddImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] Add(forward)", "[Add][GPU]") {
    SECTION("Same input") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(Array4D<float,3,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{26, 53},{27, 54},{28, 55}}    //
                },                                  //
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{32, 59},{33, 60},{34, 61}},   //
                    {{35, 62},{36, 63},{37, 64}}    //
                },                                  //
                {                                   //
                    {{38, 65},{39, 66},{40, 67}},   //
                    {{41, 68},{42, 69},{43, 70}},   //
                    {{44, 71},{45, 72},{46, 73}}    //
                }                                   //
            }                                       //
        });                                         //
        input1->setBackend("cuda");

        Tensor expectedOutput = Array4D<float,3,3,3,2> {
            {
                {
                    {{40,  94},{42,  96},{44,  98}},
                    {{46, 100},{48, 102},{50, 104}},
                    {{52, 106},{54, 108},{56, 110}}
                },
                {
                    {{58, 112},{60, 114},{62, 116}},
                    {{64, 118},{66, 120},{68, 122}},
                    {{70, 124},{72, 126},{74, 128}}
                },
                {
                    {{76, 130},{78, 132},{80, 134}},
                    {{82, 136},{84, 138},{86, 140}},
                    {{88, 142},{90, 144},{92, 146}}
                }
            }
        };

        std::shared_ptr<Add_Op> op = std::make_shared<Add_Op>();
        op->setBackend("cuda");
        op->setDataType(DataType::Float32);

        op->associateInput(0, input1);
        op->associateInput(1, input1);
        op->forward();

        Tensor myOutput = *(op->getOutput(0));
        myOutput.setBackend("cpu");

        REQUIRE(approxEq<cpptype_t<DataType::Float32>>(expectedOutput, myOutput, (1.0E-5F), (1.0E-8F)));
    }

    SECTION("Broadcasting") {
        std::shared_ptr<Tensor> input_0 = std::make_shared<Tensor>(Array4D<float,3,1,3,2> {
            {                                       //
                {                                   //
                    {{0, 1},{2, 3},{4, 5}}          //
                },                                  //
                {                                   //
                    {{6, 7},{8, 9},{10, 11}}        //
                },                                  //
                {                                   //
                    {{12, 13},{14, 15},{16, 17}}    //
                }                                   //
            }                                       //
        });
        input_0->setBackend("cuda");

        std::shared_ptr<Tensor> input_1 = std::make_shared<Tensor>(Array4D<float,1,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 21},{22, 23},{24, 25}},   //
                    {{26, 27},{28, 29},{30, 31}},   //
                    {{32, 33},{34, 35},{36, 37}}    //
                }                                   //
            }                                       //
        });
        input_1->setBackend("cuda");

        std::shared_ptr<Tensor> input_2 = std::make_shared<Tensor>(Array1D<float,2> {{100,200}});
        input_2->setBackend("cuda");

        /// Input0(d0, 1, d2, d3) + Input1(1, d1, d2, d3) = Output(d0, d1, d2, d3)
        Tensor expectedOutput0 = Array4D<float,3,3,3,2> {
            {                                         //
                {                                     //
                    {{ 20, 22},{ 24, 26},{ 28, 30}},  //
                    {{ 26, 28},{ 30, 32},{ 34, 36}},  //
                    {{ 32, 34},{ 36, 38},{ 40, 42}}   //
                },                                    //
                {                                     //
                    {{ 26, 28},{ 30, 32},{ 34, 36}},  //
                    {{ 32, 34},{ 36, 38},{ 40, 42}},  //
                    {{ 38, 40},{ 42, 44},{ 46, 48}}   //
                },                                    //
                {                                     //
                    {{ 32, 34},{ 36, 38},{40, 42}},   //
                    {{ 38, 40},{ 42, 44},{46, 48}},   //
                    {{ 44, 46},{ 48, 50},{52, 54}}    //
                }                                     //
            }                                         //
        };

        std::shared_ptr<Add_Op> op0 = std::make_shared<Add_Op>();
        op0->setDataType(DataType::Float32);
        op0->setBackend("cuda");

        op0->associateInput(0, input_0);
        op0->associateInput(1, input_1);
        op0->forward();

        Tensor myOutput0 = *(op0->getOutput(0));
        myOutput0.setBackend("cpu");

        REQUIRE(approxEq<cpptype_t<DataType::Float32>>(expectedOutput0, myOutput0, (1.0E-5F), (1.0E-8F)));

        /// Input0(d0, d1, d2, d3) + Input1(d3) = Output(d0, d1, d2, d3)
        Tensor expectedOutput1 = Array4D<float,3,1,3,2> {
            {                                             //
                {                                         //
                    {{100, 201},{102, 203},{104, 205}}    //
                },                                        //
                {                                         //
                    {{106, 207},{108, 209},{110, 211}}    //
                },                                        //
                {                                         //
                    {{112, 213},{114, 215},{116, 217}}    //
                }                                         //
            }                                             //
        };                                           //
        std::shared_ptr<Add_Op> op1 = std::make_shared<Add_Op>();
        op1->setDataType(DataType::Float32);
        op1->setBackend("cuda");

        op1->associateInput(0, input_0);
        op1->associateInput(1, input_2);
        op1->forward();

        Tensor myOutput1 = *(op1->getOutput(0));
        myOutput1.setBackend("cpu");

        REQUIRE(approxEq<cpptype_t<DataType::Float32>>(expectedOutput1, myOutput1, (1.0E-5F), (1.0E-8F)));
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),
                                                               std::size_t(10));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(4), std::size_t(5));
        std::uniform_int_distribution<int> boolDist(0,1);

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create Add Operator CUDA
            std::shared_ptr<Add_Op> op_cuda = std::make_shared<Add_Op>();
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create Add Operator CPU
            std::shared_ptr<Add_Op> op_cpu = std::make_shared<Add_Op>();
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            const std::size_t nbDims = nbDimsDist(gen);
            std::vector<std::size_t> dims0, dims1, dims;
            for (std::size_t i = 0; i < nbDims; ++i) {
                const std::size_t dim = dimSizeDist(gen);
                // To test broadcasting, set some dims to 1
                dims0.push_back(boolDist(gen) ? 1 : dim);
                dims1.push_back(boolDist(gen) ? 1 : dim);

                dims.push_back(std::max(dims0[i], dims1[i]));
            }
            const std::size_t nb_elements0 = std::accumulate(dims0.cbegin(), dims0.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t nb_elements1 = std::accumulate(dims1.cbegin(), dims1.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;

            float* array0 = new float[nb_elements0];
            float* array1 = new float[nb_elements1];

            for (std::size_t i = 0; i < nb_elements0; ++i) {
                array0[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < nb_elements1; ++i) {
                array1[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d, *array1_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims0);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements0);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements0, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements0);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims0);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements0);

            // input1 CUDA
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>();
            T1_cuda->setDataType(DataType::Float32);
            T1_cuda->setBackend("cuda");
            T1_cuda->resize(dims1);
            op_cuda->associateInput(1, T1_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array1_d), sizeof(float) * nb_elements1);
            cudaMemcpy(array1_d, array1, sizeof(float) * nb_elements1, cudaMemcpyHostToDevice);
            T1_cuda->getImpl()->setRawPtr(array1_d, nb_elements1);

            // input1 CPU
            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize(dims1);
            T1_cpu -> getImpl() -> setRawPtr(array1, nb_elements1);

            // forward CUDA
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            float *computedOutput = new float[nb_elements]();
            cudaMemcpy(computedOutput, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * nb_elements, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computedOutput, *computedCPU));

            delete[] array0;
            delete[] array1;
            delete[] computedOutput;
            cudaFree(array0_d);
            cudaFree(array1_d);

        }
    }

}

TEST_CASE("[gpu/operator] Add(backward)", "[Add][GPU]") {
        std::shared_ptr<Tensor> input_0 = std::make_shared<Tensor>(Array4D<float,3,1,3,2> {
            {                                       //
                {                                   //
                    {{0, 1},{2, 3},{4, 5}}          //
                },                                  //
                {                                   //
                    {{6, 7},{8, 9},{10, 11}}        //
                },                                  //
                {                                   //
                    {{12, 13},{14, 15},{16, 17}}    //
                }                                   //
            }                                       //
        });
        input_0->setBackend("cuda");

        std::shared_ptr<Tensor> input_1 = std::make_shared<Tensor>(Array4D<float,1,3,3,2> {
        {                                       //
            {                                   //
                {{20, 21},{22, 23},{24, 25}},   //
                {{26, 27},{28, 29},{30, 31}},   //
                {{32, 33},{34, 35},{36, 37}}    //
            }                                   //
        }                                       //
        });                                     //
        input_1->setBackend("cuda");

        auto op = std::make_shared<Add_Op>();
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");

        op->associateInput(0, input_0);
        op->associateInput(1, input_1);

        op->forward();

        // Run and test backward operation
        std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array4D<float,3,3,3,2> {
            {                                         //
                {                                     //
                    {{  1,  2},{  3,  4},{  5,  6}},  //
                    {{  7,  8},{  9, 10},{ 11, 12}},  //
                    {{ 13, 14},{ 15, 16},{ 17, 18}}   //
                },                                    //
                {                                     //
                    {{ 19, 20},{ 21, 22},{ 23, 24}},  //
                    {{ 25, 26},{ 27, 28},{ 29, 30}},  //
                    {{ 31, 32},{ 33, 34},{ 35, 36}}   //
                },                                    //
                {                                     //
                    {{ 37, 38},{ 39, 40},{41, 42}},   //
                    {{ 43, 44},{ 45, 46},{47, 48}},   //
                    {{ 49, 50},{ 51, 52},{53, 54}}    //
                }                                     //
            }                                         //
        });                                           //
        myOutputGrad->setBackend("cuda");

        op->getOutput(0)->setGrad(myOutputGrad);
        REQUIRE_NOTHROW(op->backward());

        Tensor expectedInput1Grad = Array4D<float,3,1,3,2> {
            {                                         //
                {                                     //
                    {{21, 24},{27, 30},{33, 36}}      //
                },                                    //
                {                                     //
                    {{75, 78},{81, 84},{87, 90}}      //
                },                                    //
                {                                     //
                    {{129, 132},{135, 138},{141, 144}}//
                }                                     //
            }                                         //
        };
        Tensor expectedInput2Grad = Array4D<float,1,3,3,2> {
            {                                       //
                {                                   //
                    {{57, 60},{63, 66},{69, 72}},   //
                    {{75, 78},{81, 84},{87, 90}},   //
                    {{93, 96},{99, 102},{105, 108}} //
                }                                   //
            }                                       //
        };

        Tensor input1Grad = *(op->getInput(0)->grad());
        input1Grad.setBackend("cpu");
        Tensor input2Grad = *(op->getInput(1)->grad());
        input2Grad.setBackend("cpu");

        REQUIRE(approxEq<cpptype_t<DataType::Float32>>(expectedInput1Grad, input1Grad, (1.0E-5F), (1.0E-8F)));
        REQUIRE(approxEq<cpptype_t<DataType::Float32>>(expectedInput2Grad, input2Grad, (1.0E-5F), (1.0E-8F)));
}