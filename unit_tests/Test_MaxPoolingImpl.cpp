/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/MaxPoolingImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/MaxPoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;


TEST_CASE("[gpu/operator] MaxPooling(forward)", "[MaxPooling][GPU]") {
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,2,5,5> { //NCHW
        {
            {
                {{-0.3848,  0.2166, -0.4373,  0.6142,  0.5277},
                 {0.7995,  0.3638, -1.4589, -1.0843,  1.0918},
            	 {0.7147,  0.0936, -1.2902,  1.2037,  0.4874},
                 {-0.5981,  2.1184, -0.9175,  1.3859,  0.3305},
                 {-1.7700,  0.0563, -0.3914,  0.0538, -0.3955}},

                {{-3.1409, -0.4554,  0.0524,  2.2291,  0.4859},
                 {-0.7465, -0.6567, -2.3703, -0.6386, -1.4152},
                 { 2.2329, -0.5850,  0.0700,  1.2838, -1.7363},
                 { 0.2139,  0.0624, -1.0689, -0.8221, -0.8038},
                 { 0.1886, -0.7840, -0.2313,  0.2651, -1.6244}}
            },
            {
                {{ 0.4371,  1.6417,  0.9129,  0.6325,  0.5438},
                 {-2.3552, -0.8850, -0.0232, -0.5462, -1.2011},
                 {1.7653, -1.6668, -1.0814,  0.6182,  1.2071},
                 {0.9541, -0.5133,  0.8664, -0.8892,  1.4585},
                 {1.0220, -0.5107,  0.1829, -0.2301, -0.4268}},

                {{ 1.0429,  0.6279, -0.2875,  0.7187, -0.1500},
                 {1.6041,  2.9635,  1.4172, -0.7517,  0.5441},
                 {-0.2276,  0.0857,  0.6776, -0.1389, -0.0614},
                 {-0.1547, -0.3435,  0.0650, -0.5095, -1.8073},
                 {1.7217,  0.3999, -0.5953,  1.0604, -0.4126}}
            }
        }
    });
    SECTION("Stride") {
        std::shared_ptr<Node> myMaxPool = MaxPooling({2,2}, "mycdw", {2,2});
        auto op = std::static_pointer_cast<OperatorTensor>(myMaxPool -> getOperator());

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,2,2,2> {
            {
                {
                    {{  0.7995,  0.6142},
                     { 2.1184,  1.3859}},
                    {{ -0.4554,  2.2291},
                     {  2.2329,  1.2838}}
                },
                {
                    {{1.6417,  0.9129},
                     {1.7653,  0.8664}},
                    {{2.9635,  1.4172},
                     {0.0857,  0.6776}}
                }
            }
        });
        myMaxPool->getOperator()->associateInput(0,myInput);
        myMaxPool->getOperator()->setDataType(DataType::Float32);
        myMaxPool->getOperator()->setBackend("cuda");
        myMaxPool->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        constexpr std::size_t kernel = 3;
        constexpr std::size_t stride = 3;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(kernel),
                                                               std::size_t(10));

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create MaxPooling Operator CUDA
            std::shared_ptr<Node> myMaxPoolCuda = MaxPooling({kernel, kernel}, "myMaxPoolCuda", {stride, stride});
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myMaxPoolCuda->getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create MaxPooling Operator CUDA
            std::shared_ptr<Node> myMaxPoolCpu = MaxPooling({kernel, kernel}, "myMaxPoolCpu", {stride, stride});
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myMaxPoolCpu->getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            // generate a random Tensor
            const std::size_t nbDims = 4;
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                dims.push_back(dimSizeDist(gen));
            }

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;

            // Fill input tensor
            float *array0 = new float[nb_elements];
            for (std::size_t i = 0; i < nb_elements; ++i)
            {
                array0[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // Run inference
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] computed_cuda;
            delete[] array0;
            cudaFree(array0_d);
        }
        Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
        Log::info("Total time: {}μs\n", duration.count());
    }
}