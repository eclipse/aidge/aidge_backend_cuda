/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ConvImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/recipes/Recipes.hpp"  // explicitCastMove
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/utils/TensorUtils.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("[cuda/castmove] CastMove(forward)") {
    std::shared_ptr<Tensor> inputTensor =
            std::make_shared<Tensor>(Array4D<int, 2, 1, 5, 5>{{{{{0, 1, 2, 3, 4},
                                                                 {5, 6, 7, 8, 9},
                                                                 {10, 11, 12, 13, 14},
                                                                 {15, 16, 17, 18, 19},
                                                                 {20, 21, 22, 23, 24}}},
                                                               {{{25, 26, 27, 28, 29},
                                                                 {30, 31, 32, 33, 34},
                                                                 {35, 36, 37, 38, 39},
                                                                 {40, 41, 42, 43, 44},
                                                                 {45, 46, 47, 48, 49}}}}});

    std::shared_ptr<Tensor> weight1 = std::make_shared<Tensor>(
            Array4D<int, 3, 1, 3, 3>{{{{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}},
                                      {{{10, 11, 12}, {13, 14, 15}, {16, 17, 18}}},
                                      {{{19, 20, 21}, {22, 23, 24}, {25, 26, 27}}}}});

    std::shared_ptr<Tensor> bias1 = std::make_shared<Tensor>(Array1D<int, 3>{{1, 2, 3}});

    SECTION("Test implicit") {
        std::shared_ptr<GraphView> g =
                Sequential({
                    Conv(1, 3, {3, 3}, "conv1"),
                    Conv(3, 4, {1, 1}, "conv2"),
                    Conv(4, 3, {1, 1}, "conv3")});

        g->getNode("conv1")->getOperator()->setInput(0, inputTensor);
        g->getNode("conv1")->getOperator()->setInput(1, weight1);
        g->getNode("conv1")->getOperator()->setInput(2, bias1);

        std::shared_ptr<Tensor> weight2 =
                std::make_shared<Tensor>(Array4D<int, 4, 3, 1, 1>{{{{{1}}, {{2}}, {{3}}},
                                                                   {{{4}}, {{5}}, {{6}}},
                                                                   {{{7}}, {{8}}, {{9}}},
                                                                   {{{10}}, {{11}}, {{12}}}}});
        std::shared_ptr<Tensor> bias2 = std::make_shared<Tensor>(Array1D<int, 4>{{1, 2, 3, 4}});
        g->getNode("conv2")->getOperator()->setInput(1, weight2);
        g->getNode("conv2")->getOperator()->setInput(2, bias2);
        // *(g->getNode("conv2")->getOperator()->input(1, weight2);

        std::shared_ptr<Tensor> weight3 = std::make_shared<Tensor>(
                Array4D<int, 3, 4, 1, 1>{{{{{1}}, {{2}}, {{3}}, {{4}}},
                                          {{{5}}, {{6}}, {{7}}, {{8}}},
                                          {{{9}}, {{10}}, {{11}}, {{12}}}}});
        std::shared_ptr<Tensor> bias3 = std::make_shared<Tensor>(Array1D<int, 3>{{1, 2, 3}});
        g->getNode("conv3")->getOperator()->setInput(1, weight3);
        g->getNode("conv3")->getOperator()->setInput(2, bias3);

        // input->addChild(g);
        g->setDataType(Aidge::DataType::Float32);
        g->getNode("conv1")->getOperator()->setDataType(DataType::Float16);
        g->getNode("conv3")->getOperator()->setDataType(DataType::Float64);

        g->setBackend("cuda");
        g->forwardDims();
        SequentialScheduler scheduler(g);
        REQUIRE_NOTHROW(scheduler.forward());
        scheduler.saveSchedulingDiagram("schedulingSequential");

        std::shared_ptr<Tensor> expectedOutput1 = std::make_shared<Tensor>(Array4D<int, 2, 3, 3, 3>{
                {{{{367, 412, 457}, {592, 637, 682}, {817, 862, 907}},
                  {{854, 980, 1106}, {1484, 1610, 1736}, {2114, 2240, 2366}},
                  {{1341, 1548, 1755}, {2376, 2583, 2790}, {3411, 3618, 3825}}},
                 {{{1492, 1537, 1582}, {1717, 1762, 1807}, {1942, 1987, 2032}},
                  {{4004, 4130, 4256}, {4634, 4760, 4886}, {5264, 5390, 5516}},
                  {{6516, 6723, 6930}, {7551, 7758, 7965}, {8586, 8793, 9000}}}}});

        std::shared_ptr<Tensor> expectedOutput2 = std::make_shared<Tensor>(Array4D<int, 2, 4, 3, 3>{
                {{{{6099, 7017, 7935}, {10689, 11607, 12525}, {15279, 16197, 17115}},
                  {{13786, 15838, 17890}, {24046, 26098, 28150}, {34306, 36358, 38410}},
                  {{21473, 24659, 27845}, {37403, 40589, 43775}, {53333, 56519, 59705}},
                  {{29160, 33480, 37800}, {50760, 55080, 59400}, {72360, 76680, 81000}}},
                 {{{29049, 29967, 30885}, {33639, 34557, 35475}, {38229, 39147, 40065}},
                  {{65086, 67138, 69190}, {75346, 77398, 79450}, {85606, 87658, 89710}},
                  {{101123, 104309, 107495}, {117053, 120239, 123425}, {132983, 136169, 139355}},
                  {{137160, 141480, 145800}, {158760, 163080, 167400}, {180360, 184680, 189000}}}}});

        std::shared_ptr<Tensor> expectedOutput3 = std::make_shared<Tensor>(Array4D<int, 2, 3, 3, 3>{
                {{{{214731, 246591, 278451}, {374031, 405891, 437751}, {533331, 565191, 597051}},
                  {{496804, 570568, 644332}, {865624, 939388, 1013152}, {1234444, 1308208, 1381972}},
                  {{778877, 894545, 1010213}, {1357217, 1472885, 1588553}, {1935557, 2051225, 2166893}}},
                 {{{1011231, 1043091, 1074951}, {1170531, 1202391, 1234251}, {1329831, 1361691, 1393551}},
                  {{2340904, 2414668, 2488432}, {2709724, 2783488, 2857252}, {3078544, 3152308, 3226072}},
                  {{3670577, 3786245, 3901913}, {4248917, 4364585, 4480253}, {4827257, 4942925, 5058593}}}}});

        std::shared_ptr<Tensor> other1 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv1")->getOperator())->getOutput(0);
        Tensor hostOther1(other1->dataType());
        hostOther1.setBackend("cpu");
        hostOther1.copyCastFrom(*other1);
        REQUIRE(approxEq<half_float::half, int>(hostOther1, *expectedOutput1, 0.001, 0.0));

        std::shared_ptr<Tensor> other2 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv2")->getOperator())->getOutput(0);
        Tensor hostOther2(other2->dataType());
        hostOther2.setBackend("cpu");
        hostOther2.copyCastFrom(*other2);
        REQUIRE(approxEq<float, int>(hostOther2, *expectedOutput2, 0.001, 0.0));

        std::shared_ptr<Tensor> other3 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv3")->getOperator())->getOutput(0);
        Tensor hostOther3(other3->dataType());
        hostOther3.setBackend("cpu");
        hostOther3.copyCastFrom(*other3);
        REQUIRE(approxEq<double, int>(hostOther3, *expectedOutput3, 0.001, 0.0));
    }

    SECTION("Test explicit") {
        std::shared_ptr<GraphView> g =
                Sequential({
                    Conv(1, 3, {3, 3}, "conv1"),
                    Conv(3, 4, {1, 1}, "conv2"),
                    Conv(4, 3, {1, 1}, "conv3")});

        g->getNode("conv1")->getOperator()->setInput(0, inputTensor);
        g->getNode("conv1")->getOperator()->setInput(1, weight1);
        g->getNode("conv1")->getOperator()->setInput(2, bias1);

        std::shared_ptr<Tensor> weight2 =
                std::make_shared<Tensor>(Array4D<int, 4, 3, 1, 1>{{{{{1}}, {{2}}, {{3}}},
                                                                   {{{4}}, {{5}}, {{6}}},
                                                                   {{{7}}, {{8}}, {{9}}},
                                                                   {{{10}}, {{11}}, {{12}}}}});
        std::shared_ptr<Tensor> bias2 = std::make_shared<Tensor>(Array1D<int, 4>{{1, 2, 3, 4}});
        g->getNode("conv2")->getOperator()->setInput(1, weight2);
        g->getNode("conv2")->getOperator()->setInput(2, bias2);
        // *(g->getNode("conv2")->getOperator()->input(1, weight2);

        std::shared_ptr<Tensor> weight3 = std::make_shared<Tensor>(
                Array4D<int, 3, 4, 1, 1>{{{{{1}}, {{2}}, {{3}}, {{4}}},
                                          {{{5}}, {{6}}, {{7}}, {{8}}},
                                          {{{9}}, {{10}}, {{11}}, {{12}}}}});
        std::shared_ptr<Tensor> bias3 = std::make_shared<Tensor>(Array1D<int, 3>{{1, 2, 3}});
        g->getNode("conv3")->getOperator()->setInput(1, weight3);
        g->getNode("conv3")->getOperator()->setInput(2, bias3);

        // input->addChild(g);
        g->setDataType(Aidge::DataType::Float32);
        g->getNode("conv1")->getOperator()->setDataType(DataType::Float16);
        g->getNode("conv3")->getOperator()->setDataType(DataType::Float64);

        explicitCastMove(g);
        g->setBackend("cuda");
        g->forwardDims();

        SequentialScheduler scheduler(g);
        REQUIRE_NOTHROW(scheduler.forward());
        scheduler.saveSchedulingDiagram("schedulingSequential");

        std::shared_ptr<Tensor> expectedOutput1 = std::make_shared<Tensor>(Array4D<int, 2, 3, 3, 3>{
                {{{{367, 412, 457}, {592, 637, 682}, {817, 862, 907}},
                  {{854, 980, 1106}, {1484, 1610, 1736}, {2114, 2240, 2366}},
                  {{1341, 1548, 1755}, {2376, 2583, 2790}, {3411, 3618, 3825}}},
                 {{{1492, 1537, 1582}, {1717, 1762, 1807}, {1942, 1987, 2032}},
                  {{4004, 4130, 4256}, {4634, 4760, 4886}, {5264, 5390, 5516}},
                  {{6516, 6723, 6930}, {7551, 7758, 7965}, {8586, 8793, 9000}}}}});

        std::shared_ptr<Tensor> expectedOutput2 = std::make_shared<Tensor>(Array4D<int, 2, 4, 3, 3>{
                {{{{6099, 7017, 7935}, {10689, 11607, 12525}, {15279, 16197, 17115}},
                  {{13786, 15838, 17890}, {24046, 26098, 28150}, {34306, 36358, 38410}},
                  {{21473, 24659, 27845}, {37403, 40589, 43775}, {53333, 56519, 59705}},
                  {{29160, 33480, 37800}, {50760, 55080, 59400}, {72360, 76680, 81000}}},
                 {{{29049, 29967, 30885}, {33639, 34557, 35475}, {38229, 39147, 40065}},
                  {{65086, 67138, 69190}, {75346, 77398, 79450}, {85606, 87658, 89710}},
                  {{101123, 104309, 107495}, {117053, 120239, 123425}, {132983, 136169, 139355}},
                  {{137160, 141480, 145800}, {158760, 163080, 167400}, {180360, 184680, 189000}}}}});

        std::shared_ptr<Tensor> expectedOutput3 = std::make_shared<Tensor>(Array4D<int, 2, 3, 3, 3>{
                {{{{214731, 246591, 278451}, {374031, 405891, 437751}, {533331, 565191, 597051}},
                  {{496804, 570568, 644332}, {865624, 939388, 1013152}, {1234444, 1308208, 1381972}},
                  {{778877, 894545, 1010213}, {1357217, 1472885, 1588553}, {1935557, 2051225, 2166893}}},
                 {{{1011231, 1043091, 1074951}, {1170531, 1202391, 1234251}, {1329831, 1361691, 1393551}},
                  {{2340904, 2414668, 2488432}, {2709724, 2783488, 2857252}, {3078544, 3152308, 3226072}},
                  {{3670577, 3786245, 3901913}, {4248917, 4364585, 4480253}, {4827257, 4942925, 5058593}}}}});

        std::shared_ptr<Tensor> other1 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv1")->getOperator())->getOutput(0);
        Tensor hostOther1(other1->dataType());
        hostOther1.setBackend("cpu");
        hostOther1.copyCastFrom(*other1);
        REQUIRE(approxEq<half_float::half, int>(hostOther1, *expectedOutput1, 0.001, 0.0));

        std::shared_ptr<Tensor> other2 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv2")->getOperator())->getOutput(0);
        Tensor hostOther2(other2->dataType());
        hostOther2.setBackend("cpu");
        hostOther2.copyCastFrom(*other2);
        REQUIRE(approxEq<float, int>(hostOther2, *expectedOutput2, 0.001, 0.0));

        std::shared_ptr<Tensor> other3 = std::static_pointer_cast<Conv_Op<2>>(g->getNode("conv3")->getOperator())->getOutput(0);
        Tensor hostOther3(other3->dataType());
        hostOther3.setBackend("cpu");
        hostOther3.copyCastFrom(*other3);
        REQUIRE(approxEq<double, int>(hostOther3, *expectedOutput3, 0.001, 0.0));
    }
}
