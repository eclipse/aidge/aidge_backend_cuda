/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ArgMaxImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] ArgMax(forward)", "[ArgMax][CPU]") {
    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,2,3,4> {
            {
                {
                    { 1.0, 2.0, 3.0, 4.0},
                    { 8.0, 0.0, 17.0, 1.0},
                    { 5.0, 10.0, 6.0, 0.0}
                },
                {
                    { 7.0, 1.0, 9.0, 4.0},
                    { 0.0, 8.0, 4.0, 2.0},
                    { 9.0, 2.0, 0.0, 5.0}
                }
            }
        });
        myInput->setBackend("cuda");

        SECTION("Axis 2") {

            const Tensor myOutput = Tensor(Array3D<float,2,3, 1> {
               {
                    {
                        {3.0},
                        {2.0},
                        {1.0}
                    },
                    {
                        {2.0},
                        {1.0},
                        {0.0}
                    }
               }
            });

            std::shared_ptr<ArgMax_Op> op = std::make_shared<ArgMax_Op>(2);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
            REQUIRE(approxEq<float>(cudaOutput, myOutput));

        }
        SECTION("Axis 2 with keep_dims false") {

            const Tensor myOutput = Tensor(Array2D<float,2,3> {
               {
                    { 3.0, 2.0, 1.0 },
                    { 2.0, 1.0, 0.0 }
               }
            });

            std::shared_ptr<ArgMax_Op> op = std::make_shared<ArgMax_Op>(2,0);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
            REQUIRE(approxEq<float>(cudaOutput, myOutput));
        }
        SECTION("Axis 1") {
            const Tensor myOutput = Tensor(Array3D<float,2,1,4> {
                {
                    {
                        { 1.0, 2.0, 1.0, 0.0 }
                    },
                    {
                        { 2.0, 1.0, 0.0, 2.0 }
                    }
                }
            });

            std::shared_ptr<ArgMax_Op> op = std::make_shared<ArgMax_Op>(1);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
            REQUIRE(approxEq<float>(cudaOutput, myOutput));
        }
        SECTION("Axis 0") {
            const Tensor myOutput = Tensor(Array3D<float,1,3,4> {
                {
                    {
                        { 1.0, 0.0, 1.0, 0.0 },
                        { 0.0, 1.0, 0.0, 1.0 },
                        { 1.0, 0.0, 0.0, 1.0 }
                    }
                }
            });

            std::shared_ptr<ArgMax_Op> op = std::make_shared<ArgMax_Op>(0);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
            REQUIRE(approxEq<float>(cudaOutput, myOutput));
        }
    }
    SECTION("Select_Last_Index") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array1D<float,10> {
            {
                1.0, 5.0, 9.0, 0.0, 6.0, 2.0, 9.0, 4.0, 3.0, 9.0
            }
        });
        const Tensor myOutput = Tensor(Array1D<float,1> {{9}});

        std::shared_ptr<ArgMax_Op> op = std::make_shared<ArgMax_Op>(0,1,1);
        op->associateInput(0,myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        std::shared_ptr<Tensor> outputFallback;
        const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
        REQUIRE(approxEq<float>(cudaOutput, myOutput));
    }
}