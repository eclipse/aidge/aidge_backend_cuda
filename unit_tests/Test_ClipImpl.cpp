/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/ClipImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ClipImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace std::chrono;
namespace Aidge {

TEST_CASE("[gpu/operator] Clip", "[Clip][GPU]")
{

        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(0, 10);
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),std::size_t(20));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(4), std::size_t(6));
        std::uniform_int_distribution<int> boolDist(0,1);

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> startcuda;
        std::chrono::time_point<std::chrono::system_clock> endcuda;
        std::chrono::time_point<std::chrono::system_clock> startcpu;
        std::chrono::time_point<std::chrono::system_clock> endcpu;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            std::shared_ptr<Clip_Op> op_cuda = std::make_shared<Clip_Op>(1.0, 3.0);

            // Create Div Operator CPU
            std::shared_ptr<Clip_Op> op_cpu = std::make_shared<Clip_Op>(1.0, 3.0);
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");


            const std::size_t nbDims = nbDimsDist(gen);
            std::vector<std::size_t> dims0;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                    const std::size_t dim = dimSizeDist(gen);
                    dims0.push_back(dim);
            }

            const std::size_t nb_elements0 = std::accumulate(dims0.cbegin(), dims0.cend(), std::size_t(1), std::multiplies<std::size_t>());
            float* array0 = new float[nb_elements0];
            for (std::size_t i = 0; i < nb_elements0; ++i) {
                    array0[i] = valueDist(gen);
            }



            float* array0_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims0);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements0);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements0, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements0);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims0);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements0);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            startcuda = std::chrono::system_clock::now();
            op_cuda->forward();
            endcuda = std::chrono::system_clock::now();


            // forward CPU
            startcpu = std::chrono::system_clock::now();
            op_cpu->forward();
            endcpu = std::chrono::system_clock::now();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

                std::shared_ptr<Tensor> outputFallback;
                const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));;
                REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

                delete[] array0;
                cudaFree(array0_d);

                const auto duration_cuda = duration_cast<milliseconds>(endcuda - startcuda).count();
                fmt::print("CUDA exec time: {} ms\n", duration_cuda);
                const auto duration_cpu = duration_cast<milliseconds>(endcpu - startcpu).count();
                fmt::print("CPU exec time: {} ms\n", duration_cpu);
                //Exec time difference (CPU - CUDA):
                const float ratio = static_cast<float>(duration_cuda) / static_cast<float>(duration_cpu);
                fmt::print("Exec time ratio (CUDA/CPU): {} %\n", ratio*100);

        }
}
} // namespace Aidge
