/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>  // cudaMemcpy, cudaMemcpyDeviceToHost

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/DivImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/DivImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Div.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

TEST_CASE("[gpu/operator] Div", "[Div][GPU]") {
constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),
                                                               std::size_t(10));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(4), std::size_t(5));
        std::uniform_int_distribution<int> boolDist(0,1);

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create Div Operator CUDA
            std::shared_ptr<Div_Op> op_cuda = std::make_shared<Div_Op>();
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create Div Operator CPU
            std::shared_ptr<Div_Op> op_cpu = std::make_shared<Div_Op>();
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            const std::size_t nbDims = nbDimsDist(gen);
            std::vector<std::size_t> dims0, dims1, dims;
            for (std::size_t i = 0; i < nbDims; ++i) {
                const std::size_t dim = dimSizeDist(gen);
                dims0.push_back(dim);
                dims1.push_back(boolDist(gen) ? 1 : dim);
                dims.push_back(std::max(dims0[i], dims1[i]));
            }

            const std::size_t nb_elements0 = std::accumulate(dims0.cbegin(), dims0.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t nb_elements1 = std::accumulate(dims1.cbegin(), dims1.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;
            float* array0 = new float[nb_elements0];
            float* array1 = new float[nb_elements1];

            for (std::size_t i = 0; i < nb_elements0; ++i) {
                array0[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < nb_elements1; ++i) {
                array1[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d, *array1_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims0);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements0);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements0, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements0);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims0);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements0);

            // input1 CUDA
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>();
            T1_cuda->setDataType(DataType::Float32);
            T1_cuda->setBackend("cuda");
            T1_cuda->resize(dims1);
            op_cuda->associateInput(1, T1_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array1_d), sizeof(float) * nb_elements1);
            cudaMemcpy(array1_d, array1, sizeof(float) * nb_elements1, cudaMemcpyHostToDevice);
            T1_cuda->getImpl()->setRawPtr(array1_d, nb_elements1);

            // input1 CPU
            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize(dims1);
            T1_cpu -> getImpl() -> setRawPtr(array1, nb_elements1);

            // forward CUDA
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            // forward CPU
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
            REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

            delete[] array0;
            delete[] array1;
            cudaFree(array0_d);
            cudaFree(array1_d);
        }
        fmt::print("total time: {}μs", duration.count());
}
} // namespace Aidge
