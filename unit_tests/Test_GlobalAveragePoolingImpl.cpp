/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/GlobalAveragePoolingImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/GlobalAveragePoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

TEST_CASE("[gpu/operator] GlobalAveragePooling",
          "[GlobalAveragePooling][GPU]") {

    SECTION("4D-Tensor")
    {
      std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,1,3,4,4> { //NCHW
        {
            {
                {{0, 1, 2, 3},
                 {4, 5, 6, 7},
            	   {8, 9, 10, 11},
                 {12, 13, 14, 15}},

                {{16, 17, 18, 19},
                 {20, 21, 22, 23},
                 {24, 25, 26, 27},
                 {28, 29, 30, 31}},

                {{32, 33, 34, 35},
                 {36, 37, 38, 39},
                 {40, 41, 42, 43},
                 {44, 45, 46, 47}}
            }
        }
     });
    std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,1,3,1,1> {
        {
          {
            {{ 7.5 }},
            {{ 23.5 }},
            {{ 39.5 }}
          }
        }
      });
      myInput->setBackend("cuda");
      myInput->setDataType(DataType::Float32);
      // Create MyGlobalAveragePooling Operator
      std::shared_ptr<Node> globAvgPool = GlobalAveragePooling();
      auto op = std::static_pointer_cast<OperatorTensor>(globAvgPool->getOperator());
      op->setDataType(DataType::Float32);
      op->setBackend("cuda");
      op->associateInput(0, myInput);

      globAvgPool->forward();
      float* computedOutput   = new float[myOutput->size()]();
      cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

      for(int i = 0; i < myOutput->size(); i++){
          const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
          REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
      }

      delete[] computedOutput;
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),
                                                               std::size_t(10));

        // To measure execution time of 'AveragePooling_Op::forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create GlobalAveragePooling Operator CUDA
            std::shared_ptr<Node> myGAvgPoolCuda = GlobalAveragePooling();
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myGAvgPoolCuda->getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create GlobalAveragePooling Operator CUDA
            std::shared_ptr<Node> myGAvgPoolCpu = GlobalAveragePooling();
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myGAvgPoolCpu->getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            // generate a random Tensor
            const std::size_t nbDims = 4;
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                dims.push_back(dimSizeDist(gen));
            }

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;

            // Fill input tensor
            float *array0 = new float[nb_elements];
            for (std::size_t i = 0; i < nb_elements; ++i)
            {
                array0[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // Run inference
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] computed_cuda;
            delete[] array0;
            cudaFree(array0_d);
        }
        Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
        Log::info("Total time: {}μs\n", duration.count());
    }
}
} // namespace Aidge
