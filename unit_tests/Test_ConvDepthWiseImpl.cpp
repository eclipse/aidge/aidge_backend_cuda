/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>  // cudaMemcpy, cudaMemcpyDeviceToHost
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/ConvDepthWiseImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ConvImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] ConvDepthWise(forward)", "[ConvDepthWise][CPU]") {
    SECTION("Deterministic Input") {
        std::shared_ptr<Node> myCDW = ConvDepthWise(4, {3,3}, "mycdw");
        auto op = std::static_pointer_cast<OperatorTensor>(myCDW -> getOperator());
        std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array4D<float,4,1,3,3> {
            {
                {{
                    {  0,  1,  2},
                    {  3,  4,  5},
                    {  6,  7,  8}

                }},
                {{
                    { 27, 28, 29},
                    { 30, 31, 32},
                    { 33, 34, 35}

                }},
                {{
                    { 54, 55, 56},
                    { 57, 58, 59},
                    { 60, 61, 62}
                }},
                {{
                    { 81, 82, 83},
                    { 84, 85, 86},
                    { 87, 88, 89}
                }}
            }
        });
        std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float,4> {{7,0,9,0}});
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,4,5,5> { //NCHW
            {
                {
                    {{  0,   1,   2,   3,   4},
                    {  5,   6,   7,   8,   9},
                    { 10,  11,  12,  13,  14},
                    { 15,  16,  17,  18,  19},
                    { 20,  21,  22,  23,  24}},

                    {{ 25,  26,  27,  28,  29},
                    { 30,  31,  32,  33,  34},
                    { 35,  36,  37,  38,  39},
                    { 40,  41,  42,  43,  44},
                    { 45,  46,  47,  48,  49}},

                    {{ 50,  51,  52,  53,  54},
                    { 55,  56,  57,  58,  59},
                    { 60,  61,  62,  63,  64},
                    { 65,  66,  67,  68,  69},
                    { 70,  71,  72,  73,  74}},

                    {{ 75,  76,  77,  78,  79},
                    { 80,  81,  82,  83,  84},
                    { 85,  86,  87,  88,  89},
                    { 90,  91,  92,  93,  94},
                    { 95,  96,  97,  98,  99}}
                },
                {
                    {{100, 101, 102, 103, 104},
                    {105, 106, 107, 108, 109},
                    {110, 111, 112, 113, 114},
                    {115, 116, 117, 118, 119},
                    {120, 121, 122, 123, 124}},

                    {{125, 126, 127, 128, 129},
                    {130, 131, 132, 133, 134},
                    {135, 136, 137, 138, 139},
                    {140, 141, 142, 143, 144},
                    {145, 146, 147, 148, 149}},

                    {{150, 151, 152, 153, 154},
                    {155, 156, 157, 158, 159},
                    {160, 161, 162, 163, 164},
                    {165, 166, 167, 168, 169},
                    {170, 171, 172, 173, 174}},

                    {{175, 176, 177, 178, 179},
                    {180, 181, 182, 183, 184},
                    {185, 186, 187, 188, 189},
                    {190, 191, 192, 193, 194},
                    {195, 196, 197, 198, 199}}
                }
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,4,3,3> {
            {
                {
                    {{   319,    355,    391},
                    {   499,    535,    571},
                    {   679,    715,    751}},

                    {{  8745,   9024,   9303},
                    { 10140,  10419,  10698},
                    { 11535,  11814,  12093}},

                    {{ 29337,  29859,  30381},
                    { 31947,  32469,  32991},
                    { 34557,  35079,  35601}},

                    {{ 62061,  62826,  63591},
                    { 65886,  66651,  67416},
                    { 69711,  70476,  71241}}
                },
                {
                    {{  3919,   3955,   3991},
                    {  4099,   4135,   4171},
                    {  4279,   4315,   4351}},

                    {{ 36645,  36924,  37203},
                    { 38040,  38319,  38598},
                    { 39435,  39714,  39993}},

                    {{ 81537,  82059,  82581},
                    { 84147,  84669,  85191},
                    { 86757,  87279,  87801}},

                    {{138561, 139326, 140091},
                    {142386, 143151, 143916},
                    {146211, 146976, 147741}}
                }
            }
        });
        myInput->setBackend("cuda");
        myWeights->setBackend("cuda");
        myBias->setBackend("cuda");
        op -> associateInput(0, myInput);
        op -> associateInput(1, myWeights);
        op -> associateInput(2, myBias);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");

        myCDW -> forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> kernelDist(1, std::size_t(5));
        std::uniform_int_distribution<std::size_t> dimSizeDist(1, std::size_t(10));

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            const std::size_t kernel = kernelDist(gen);
            std::uniform_int_distribution<std::size_t> resolutionDist(std::size_t(kernel+2),
                                                               std::size_t(10));
            const std::size_t nbDims = 4;
            // input  (batch, ch, Xin, Yin)
            // weight (outCh, ch, kernelX, kernelY)
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i) {
                if(i < 2)
                    dims.push_back(dimSizeDist(gen));
                else
                    dims.push_back(resolutionDist(gen));
            }
            dims[1] = 1; // TODO FIX: ConvDepthWise doesn't give the same output in CUDA as in CPU unless channels is 1
            const std::size_t nbChannels = dims[1];
            const std::vector<std::size_t> dimsW{nbChannels,nbChannels,kernel,kernel};

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t wieghtSize = std::accumulate(dimsW.cbegin(), dimsW.cend(), std::size_t(1), std::multiplies<std::size_t>());

            // Create ConvDepthWise Operator CUDA
            std::shared_ptr<Node> myConvCUDA = ConvDepthWise(nbChannels,{kernel,kernel}, "myconvcuda");
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myConvCUDA -> getOperator());

            // Create ConvDepthWise Operator CPU
            std::shared_ptr<Node> myConvCPU = ConvDepthWise(nbChannels,{kernel,kernel}, "myconvcpu");
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myConvCPU -> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");


            float* array0 = new float[nb_elements];
            float* weights = new float[wieghtSize];
            float* bias = new float[nbChannels];

            for (std::size_t i = 0; i < nb_elements; ++i) {
                array0[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < wieghtSize; ++i) {
                weights[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < nbChannels; ++i) {
                bias[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d, *weight_d, *bias_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // weight CUDA
            std::shared_ptr<Tensor> Tw_cuda = std::make_shared<Tensor>();
            Tw_cuda->setDataType(DataType::Float32);
            Tw_cuda->setBackend("cuda");
            Tw_cuda->resize(dimsW);
            op_cuda->associateInput(1, Tw_cuda);
            cudaMalloc(reinterpret_cast<void **>(&weight_d), sizeof(float) * wieghtSize);
            cudaMemcpy(weight_d, weights, sizeof(float) * wieghtSize, cudaMemcpyHostToDevice);
            Tw_cuda->getImpl()->setRawPtr(weight_d, wieghtSize);

            // weight CPU
            std::shared_ptr<Tensor> Tw_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,Tw_cpu);
            Tw_cpu->setDataType(DataType::Float32);
            Tw_cpu->setBackend("cpu");
            Tw_cpu->resize(dimsW);
            Tw_cpu -> getImpl() -> setRawPtr(weights, wieghtSize);

            // bias CUDA
            std::shared_ptr<Tensor> Tb_cuda = std::make_shared<Tensor>();
            Tb_cuda->setDataType(DataType::Float32);
            Tb_cuda->setBackend("cuda");
            Tb_cuda->resize({nbChannels});
            op_cuda->associateInput(2, Tb_cuda);
            cudaMalloc(reinterpret_cast<void **>(&bias_d), sizeof(float) * nbChannels);
            cudaMemcpy(bias_d, bias, sizeof(float) * nbChannels, cudaMemcpyHostToDevice);
            Tb_cuda->getImpl()->setRawPtr(bias_d, nbChannels);

            // bias CPU
            std::shared_ptr<Tensor> Tb_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(2,Tb_cpu);
            Tb_cpu->setDataType(DataType::Float32);
            Tb_cpu->setBackend("cpu");
            Tb_cpu->resize({nbChannels});
            Tb_cpu -> getImpl() -> setRawPtr(bias, nbChannels);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] array0;
            delete[] weights;
            delete[] bias;
            delete[] computed_cuda;
            cudaFree(array0_d);
            cudaFree(weight_d);
            cudaFree(bias_d);
        }
        fmt::print("total time: {}μs", duration.count());
    }
}