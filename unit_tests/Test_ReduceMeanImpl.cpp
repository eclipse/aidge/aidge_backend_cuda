/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <memory>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/ReduceMeanImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ReduceMeanImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

TEST_CASE("[gpu/operator] ReduceMean(forward)", "[ReduceMean][GPU]") {
    SECTION("KeepDims") {
        SECTION("test 1") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,2,2> {
                {
                    {
                        { 5.0, 1.0 },
                        { 20.0, 2.0 }
                    },
                    {
                        { 30.0, 1.0 },
                        { 40.0, 2.0 }
                    },
                    {
                        { 55.0, 1.0 },
                        { 60.0, 2.0 }
                    }
                }
            });
            myInput->setBackend("cuda");
            std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array3D<float,3,1,2> {
                {

                    {{ 12.5, 1.5 }},
                    {{ 35.0, 1.5 }},
                    {{ 57.5, 1.5 }}
                }
            });

            std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({1}));
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);
            for(std::size_t i = 0; i < myOutput->size(); i++){
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
        SECTION("test 2") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,3,2> {
                {
                    {
                        { 0.0, 0.0 },
                        { 1.0, 1.0 },
                        { 2.0, 2.0 }
                    },
                    {
                        { 3.0, 3.0 },
                        { 4.0, 4.0 },
                        { 5.0, 5.0 }
                    },
                    {
                        { 6.0, 6.0 },
                        { 7.0, 7.0 },
                        { 8.0, 8.0 }
                    }
                }
            });
            myInput->setBackend("cuda");
            std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array3D<float,3,1,1> {
                {

                    {{ 1.0 }},
                    {{ 4.0 }},
                    {{ 7.0 }}
                }
            });

            std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({1, 2}));
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);
            for(std::size_t i = 0; i < myOutput->size(); ++i) {
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
    }
    SECTION("not_KeepDims") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,2,2> {
            {
                {
                    { 5.0, 1.0 },
                    { 20.0, 2.0 }
                },
                {
                    { 30.0, 1.0 },
                    { 40.0, 2.0 }
                },
                {
                    { 55.0, 1.0 },
                    { 60.0, 2.0 }
                }
            }
        });
        myInput->setBackend("cuda");
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array2D<float,3,2> {
            {
                { 12.5, 1.5 },
                { 35.0, 1.5 },
                { 57.5, 1.5 }
            }
        });

        std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({1}), false);
        op->associateInput(0,myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();
        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);
        for(std::size_t i = 0; i < myOutput->size(); ++i) {
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            fmt::print("Computed: {}, target: {}\n", computedOutput[i], targetOutput);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;

    }
    SECTION("all_axes") {
        SECTION("1") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,2,2> {
                {
                    {
                        { 5.0, 1.0 },
                        { 20.0, 2.0 }
                    },
                    {
                        { 30.0, 1.0 },
                        { 40.0, 2.0 }
                    },
                    {
                        { 55.0, 1.0 },
                        { 60.0, 2.0 }
                    }
                }
            });
            myInput->setBackend("cuda");
            std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array1D<float,1> {
                {18.25}
            });

            std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({0, 1, 2}), false);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();
            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);
            for(std::size_t i = 0; i < myOutput->size(); ++i) {
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
        SECTION("2") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array2D<float,5,4> {
               {{ 0.004232f, 0.105120f, 0.045124f, 0.009205f},
                { 0.000766f, 0.272162f, 0.503560f, 0.044163f},
                { 0.049755f, 0.000305f, 0.143634f, 0.013253f},
                { 0.096258f, 0.311231f, 0.358143f, 0.000452f},
                { 0.468617f, 0.015693f, 0.145316f, 0.000105f}}
            });
            myInput->setBackend("cuda");
            std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array1D<float,1> {
                {0.1293547f}
            });

            std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({0, 1}), false);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);
            for(std::size_t i = 0; i < myOutput->size(); i++){
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
        SECTION("noop_with_empty_axes") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,2,2> {
                {
                    {
                        { 5.0, 1.0 },
                        { 20.0, 2.0 }
                    },
                    {
                        { 30.0, 1.0 },
                        { 40.0, 2.0 }
                    },
                    {
                        { 55.0, 1.0 },
                        { 60.0, 2.0 }
                    }
                }
            });
            myInput->setBackend("cuda");
            std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>(), false, true);
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");
            op->forward();

            myInput->setBackend("cpu");
            float* computedOutput   = new float[myInput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myInput->size(), cudaMemcpyDeviceToHost);
            for(std::size_t i = 0; i < myInput->size(); i++){
                const float targetOutput = *(static_cast<float*>(myInput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
    }
}

TEST_CASE("[gpu/operator] ReduceMean(backward)", "[ReduceMean][GPU]") {
    SECTION("KeepDims") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,3,2,2> {
            {
                {
                    { 5.0, 1.0 },
                    { 20.0, 2.0 }
                },
                {
                    { 30.0, 1.0 },
                    { 40.0, 2.0 }
                },
                {
                    { 55.0, 1.0 },
                    { 60.0, 2.0 }
                }
            }
        });
        myInput->setBackend("cuda");


        std::shared_ptr<ReduceMean_Op> op = std::make_shared<ReduceMean_Op>(std::vector<std::int32_t>({1}));
        op->associateInput(0,myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();


        std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array3D<float,3,1,2> {
            {

                {{ 1.0, 2.0 }},
                {{ 3.0, 4.0 }},
                {{ 5.0, 6.0 }}
            }
        });
        std::shared_ptr<Tensor> expectedInputGrad = std::make_shared<Tensor>(Array3D<float,3,2,2> {
            {
                {
                    { 1.0, 2.0 },
                    { 1.0, 2.0 }
                },
                {
                    { 3.0, 4.0 },
                    { 3.0, 4.0 }
                },
                {
                    { 5.0, 6.0 },
                    { 5.0, 6.0 }
                }
            }
        });
        myOutputGrad->setBackend("cuda");
        op->getOutput(0)->setGrad(myOutputGrad);
        REQUIRE_NOTHROW(op->backward());

        float *computedGradCuda = new float[expectedInputGrad->size()]();
        cudaMemcpy(computedGradCuda, op->getInput(0)->grad()->getImpl()->rawPtr(), sizeof(float) * expectedInputGrad->size(), cudaMemcpyDeviceToHost);

        for(std::size_t i = 0; i < expectedInputGrad->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedInputGrad->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedGradCuda[i] - targetOutput) < 1e-6);
        }

        delete[] computedGradCuda;
    }
}
}
