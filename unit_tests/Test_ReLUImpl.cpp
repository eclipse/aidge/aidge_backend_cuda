/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ReLUImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;


TEST_CASE("[gpu/operator] ReLU(forward)", "[ReLU][GPU]") {
    SECTION("Constant Input") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                },
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                }
            }
        });

        std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }
    SECTION("Random Input")
    {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),
                                                               std::size_t(10));

        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(8)); // Max nbDims supported by cudnn is 8
        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create ReLU Operator
            std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
            op->setDataType(DataType::Float32);
            op->setBackend("cuda");

            // generate a random Tensor
            const std::size_t nbDims = nbDimsDist(gen);
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                dims.push_back(dimSizeDist(gen));
            }

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;

            // Create the input Tensor
            std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
            T0->setDataType(DataType::Float32);
            T0->setBackend("cuda");
            T0->resize(dims);
            op->associateInput(0, T0);

            // Fill input tensor
            float *input_h = new float[nb_elements];
            float *output_h = new float[nb_elements];
            for (std::size_t i = 0; i < nb_elements; ++i)
            {
                float value = valueDist(gen);
                input_h[i] = value;
                output_h[i] = value>=0?value:0.0f;
            }
            float *input_d;
            cudaMalloc(reinterpret_cast<void **>(&input_d), sizeof(float) * nb_elements);
            cudaMemcpy(input_d, input_h, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0->getImpl()->setRawPtr(input_d, nb_elements);

            // Run inference
            start = std::chrono::system_clock::now();
            op->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            float *computedOutput = new float[nb_elements]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * nb_elements, cudaMemcpyDeviceToHost);

            REQUIRE(approxEq<float>(*computedOutput, *output_h));

            delete[] computedOutput;
            delete[] input_h;
            cudaFree(input_d);
        }
        Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
        Log::info("Total time: {}μs\n", duration.count());
    }
}
