/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/FCImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/FCImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] FC(forward)", "[FC][GPU]") {
    SECTION("Static Input") {
        std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array2D<float, 5, 75>{
                {{1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,
                5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,
                9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
                13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15},
                {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,
                5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,
                9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
                13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15},
                {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,
                5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,
                9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
                13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15},
                {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,
                5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,
                9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
                13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15},
                {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,
                5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,
                9,  10, 11, 12, 13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
                13, 14, 15, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15}}});
        std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float, 5>{{1, 2, 3, 4, 5}});
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array2D<float, 2, 5>{
                {{23601, 23602, 23603, 23604, 23605}, {68601, 68602, 68603, 68604, 68605}}});
        myWeights->setBackend("cuda");
        myBias->setBackend("cuda");
        std::shared_ptr<Node> myFC = FC(75, 5, false, "myfc");
        auto op = std::static_pointer_cast<OperatorTensor>(myFC -> getOperator());
        op -> associateInput(1, myWeights);
        op -> associateInput(2, myBias);
        SECTION("2D input") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array2D<float, 2, 75>{
                    {{0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18,
                    19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
                    38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
                    57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74},
                    {75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
                    90,  91,  92,  93,  94,  95,  96,  97,  98,  99,  100, 101, 102, 103, 104,
                    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
                    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
                    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149}}});
            myInput->setBackend("cuda");
            op->associateInput(0, myInput);
            op -> setDataType(DataType::Float32);
            op -> setBackend("cuda");
            myFC->forward();

            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

            for(int i = 0; i < myOutput->size(); i++){
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
        SECTION("4D input") {
            std::shared_ptr<Tensor> myInput =
                    std::make_shared<Tensor>(Array4D<float, 2, 3, 5, 5>{{{{{0, 1, 2, 3, 4},
                                                                        {5, 6, 7, 8, 9},
                                                                        {10, 11, 12, 13, 14},
                                                                        {15, 16, 17, 18, 19},
                                                                        {20, 21, 22, 23, 24}},
                                                                        {{25, 26, 27, 28, 29},
                                                                        {30, 31, 32, 33, 34},
                                                                        {35, 36, 37, 38, 39},
                                                                        {40, 41, 42, 43, 44},
                                                                        {45, 46, 47, 48, 49}},
                                                                        {{50, 51, 52, 53, 54},
                                                                        {55, 56, 57, 58, 59},
                                                                        {60, 61, 62, 63, 64},
                                                                        {65, 66, 67, 68, 69},
                                                                        {70, 71, 72, 73, 74}}},
                                                                    {{{75, 76, 77, 78, 79},
                                                                        {80, 81, 82, 83, 84},
                                                                        {85, 86, 87, 88, 89},
                                                                        {90, 91, 92, 93, 94},
                                                                        {95, 96, 97, 98, 99}},
                                                                        {{100, 101, 102, 103, 104},
                                                                        {105, 106, 107, 108, 109},
                                                                        {110, 111, 112, 113, 114},
                                                                        {115, 116, 117, 118, 119},
                                                                        {120, 121, 122, 123, 124}},
                                                                        {{125, 126, 127, 128, 129},
                                                                        {130, 131, 132, 133, 134},
                                                                        {135, 136, 137, 138, 139},
                                                                        {140, 141, 142, 143, 144},
                                                                        {145, 146, 147, 148, 149}}}}});
            myInput->setBackend("cuda");
            op->associateInput(0, myInput);
            op -> setDataType(DataType::Float32);
            op -> setBackend("cuda");
            myFC->forward();

            float* computedOutput   = new float[myOutput->size()]();
            cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

            for(int i = 0; i < myOutput->size(); i++){
                const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
                REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
            }

            delete[] computedOutput;
        }
    }

    SECTION("Random Input"){
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(1, std::size_t(10));

        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            const std::size_t nbDims = 4;
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i) {
                dims.push_back(dimSizeDist(gen));
            }
            const std::size_t outChannels = dimSizeDist(gen);

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t inChannels = nb_elements / dims[0];

            const std::vector<std::size_t> dimsW{outChannels, inChannels};
            const std::size_t wieghtSize = outChannels * inChannels;

            // Create FC Operator CUDA
            std::shared_ptr<Node> myFCCUDA = FC(inChannels, outChannels, false, "myfccuda");
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myFCCUDA -> getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create FC Operator CPU
            std::shared_ptr<Node> myFCCPU = FC(inChannels, outChannels, false, "myfccpu");
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myFCCPU -> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");


            float* array0 = new float[nb_elements];
            float* weights = new float[wieghtSize];
            float* bias = new float[outChannels];

            for (std::size_t i = 0; i < nb_elements; ++i) {
                array0[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < wieghtSize; ++i) {
                weights[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < outChannels; ++i) {
                bias[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d, *weight_d, *bias_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // weight CUDA
            std::shared_ptr<Tensor> Tw_cuda = std::make_shared<Tensor>();
            Tw_cuda->setDataType(DataType::Float32);
            Tw_cuda->setBackend("cuda");
            Tw_cuda->resize(dimsW);
            op_cuda->associateInput(1, Tw_cuda);
            cudaMalloc(reinterpret_cast<void **>(&weight_d), sizeof(float) * wieghtSize);
            cudaMemcpy(weight_d, weights, sizeof(float) * wieghtSize, cudaMemcpyHostToDevice);
            Tw_cuda->getImpl()->setRawPtr(weight_d, wieghtSize);

            // weight CPU
            std::shared_ptr<Tensor> Tw_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,Tw_cpu);
            Tw_cpu->setDataType(DataType::Float32);
            Tw_cpu->setBackend("cpu");
            Tw_cpu->resize(dimsW);
            Tw_cpu -> getImpl() -> setRawPtr(weights, wieghtSize);

            // bias CUDA
            std::shared_ptr<Tensor> Tb_cuda = std::make_shared<Tensor>();
            Tb_cuda->setDataType(DataType::Float32);
            Tb_cuda->setBackend("cuda");
            Tb_cuda->resize({outChannels});
            op_cuda->associateInput(2, Tb_cuda);
            cudaMalloc(reinterpret_cast<void **>(&bias_d), sizeof(float) * outChannels);
            cudaMemcpy(bias_d, bias, sizeof(float) * outChannels, cudaMemcpyHostToDevice);
            Tb_cuda->getImpl()->setRawPtr(bias_d, outChannels);

            // bias CPU
            std::shared_ptr<Tensor> Tb_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(2,Tb_cpu);
            Tb_cpu->setDataType(DataType::Float32);
            Tb_cpu->setBackend("cpu");
            Tb_cpu->resize({outChannels});
            Tb_cpu -> getImpl() -> setRawPtr(bias, outChannels);

            // forward CUDA
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] array0;
            delete[] weights;
            delete[] bias;
            delete[] computed_cuda;
            cudaFree(array0_d);
            cudaFree(weight_d);
            cudaFree(bias_d);
        }
        fmt::print("Total time: {}μs\n", duration.count());
    }
}

TEST_CASE("[gpu/operator] FC(backward)", "[FC][GPU]") {
    SECTION("2D input") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array2D<float, 2, 3>{
            {
                {0.1, 0.2, 0.3},
                {0.4, 0.5, 0.6}
        }});
        myInput->setBackend("cuda");
        std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array2D<float, 2, 3>{
                {{0.1, 0.2, 0.3},
                {0.4, 0.5, 0.6}}});

        std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float, 2>{{0.1, 0.2}});
        myWeights->setBackend("cuda");
        myBias->setBackend("cuda");
        std::shared_ptr<Node> myFC = FC(3, 2, false, "myfc");
        auto op = std::static_pointer_cast<OperatorTensor>(myFC -> getOperator());

        op->associateInput(0, myInput);
        op -> associateInput(1, myWeights);
        op -> associateInput(2, myBias);
        op -> setDataType(DataType::Float32);
        op -> setBackend("cuda");
        myFC->forward();

        // Run and test backward operation
        std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array2D<float, 2, 2> {
            {
                {0.1, 0.2},
                {0.3, 0.4}
            }
        });
        myOutputGrad->setBackend("cuda");
        std::shared_ptr<Tensor> predictedOutput = op->getOutput(0);
        std::shared_ptr<Tensor> input = op->getInput(0);
        predictedOutput->setGrad(myOutputGrad);
        REQUIRE_NOTHROW(myFC->backward());

        std::shared_ptr<Tensor> expectedInputGrad = std::make_shared<Tensor>(Array2D<float,2,3> {
            {
                {0.09, 0.12, 0.15},
                {0.19, 0.26, 0.33}
            }
        });
        std::shared_ptr<Tensor> expectedBiasGrad = std::make_shared<Tensor>(Array1D<float,2> {
            {0.4, 0.6}
        });
        std::shared_ptr<Tensor> expectedWeightsGrad = std::make_shared<Tensor>(Array2D<float,2,3> {
            {
                {0.13, 0.17, 0.21},
                {0.18, 0.24, 0.3 }
            }
        });
        float *computedGradCuda = new float[expectedInputGrad->size()]();
        cudaMemcpy(computedGradCuda, input->grad()->getImpl()->rawPtr(), sizeof(float) * expectedInputGrad->size(), cudaMemcpyDeviceToHost);
        float *computedGradWCuda = new float[expectedWeightsGrad->size()]();
        cudaMemcpy(computedGradWCuda, op->getInput(1)->grad()->getImpl()->rawPtr(), sizeof(float) * expectedWeightsGrad->size(), cudaMemcpyDeviceToHost);
        float *computedGradBCuda = new float[expectedBiasGrad->size()]();
        cudaMemcpy(computedGradBCuda, op->getInput(2)->grad()->getImpl()->rawPtr(), sizeof(float) * expectedBiasGrad->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < expectedInputGrad->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedInputGrad->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedGradCuda[i] - targetOutput) < 1e-6);
        }
        for(int i = 0; i < expectedBiasGrad->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedBiasGrad->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedGradBCuda[i] - targetOutput) < 1e-6);
        }
        for(int i = 0; i < expectedWeightsGrad->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedWeightsGrad->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedGradWCuda[i] - targetOutput) < 1e-6);
        }




        delete[] computedGradCuda;
        delete[] computedGradWCuda;
        delete[] computedGradBCuda;
    }
}