/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm> // std::equal
#include <cmath>  // std::fabs
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>

#include "Test_cuda.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("CUDA test") {
    constexpr int N = 100;

    // Allocate host memory
    float* a   = new float[N]();
    float* b   = new float[N]();
    float* out = new float[N]();

    // Initialize host arrays
    for(int i = 0; i < N; i++){
        a[i] = 1.0f;
        b[i] = 2.0f;
    }

    // Allocate device memory
    float *d_a, *d_b, *d_out;
    cudaMalloc(reinterpret_cast<void**>(&d_a), sizeof(float) * N);
    cudaMalloc(reinterpret_cast<void**>(&d_b), sizeof(float) * N);
    cudaMalloc(reinterpret_cast<void**>(&d_out), sizeof(float) * N);

    // Transfer data from host to device memory
    cudaMemcpy(d_a, a, sizeof(float) * N, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(float) * N, cudaMemcpyHostToDevice);

    // Executing kernel
    vector_add(d_out, d_a, d_b, N);

    // Transfer data back to host memory
    cudaMemcpy(out, d_out, sizeof(float) * N, cudaMemcpyDeviceToHost);

    // Verification
    for(int i = 0; i < N; i++){
        REQUIRE(std::fabs(out[i] - a[i] - b[i]) < 1e-6);
    }

    // Deallocate device memory
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_out);

    // Deallocate host memory
    delete[] a;
    delete[] b;
    delete[] out;
}

TEST_CASE("Tensor creation", "[Connector]") {
    SECTION("from const array") {
        Tensor x;
        x = Array3D<int,2,2,2>{
        {
            {
                {1, 2},
                {3, 4}
            },
            {
                {5, 6},
                {7, 8}
            }
        }};
        x.setBackend("cuda");

        REQUIRE(x.nbDims() == 3);
        REQUIRE(x.dims()[0] == 2);
        REQUIRE(x.dims()[1] == 2);
        REQUIRE(x.dims()[2] == 2);
        REQUIRE(x.size() == 8);

        int val[8] = {0,0,0,0,0,0,0,0};
        cudaMemcpy(&val[0], x.getImpl()->rawPtr(), 8 * sizeof(int), cudaMemcpyDeviceToHost);
        REQUIRE(val[0] == 1);
        REQUIRE(val[7] == 8);
    }

    SECTION("from const array before backend") {
        Tensor x = Array3D<int,2,2,2>{
        {
            {
                {1, 2},
                {3, 4}
            },
            {
                {5, 6},
                {7, 8}
            }
        }};
        x.setBackend("cuda");

        REQUIRE(x.nbDims() == 3);
        REQUIRE(x.dims()[0] == 2);
        REQUIRE(x.dims()[1] == 2);
        REQUIRE(x.dims()[2] == 2);
        REQUIRE(x.size() == 8);

        std::array<int, 8> val;
        cudaMemcpy(&val[0], x.getImpl()->rawPtr(), 8 * sizeof(int), cudaMemcpyDeviceToHost);
        REQUIRE(val[0] == 1);
        REQUIRE(val[7] == 8);
    }
}

TEST_CASE("Tensor Descriptor Update") {
    Tensor x;
    x.setBackend("cuda");

    std::vector<std::size_t> shapeA = { 7, 6, 5, 4, 3 };
    x.resize(shapeA);

    cudnnTensorDescriptor_t desc = std::dynamic_pointer_cast<TensorImpl_cuda_>(x.getImpl())->getCudnnTensorDesc(x);

    cudnnDataType_t currentDataType;
    int currentNbDims;
    std::vector<int> currentDimA(shapeA.size());
    std::vector<int> currentStrideA(shapeA.size());

    REQUIRE_NOTHROW(cudnnGetTensorNdDescriptor(desc, shapeA.size(), &currentDataType, &currentNbDims, currentDimA.data(), currentStrideA.data()));

    REQUIRE(std::equal(currentDimA.begin(), currentDimA.end(), shapeA.begin(), [](int a, std::size_t b) {
                            return static_cast<std::size_t>(a) == b;
                        }
                      )
            );

    // Change the tensor shape and check tensor descriptor
    std::vector<std::size_t> shapeB = { 6, 5, 4 };
    x.resize(shapeB);

    std::vector<int> currentDimB(shapeB.size());
    std::vector<int> currentStrideB(shapeB.size());

    desc = std::dynamic_pointer_cast<TensorImpl_cuda_>(x.getImpl())->getCudnnTensorDesc(x);
    REQUIRE_NOTHROW(cudnnGetTensorNdDescriptor(desc, shapeB.size(), &currentDataType, &currentNbDims, currentDimB.data(), currentStrideB.data()));

    REQUIRE(std::equal(currentDimB.begin(), currentDimB.end(), shapeB.begin(), [](int a, std::size_t b) {
                            return static_cast<std::size_t>(a) == b;
                        }
                      )
            );
}
