/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <cuda_fp16.h> // half type
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/AvgPoolingImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AvgPoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/half.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] AvgPooling(forward)", "[AvgPooling][GPU]")
{
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float, 2, 2, 5, 5>{// NCHW
                                                                                          {
                                                                                              {{{0, 1, 2, 3, 4},
                                                                                                {5, 6, 7, 8, 9},
                                                                                                {10, 11, 12, 13, 14},
                                                                                                {15, 16, 17, 18, 19},
                                                                                                {20, 21, 22, 23, 24}},

                                                                                               {{25, 26, 27, 28, 29},
                                                                                                {30, 31, 32, 33, 34},
                                                                                                {35, 36, 37, 38, 39},
                                                                                                {40, 41, 42, 43, 44},
                                                                                                {45, 46, 47, 48, 49}}},
                                                                                              {{{100, 101, 102, 103, 104},
                                                                                                {105, 106, 107, 108, 109},
                                                                                                {110, 111, 112, 113, 114},
                                                                                                {115, 116, 117, 118, 119},
                                                                                                {120, 121, 122, 123, 124}},

                                                                                               {{125, 126, 127, 128, 129},
                                                                                                {130, 131, 132, 133, 134},
                                                                                                {135, 136, 137, 138, 139},
                                                                                                {140, 141, 142, 143, 144},
                                                                                                {145, 146, 147, 148, 149}}}}});
    SECTION("Stride")
    {
        std::shared_ptr<AvgPooling_Op<2>> op = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({2,2}), std::array<DimSize_t, 2>({2,2}));

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float, 2, 2, 2, 2>{
            {{{{3, 5},
               {13, 15}},
              {{28, 30},
               {38, 40}}},
             {{{103, 105},
               {113, 115}},
              {{128, 130},
               {138, 140}}}}});
        op->associateInput(0, myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        float *computedOutput = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for (int i = 0; i < myOutput->size(); i++)
        {
            const float targetOutput = *(static_cast<float *>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("Stride >= feature dim")
    {
        std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(Array4D<float, 1, 1, 3, 3>{// NCHW
                                        {
                                            {{{0.3745, 0.9507, 0.7320},
                                                {0.5987, 0.1560, 0.1560},
                                                {0.0581, 0.8662, 0.6011}}}}});
        std::shared_ptr<AvgPooling_Op<2>> op = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({3,3}), std::array<DimSize_t, 2>({3,3}));

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float, 1, 1, 1, 1>{
            {{{{(0.3745 + 0.9507 + 0.7320 + 0.5987 + 0.1560 + 0.1560 + 0.0581 + 0.8662 + 0.6011) / 9.0}}}}});
        op->associateInput(0, myInput2);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        float *computedOutput = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for (int i = 0; i < myOutput->size(); i++)
        {
            const float targetOutput = *(static_cast<float *>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("half")
    {
        std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(Array4D<half_float::half, 1, 1, 3, 3>{// NCHW
                                                                                                          {
                                                                                                              {{{half_float::half(0.3745), half_float::half(0.9507), half_float::half(0.7320)},
                                                                                                                {half_float::half(0.5987), half_float::half(0.1560), half_float::half(0.1560)},
                                                                                                                {half_float::half(0.0581), half_float::half(0.8662), half_float::half(0.6011)}}}}});
        myInput2->setBackend("cuda");

        std::shared_ptr<AvgPooling_Op<2>> op = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({3,3}), std::array<DimSize_t, 2>({3,3}));
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<half_float::half, 1, 1, 1, 1>{
            {{{{(half_float::half(0.3745) + half_float::half(0.9507) + half_float::half(0.7320) + half_float::half(0.5987) + half_float::half(0.1560) + half_float::half(0.1560) + half_float::half(0.0581) + half_float::half(0.8662) + half_float::half(0.6011)) / half_float::half(9.0)}}}}});
        op->associateInput(0, myInput2);
        op->setDataType(DataType::Float16);
        op->setBackend("cuda");
        op->forward();

        half_float::half *computedOutput = new half_float::half[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(half_float::half) * myOutput->size(), cudaMemcpyDeviceToHost);

        for (int i = 0; i < myOutput->size(); i++)
        {
            const half_float::half targetOutput = *(static_cast<half_float::half *>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        std::size_t kernel = 3;
        std::size_t stride = 3;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(kernel),
                                                               std::size_t(10));

        // To measure execution time of 'AveragePooling_Op::forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        std::size_t number_of_operation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // Create AveragePooling Operator CUDA
            std::shared_ptr<AvgPooling_Op<2>> op_cuda = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({kernel, kernel}), std::array<DimSize_t, 2>({stride, stride}));
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create AveragePooling Operator CPU
            std::shared_ptr<AvgPooling_Op<2>> op_cpu = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({kernel, kernel}), std::array<DimSize_t, 2>({stride, stride}));
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            // generate a random Tensor
            const std::size_t nbDims = 4;
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                dims.push_back(dimSizeDist(gen));
            }

            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            number_of_operation += nb_elements;

            // Fill input tensor
            float *array0 = new float[nb_elements];
            for (std::size_t i = 0; i < nb_elements; ++i)
            {
                array0[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // Run inference
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] computed_cuda;
            delete[] array0;
            cudaFree(array0_d);
        }
        Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
        Log::info("Total time: {}μs\n", duration.count());
    }
}

TEST_CASE("[gpu/operator] AvgPooling(backward)", "[AvgPooling][GPU]")
{
    // Run forward operation
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float, 1, 1, 4, 4> {// NCHW
        {
            {
                {
                    {1, 2, 3, 4},
                    {5, 6, 7, 8},
                    {9, 10, 11, 12},
                    {13, 14, 15, 16}
                }
            }
        }
    });
    myInput->setBackend("cuda");

    std::shared_ptr<AvgPooling_Op<2>> op = std::make_shared<AvgPooling_Op<2>>(std::array<DimSize_t, 2>({2, 2}), std::array<DimSize_t, 2>({2, 2}));
    op->associateInput(0, myInput);
    op->setDataType(DataType::Float32);
    op->setBackend("cuda");
    op->forward();

    // Run and test backward operation
    std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array4D<float, 1,1,2,2> {
        {
            {
                {
                    {1, 2},
                    {3, 4}
                }
            }
        }
    });
    myOutputGrad->setBackend("cuda");
    std::shared_ptr<Tensor> predictedOutput = op->getOutput(0);
    std::shared_ptr<Tensor> input = op->getInput(0);
    predictedOutput->setGrad(myOutputGrad);
    REQUIRE_NOTHROW(op->backward());

    std::shared_ptr<Tensor> expectedInputGrad = std::make_shared<Tensor>(Array4D<float, 1, 1, 4, 4>{
        {
            {
                {
                    {0.25, 0.25, 0.5, 0.5},
                    {0.25, 0.25, 0.5, 0.5},
                    {0.75, 0.75, 1, 1},
                    {0.75, 0.75, 1, 1}
                }
            }
        }
    });

    float *computedGradCuda = new float[expectedInputGrad->size()]();
    cudaMemcpy(computedGradCuda, input->grad()->getImpl()->rawPtr(), sizeof(float) * expectedInputGrad->size(), cudaMemcpyDeviceToHost);

    for(int i = 0; i < expectedInputGrad->size(); i++){
        const float targetOutput = *(static_cast<float*>(expectedInputGrad->getImpl()->rawPtr()) + i);
        REQUIRE(std::fabs(computedGradCuda[i] - targetOutput) < 1e-6);
    }

    delete[] computedGradCuda;
}