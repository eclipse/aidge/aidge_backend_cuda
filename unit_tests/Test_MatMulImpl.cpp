/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include <array>
#include <numeric> // std::accumulate
#include <random>  // std::random_device, std::mt19937, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu.hpp"
#include "aidge/backend/cuda.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] MatMul(forward)", "[MatMul][GPU]") {
    const std::uint16_t NBTRIALS = 10;
    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0, 1.0); // Random float distribution between 0 and 1
    std::uniform_int_distribution<std::size_t> distDims(2, 10);
    std::uniform_int_distribution<std::size_t> distNbMatrix(1, 5);

    // Create MatMul Operator
    std::shared_ptr<Node> myMatMulCPU = MatMul();
    auto op_cpu = std::static_pointer_cast<OperatorTensor>(myMatMulCPU -> getOperator());

    std::shared_ptr<Node> myMatMulCUDA = MatMul();
    auto op_cuda = std::static_pointer_cast<OperatorTensor>(myMatMulCUDA -> getOperator());

    // To measure execution time of 'MatMul_Op::forward()' member function call
    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> end;
    std::chrono::duration<double, std::micro> duration;

    SECTION("2-D Tensors") {
        std::size_t totalComputation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            const std::size_t dim2 = distDims(gen);
            totalComputation += dim0*dim1*dim2;

            // Create and populate the array with random float values
            float* bigArray1 = new float[dim0*dim1];
            for (int i = 0; i < dim0*dim1; ++i) {
                bigArray1[i] = dis(gen); // Generate random float value
            }
            float* bigArray2 = new float[dim1*dim2];
            for (int i = 0; i < dim1*dim2; ++i) {
                bigArray2[i] = dis(gen); // Generate random float value
            }

            float * bigArray1_d, *bigArray2_d;
            cudaMalloc(reinterpret_cast<void **>(&bigArray1_d), sizeof(float) * dim0*dim1);
            cudaMemcpy(bigArray1_d, bigArray1, sizeof(float) * dim0*dim1, cudaMemcpyHostToDevice);
            cudaMalloc(reinterpret_cast<void **>(&bigArray2_d), sizeof(float) * dim1*dim2);
            cudaMemcpy(bigArray2_d, bigArray2, sizeof(float) * dim1*dim2, cudaMemcpyHostToDevice);

            // Create Input0
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>(DataType::Float32);
            T1_cuda -> resize({dim0,dim1});
            T1_cuda->setBackend("cuda");
            op_cuda->associateInput(0, T1_cuda);
            T1_cuda->getImpl()->setRawPtr(bigArray1_d, dim0*dim1);

            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize({dim0,dim1});
            T1_cpu -> getImpl() -> setRawPtr(bigArray1, dim0*dim1);

            // Create Input1
            std::shared_ptr<Tensor> T2_cuda = std::make_shared<Tensor>(DataType::Float32);
            T2_cuda -> resize({dim1,dim2});
            T2_cuda->setBackend("cuda");
            op_cuda->associateInput(1, T2_cuda);
            T2_cuda->getImpl()->setRawPtr(bigArray2_d, dim1*dim2);

            std::shared_ptr<Tensor> T2_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T2_cpu);
            T2_cpu->setDataType(DataType::Float32);
            T2_cpu->setBackend("cpu");
            T2_cpu->resize({dim1,dim2});
            T2_cpu -> getImpl() -> setRawPtr(bigArray2, dim1*dim2);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            op_cuda->forwardDims();
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            // forward CPU
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");
            op_cpu->forwardDims();
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
            REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

            delete[] bigArray1;
            delete[] bigArray2;
            cudaFree(bigArray1_d);
            cudaFree(bigArray2_d);
        }
        Log::info("multiplications over time spent: {}\n", (totalComputation / duration.count()));
        Log::info("total time: {}μs\n", duration.count());
    }
    SECTION("3-D Tensors") {
        std::size_t totalComputation = 0;
        duration = std::chrono::duration<double, std::micro>::zero();
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dimNb = distNbMatrix(gen);
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            const std::size_t dim2 = distDims(gen);
            totalComputation += dim0*dim1*dim2*dimNb;

            // Create and populate the array with random float values
            float* bigArray1 = new float[dimNb*dim0*dim1];
            for (std::size_t i = 0; i < dimNb*dim0*dim1; ++i) {
                bigArray1[i] = dis(gen); // Generate random float value
            }
            float* bigArray2 = new float[dimNb*dim1*dim2];
            for (int i = 0; i < dimNb*dim1*dim2; ++i) {
                bigArray2[i] = dis(gen); // Generate random float value
            }

            float * bigArray1_d, *bigArray2_d;
            cudaMalloc(reinterpret_cast<void **>(&bigArray1_d), sizeof(float) * dimNb*dim0*dim1);
            cudaMemcpy(bigArray1_d, bigArray1, sizeof(float) * dimNb*dim0*dim1, cudaMemcpyHostToDevice);
            cudaMalloc(reinterpret_cast<void **>(&bigArray2_d), sizeof(float) * dimNb*dim1*dim2);
            cudaMemcpy(bigArray2_d, bigArray2, sizeof(float) * dimNb*dim1*dim2, cudaMemcpyHostToDevice);

            // Create Input0
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>(DataType::Float32);
            T1_cuda -> resize({dimNb,dim0,dim1});
            T1_cuda->setBackend("cuda");
            op_cuda->associateInput(0, T1_cuda);
            T1_cuda->getImpl()->setRawPtr(bigArray1_d, dimNb*dim0*dim1);

            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize({dimNb,dim0,dim1});
            T1_cpu -> getImpl() -> setRawPtr(bigArray1, dimNb*dim0*dim1);

            // Create Input1
            std::shared_ptr<Tensor> T2_cuda = std::make_shared<Tensor>(DataType::Float32);
            T2_cuda -> resize({dimNb,dim1,dim2});
            T2_cuda->setBackend("cuda");
            op_cuda->associateInput(1, T2_cuda);
            T2_cuda->getImpl()->setRawPtr(bigArray2_d, dimNb*dim1*dim2);

            std::shared_ptr<Tensor> T2_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T2_cpu);
            T2_cpu->setDataType(DataType::Float32);
            T2_cpu->setBackend("cpu");
            T2_cpu->resize({dimNb,dim1,dim2});
            T2_cpu -> getImpl() -> setRawPtr(bigArray2, dimNb*dim1*dim2);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            op_cuda->forwardDims();
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            // forward CPU
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");
            op_cpu->forwardDims();
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
            REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

            delete[] bigArray1;
            delete[] bigArray2;
            cudaFree(bigArray1_d);
            cudaFree(bigArray2_d);
        }
        Log::info("multiplications over time spent: {}\n", (totalComputation / duration.count()));
        Log::info("total time: {}μs\n", duration.count());
    }

    SECTION("4-D Tensors") {
        std::size_t totalComputation = 0;
        duration = std::chrono::duration<double, std::micro>::zero();
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dimNb1 = distNbMatrix(gen);
            const std::size_t dimNb2 = distNbMatrix(gen);
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            const std::size_t dim2 = distDims(gen);
            totalComputation += dim0*dim1*dim2*dimNb1*dimNb2;

            // Create and populate the array with random float values
            float* bigArray1 = new float[dimNb1*dimNb2*dim0*dim1];
            for (std::size_t i = 0; i < dimNb1*dimNb2*dim0*dim1; ++i) {
                bigArray1[i] = dis(gen); // Generate random float value
            }
            float* bigArray2 = new float[1*1*dim1*dim2];
            for (std::size_t i = 0; i < 1*1*dim1*dim2; ++i) {
                bigArray2[i] = dis(gen); // Generate random float value
            }

            float * bigArray1_d, *bigArray2_d;
            cudaMalloc(reinterpret_cast<void **>(&bigArray1_d), sizeof(float) * dimNb1*dimNb2*dim0*dim1);
            cudaMemcpy(bigArray1_d, bigArray1, sizeof(float) * dimNb1*dimNb2*dim0*dim1, cudaMemcpyHostToDevice);
            cudaMalloc(reinterpret_cast<void **>(&bigArray2_d), sizeof(float) * 1*1*dim1*dim2);
            cudaMemcpy(bigArray2_d, bigArray2, sizeof(float) * 1*1*dim1*dim2, cudaMemcpyHostToDevice);

            // Create Input0
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>(DataType::Float32);
            T1_cuda -> resize({dimNb1,dimNb2,dim0,dim1});
            T1_cuda->setBackend("cuda");
            op_cuda->associateInput(0, T1_cuda);
            T1_cuda->getImpl()->setRawPtr(bigArray1_d, dimNb1*dimNb2*dim0*dim1);

            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize({dimNb1,dimNb2,dim0,dim1});
            T1_cpu -> getImpl() -> setRawPtr(bigArray1, dimNb1*dimNb2*dim0*dim1);

            // Create Input1
            std::shared_ptr<Tensor> T2_cuda = std::make_shared<Tensor>(DataType::Float32);
            T2_cuda -> resize({1,1,dim1,dim2});
            T2_cuda->setBackend("cuda");
            op_cuda->associateInput(1, T2_cuda);
            T2_cuda->getImpl()->setRawPtr(bigArray2_d, 1*1*dim1*dim2);

            std::shared_ptr<Tensor> T2_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T2_cpu);
            T2_cpu->setDataType(DataType::Float32);
            T2_cpu->setBackend("cpu");
            T2_cpu->resize({1,1,dim1,dim2});
            T2_cpu -> getImpl() -> setRawPtr(bigArray2, 1*1*dim1*dim2);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            op_cuda->forwardDims();
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            // forward CPU
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");
            op_cpu->forwardDims();
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
            REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

            delete[] bigArray1;
            delete[] bigArray2;
            cudaFree(bigArray1_d);
            cudaFree(bigArray2_d);
        }
        Log::info("multiplications over time spent: {}\n", (totalComputation / duration.count()));
        Log::info("total time: {}μs\n", duration.count());
    }

    SECTION("+3-D / 1-D") {
        std::size_t totalComputation = 0;
        duration = std::chrono::duration<double, std::micro>::zero();
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dimNb = distNbMatrix(gen);
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            totalComputation += dim0*dim1*dimNb;

            // Create and populate the array with random float values
            float* bigArray1 = new float[dimNb*dim0*dim1];
            for (std::size_t i = 0; i < dimNb*dim0*dim1; ++i) {
                bigArray1[i] = dis(gen); // Generate random float value
            }
            float* bigArray2 = new float[dim1];
            for (int i = 0; i < dim1; ++i) {
                bigArray2[i] = dis(gen); // Generate random float value
            }

            float * bigArray1_d, *bigArray2_d;
            cudaMalloc(reinterpret_cast<void **>(&bigArray1_d), sizeof(float) * dimNb*dim0*dim1);
            cudaMemcpy(bigArray1_d, bigArray1, sizeof(float) * dimNb*dim0*dim1, cudaMemcpyHostToDevice);
            cudaMalloc(reinterpret_cast<void **>(&bigArray2_d), sizeof(float) * dim1);
            cudaMemcpy(bigArray2_d, bigArray2, sizeof(float) * dim1, cudaMemcpyHostToDevice);

            // Create Input0
            std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>(DataType::Float32);
            T1_cuda -> resize({dimNb,dim0,dim1});
            T1_cuda->setBackend("cuda");
            op_cuda->associateInput(0, T1_cuda);
            T1_cuda->getImpl()->setRawPtr(bigArray1_d, dimNb*dim0*dim1);

            std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T1_cpu);
            T1_cpu->setDataType(DataType::Float32);
            T1_cpu->setBackend("cpu");
            T1_cpu->resize({dimNb,dim0,dim1});
            T1_cpu -> getImpl() -> setRawPtr(bigArray1, dimNb*dim0*dim1);

            // Create Input1
            std::shared_ptr<Tensor> T2_cuda = std::make_shared<Tensor>(DataType::Float32);
            T2_cuda -> resize({dim1});
            T2_cuda->setBackend("cuda");
            op_cuda->associateInput(1, T2_cuda);
            T2_cuda->getImpl()->setRawPtr(bigArray2_d, dim1);

            std::shared_ptr<Tensor> T2_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,T2_cpu);
            T2_cpu->setDataType(DataType::Float32);
            T2_cpu->setBackend("cpu");
            T2_cpu->resize({dim1});
            T2_cpu -> getImpl() -> setRawPtr(bigArray2, dim1);

            // forward CUDA
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            op_cuda->forwardDims();
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            // forward CPU
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");
            op_cpu->forwardDims();
            op_cpu->forward();
            float *computedCPU = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());

            std::shared_ptr<Tensor> outputFallback;
            const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
            REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

            delete[] bigArray1;
            delete[] bigArray2;
            cudaFree(bigArray1_d);
            cudaFree(bigArray2_d);
        }
        Log::info("multiplications over time spent: {}\n", (totalComputation / duration.count()));
        Log::info("total time: {}μs\n", duration.count());
    }

}