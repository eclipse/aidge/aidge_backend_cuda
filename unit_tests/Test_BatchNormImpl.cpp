/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cmath>       // std::fabs
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/BatchNormImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/BatchNormImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] BatchNorm(forward)") {
    SECTION("Static Input") {
        std::shared_ptr<Node> myBatchNorm = BatchNorm<2>(3, 0.00001F, 0.1F, false, "mybatchnorm");
        auto op = std::static_pointer_cast<OperatorTensor>(myBatchNorm -> getOperator());
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        std::shared_ptr<Tensor> myWeights= std::make_shared<Tensor>(Array1D<float,3> {{0.9159252643585205, 0.18772238492965698, 0.4479946792125702}});
        std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float,3> {{0.33898890018463135, 0.3167555630207062, 0.7047033309936523}});
        std::shared_ptr<Tensor> myMean = std::make_shared<Tensor>(Array1D<float,3> {{0.45547693967819214, 0.22650663554668427, 0.6612948179244995}});
        std::shared_ptr<Tensor> myVar = std::make_shared<Tensor>(Array1D<float,3> {{0.02570258639752865, 0.026536229997873306, 0.15111008286476135}});
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,3,3,3> { //NCHW
            {
                    {
                        {{0.12943482, 0.6451229 , 0.24979436},
                        {0.7551012,  0.32007095, 0.89463896},
                        {0.7087448,  0.6266124,  0.4782957 }},

                        {{0.13796203, 0.9950787,  0.71555305},
                        {0.01347321, 0.4395316,  0.43097174},
                        {0.6056306 , 0.9561122 , 0.5783939 }},

                        {{0.7174486 , 0.503465 ,  0.23695093},
                        {0.5145477,  0.39576462, 0.02779444},
                        {0.60789394 ,0.14119725 ,0.20753163}}
                    },


                        {{{0.74452287, 0.5354875 , 0.8148496 },
                        {0.73356223, 0.4304034 , 0.11783765},
                        {0.8966221,  0.41049036, 0.95982736}},

                        {{0.03161403, 0.71250844, 0.14337301},
                        {0.5338889 , 0.13484782, 0.8055851 },
                        {0.71784616 ,0.8349626 , 0.10107189}},

                        {{0.85701346, 0.58286697, 0.9836816 },
                        {0.36061534, 0.03660944, 0.7375317 },
                        {0.6977233,  0.51965624, 0.29440993}}
                    }
                }
        });

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,3,3,3> {
            {
                {
                    {{-1.5233592,   1.4222438,  -0.83586717},
                    { 2.0504384,  -0.43444824,  2.847476  },
                    { 1.7856512,   1.3165123,   0.46932936}},

                    {{ 0.21473758 , 1.2022772,   0.8802177 },
                    { 0.07130594 , 0.5621954,   0.55233306},
                    { 0.7535689 ,  1.1573814,   0.72218764}},

                    {{ 0.7694162 ,  0.52281666,  0.2156798 },
                    { 0.5355886  , 0.3987003,  -0.02535689},
                    { 0.6431629 ,  0.10533108 , 0.18177633}}},


                    {{{ 1.990015,    0.7960079,   2.3917203 },
                    { 1.9274082,   0.19576907, -1.5896021 },
                    { 2.8588037 ,  0.08202624 , 3.2198315 }},

                    {{ 0.09220716,  0.8767097,   0.22097193},
                    { 0.6709106 ,  0.2111495,   0.9839494 },
                    { 0.8828597 ,  1.0177971 ,  0.17223406}},

                    {{ 0.9302539 ,  0.6143213 ,  1.0762292 },
                    { 0.35819346, -0.01519828,  0.79256046},
                    { 0.7466844 ,  0.5414758 ,  0.28189686}}
                }
            }
        });
        myInput->setBackend("cuda");
        myWeights->setBackend("cuda");
        myBias->setBackend("cuda");
        myMean->setBackend("cuda");
        myVar->setBackend("cuda");

        op->associateInput(0,myInput);
        op->associateInput(1,myWeights);
        op->associateInput(2,myBias);
        op->associateInput(3,myMean);
        op->associateInput(4,myVar);
        op->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedOutput[i] - targetOutput) < 1e-5);
        }

        delete[] computedOutput;
    }

    SECTION("Random Input") {
        constexpr std::uint16_t NBTRIALS = 10;
        constexpr float epsilon = 0.00001F;
        constexpr float momentum = 0.1F;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(
            0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(1),
                                                               std::size_t(10));
        // To measure execution time of 'forward()'
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::time_point<std::chrono::system_clock> end;
        std::chrono::duration<double, std::micro> duration{};
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {
            // generate a random Tensor
            const std::size_t nbDims = 4;
            std::vector<std::size_t> dims;
            for (std::size_t i = 0; i < nbDims; ++i)
            {
                dims.push_back(dimSizeDist(gen));
            }
            const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
            const std::size_t nbChannels = dims[1];


            // Create BatchNorm Operator Cuda
            std::shared_ptr<Node> myBatchNormCuda = BatchNorm<2>(nbChannels, epsilon, momentum, false, "mybatchnormcuda");
            auto op_cuda = std::static_pointer_cast<BatchNorm_Op<2>>(myBatchNormCuda -> getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");

            // Create BatchNorm Operator CPU
            std::shared_ptr<Node> myBatchNormCpu = BatchNorm<2>(nbChannels, epsilon, momentum, false, "mybatchnormcpu");
            auto op_cpu = std::static_pointer_cast<BatchNorm_Op<2>>(myBatchNormCpu -> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            float* array0 = new float[nb_elements];
            float* weights = new float[nbChannels];
            float* bias = new float[nbChannels];
            float* mean = new float[nbChannels];
            float* var = new float[nbChannels];


            for (std::size_t i = 0; i < nb_elements; ++i) {
                array0[i] = valueDist(gen);
            }
            for (std::size_t i = 0; i < nbChannels; ++i) {
                weights[i] = valueDist(gen);
                bias[i] = valueDist(gen);
                mean[i] = valueDist(gen);
                var[i] = valueDist(gen);
            }

            // input0 CUDA
            float* array0_d, *weight_d, *bias_d, *mean_d, *var_d;
            std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
            T0_cuda->setDataType(DataType::Float32);
            T0_cuda->setBackend("cuda");
            T0_cuda->resize(dims);
            op_cuda->associateInput(0, T0_cuda);
            cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
            cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
            T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

            // input0 CPU
            std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(0,T0_cpu);
            T0_cpu->setDataType(DataType::Float32);
            T0_cpu->setBackend("cpu");
            T0_cpu->resize(dims);
            T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

            // weight CUDA
            std::shared_ptr<Tensor> Tw_cuda = std::make_shared<Tensor>();
            Tw_cuda->setDataType(DataType::Float32);
            Tw_cuda->setBackend("cuda");
            Tw_cuda->resize({nbChannels});
            op_cuda->associateInput(1, Tw_cuda);
            cudaMalloc(reinterpret_cast<void **>(&weight_d), sizeof(float) * nbChannels);
            cudaMemcpy(weight_d, weights, sizeof(float) * nbChannels, cudaMemcpyHostToDevice);
            Tw_cuda->getImpl()->setRawPtr(weight_d, nbChannels);

            // weight CPU
            std::shared_ptr<Tensor> Tw_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(1,Tw_cpu);
            Tw_cpu->setDataType(DataType::Float32);
            Tw_cpu->setBackend("cpu");
            Tw_cpu->resize({nbChannels});
            Tw_cpu -> getImpl() -> setRawPtr(weights, nbChannels);

            // bias CUDA
            std::shared_ptr<Tensor> Tb_cuda = std::make_shared<Tensor>();
            Tb_cuda->setDataType(DataType::Float32);
            Tb_cuda->setBackend("cuda");
            Tb_cuda->resize({nbChannels});
            op_cuda->associateInput(2, Tb_cuda);
            cudaMalloc(reinterpret_cast<void **>(&bias_d), sizeof(float) * nbChannels);
            cudaMemcpy(bias_d, bias, sizeof(float) * nbChannels, cudaMemcpyHostToDevice);
            Tb_cuda->getImpl()->setRawPtr(bias_d, nbChannels);

            // bias CPU
            std::shared_ptr<Tensor> Tb_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(2,Tb_cpu);
            Tb_cpu->setDataType(DataType::Float32);
            Tb_cpu->setBackend("cpu");
            Tb_cpu->resize({nbChannels});
            Tb_cpu -> getImpl() -> setRawPtr(bias, nbChannels);

            // mean CUDA
            std::shared_ptr<Tensor> Tm_cuda = std::make_shared<Tensor>();
            Tm_cuda->setDataType(DataType::Float32);
            Tm_cuda->setBackend("cuda");
            Tm_cuda->resize({nbChannels});
            op_cuda->associateInput(3, Tm_cuda);
            cudaMalloc(reinterpret_cast<void **>(&mean_d), sizeof(float) * nbChannels);
            cudaMemcpy(mean_d, mean, sizeof(float) * nbChannels, cudaMemcpyHostToDevice);
            Tm_cuda->getImpl()->setRawPtr(mean_d, nbChannels);

            // mean CPU
            std::shared_ptr<Tensor> Tm_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(3,Tm_cpu);
            Tm_cpu->setDataType(DataType::Float32);
            Tm_cpu->setBackend("cpu");
            Tm_cpu->resize({nbChannels});
            Tm_cpu -> getImpl() -> setRawPtr(mean, nbChannels);

            // var CUDA
            std::shared_ptr<Tensor> Tv_cuda = std::make_shared<Tensor>();
            Tv_cuda->setDataType(DataType::Float32);
            Tv_cuda->setBackend("cuda");
            Tv_cuda->resize({nbChannels});
            op_cuda->associateInput(4, Tv_cuda);
            cudaMalloc(reinterpret_cast<void **>(&var_d), sizeof(float) * nbChannels);
            cudaMemcpy(var_d, var, sizeof(float) * nbChannels, cudaMemcpyHostToDevice);
            Tv_cuda->getImpl()->setRawPtr(var_d, nbChannels);

            // var CPU
            std::shared_ptr<Tensor> Tv_cpu = std::make_shared<Tensor>();
            op_cpu->associateInput(4,Tv_cpu);
            Tv_cpu->setDataType(DataType::Float32);
            Tv_cpu->setBackend("cpu");
            Tv_cpu->resize({nbChannels});
            Tv_cpu -> getImpl() -> setRawPtr(var, nbChannels);

            // forward CUDA
            start = std::chrono::system_clock::now();
            op_cuda->forward();
            end = std::chrono::system_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            const std::size_t outSize =  op_cuda->getOutput(0)->size();
            float *computed_cuda = new float[outSize]();
            cudaMemcpy(computed_cuda, op_cuda->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * outSize, cudaMemcpyDeviceToHost);

            // forward CPU
            op_cpu->forward();
            float *computed_cpu = static_cast<float*>(op_cpu->getOutput(0)->getImpl()->rawPtr());
            REQUIRE(approxEq<float>(*computed_cuda, *computed_cpu));

            delete[] array0;
            delete[] weights;
            delete[] bias;
            delete[] mean;
            delete[] var;
            delete[] computed_cuda;
            cudaFree(array0_d);
            cudaFree(weight_d);
            cudaFree(bias_d);
            cudaFree(mean_d);
            cudaFree(var_d);
        }
        fmt::print("Total time: {} μs\n", duration.count());

    }
}
TEST_CASE("[gpu/operator] BatchNorm(backward)") {
    SECTION("Static Input") {
        std::shared_ptr<Node> myBatchNorm = BatchNorm<2>(3, 0.00001F, 0.1F, "mybatchnorm");
        auto op = std::static_pointer_cast<BatchNorm_Op<2>>(myBatchNorm -> getOperator());
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        // Forward
        std::shared_ptr<Tensor> myWeights= std::make_shared<Tensor>(Array1D<float,3> {{-1.58390772, -0.48463920,  1.30413496}});
        std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float,3> {{0.06150287, -0.03140282, -0.49673468}});
        std::shared_ptr<Tensor> myMean = std::make_shared<Tensor>(Array1D<float,3> {{0.68328333, -0.47286209,  1.11688483}});
        std::shared_ptr<Tensor> myVar = std::make_shared<Tensor>(Array1D<float,3> {{0.84838068, 1.05930495, 0.53670371}});
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,3,2,2> { //NCHW
            {
                    {
                        {
                            {1.46650600,  1.24083233},
                            {-0.33106008, -0.15137172}
                        },
                        {
                            { 0.06625678, -1.83266091},
                            { 0.53444749, -0.05167147}
                        },
                        {
                            { 0.41069385, -0.70850474},
                            { 0.23363227,  0.06111236}
                        }
                    },
                    {
                        {
                            { 0.16707586,  1.07217050},
                            { 1.18544745,  0.03441877}
                        },
                        {
                            { 0.88106865,  0.33312374},
                            { 0.87147945,  1.46628737}
                        },
                       {
                            { 0.23930393, -0.94172227},
                            { 1.48735642,  0.46449399}
                        }
                    }
            }
        });

        myInput->setBackend("cuda");
        myWeights->setBackend("cuda");
        myBias->setBackend("cuda");
        myMean->setBackend("cuda");
        myVar->setBackend("cuda");

        op->associateInput(0,myInput);
        op->associateInput(1,myWeights);
        op->associateInput(2,myBias);
        op->associateInput(3,myMean);
        op->associateInput(4,myVar);
        op->forward();

        // Backward
        std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array4D<float,2,3,2,2> {
            {
                {
                    {
                        { 1.34347093,  0.90813798},
                        { 0.39607167,  1.20428133}
                    },
                    {
                        { 0.16845724,  0.48487359},
                        { 0.40748054, -0.21790814}
                    },
                    {
                        {-1.83932650, -0.42746788},
                        { 0.97129798,  2.04073548}
                    }
                },
                {
                    {
                        {-0.95714629,  0.18446854},
                        { 1.14551663, -1.38118088}
                    },
                    {
                        {-0.44466951,  2.73914146},
                        { 0.57898718,  2.23699141}
                    },
                    {
                        { 0.25004527, -0.18481003},
                        {-0.72439206,  0.87744337}
                    }

                }
            }
        });

        myOutputGrad->setBackend("cuda");
        std::shared_ptr<Tensor> predictedOutput = op->getOutput(0);
        std::shared_ptr<Tensor> input = op->getInput(0);
        std::shared_ptr<Tensor> weights = op->getInput(1);
        std::shared_ptr<Tensor> bias = op->getInput(2);
        predictedOutput->setGrad(myOutputGrad);
        REQUIRE_NOTHROW(op->backward());

        std::shared_ptr<Tensor> expectedInputGrad = std::make_shared<Tensor>(Array4D<float, 2, 3, 2, 2>{
            {
                {
                    {
                        {-0.92418045, -0.26092845},
                        {-1.53920066, -3.14756274}},

                        {{ 0.26948565, -0.18548687},
                        { 0.21506749,  0.45458069}},

                        {{-3.57358932, -1.30609703},
                        { 1.61337423,  3.55250096}}},


                        {{{ 2.41264391,  1.16695499},
                        {-0.90373814,  3.19601130}},

                        {{ 0.71554798, -1.04076481},
                        { 0.17618656, -0.60461664}},

                        {{ 0.26926503, -0.92978811},
                        {-1.13964832,  1.51398242}
                    }
                }
            }
        });

        float *computedGradCuda = new float[expectedInputGrad->size()]();
        cudaMemcpy(computedGradCuda, input->grad()->getImpl()->rawPtr(), sizeof(float) * expectedInputGrad->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < expectedInputGrad->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedInputGrad->getImpl()->rawPtr()) + i);
            REQUIRE(std::fabs(computedGradCuda[i] - targetOutput) < 1e-6);
        }

        delete[] computedGradCuda;
    }
}