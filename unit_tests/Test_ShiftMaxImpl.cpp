/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/


#include <cmath>  // std::fabs
#include <cstddef>  // std::size_t
#include <memory>

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/ShiftMaxImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ShiftMax.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] ShiftMax(forward)", "[ShiftMax][GPU]") {
    SECTION("4D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        {0.96, 0.48, 0.54, 0.49, 0.59, 0.93, 0.00, 0.00, 0.61, 0.61},
                        {0.85, 0.06, 0.11, 0.87, 0.55, 0.12, 0.80, 0.48, 0.41, 0.16}
                    },
                    {
                        {0.24, 0.46, 0.97, 0.19, 0.65, 0.12, 0.44, 1.00, 0.37, 0.09},
                        {0.44, 0.64, 0.21, 0.58, 0.05, 0.24, 0.56, 0.07, 0.49, 0.79}
                    }
                },
                {
                    {
                        {0.00, 0.13, 0.55, 0.42, 0.49, 0.28, 0.52, 0.55, 0.34, 0.85},
                        {0.98, 0.32, 0.09, 0.05, 0.37, 0.47, 0.63, 0.13, 0.70, 0.02}
                    },
                    {
                        {0.69, 0.13, 0.74, 0.61, 0.25, 0.87, 0.46, 0.40, 0.81, 0.06},
                        {0.89, 0.32, 0.61, 0.24, 0.70, 0.23, 0.09, 0.03, 0.14, 0.80}
                    }
                }
            }
        });
        //expected output of shiftmax forward operator
        std::shared_ptr<Tensor> output_shiftmax = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        { 0.111084f, 0.111084f, 0.111084f, 0.111084f, 0.111084f, 0.111084f, 0.055542f, 0.055542f, 0.111084f, 0.111084f },
                        { 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f }
                    },
                    {
                        { 0.0624695f, 0.124969f, 0.124969f, 0.0624695f, 0.124969f, 0.0624695f, 0.124969f, 0.124969f, 0.124969f, 0.0624695f },
                        { 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f }
                    }
                },
                {
                    {
                        { 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f },
                        { 0.124969f, 0.124969f, 0.0624695f, 0.0624695f, 0.124969f, 0.124969f, 0.124969f, 0.0624695f, 0.124969f, 0.0624695f }
                    },
                    {
                        { 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f },
                        { 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f, 0.0999756f }
                    }
                }
            }
        });
        //expected output of softmax forward operator (computed with PyTorch)
        std::shared_ptr<Tensor> output_softmax = std::make_shared<Tensor>(Array4D<float, 2, 2, 2, 10> {
            {
                {
                    {
                        { 0.1484f, 0.0918f, 0.0975f, 0.0928f, 0.1025f, 0.1440f, 0.0568f, 0.0568f, 0.1046f, 0.1046f },
                        { 0.1436f, 0.0652f, 0.0685f, 0.1465f, 0.1064f, 0.0692f, 0.1366f, 0.0992f, 0.0925f, 0.0721f }
                    },
                    {
                        { 0.0768f, 0.0957f, 0.1593f, 0.0730f, 0.1157f, 0.0681f, 0.0938f, 0.1642f, 0.0874f, 0.0661f },
                        { 0.1005f, 0.1227f, 0.0798f, 0.1156f, 0.0680f, 0.0823f, 0.1133f, 0.0694f, 0.1056f, 0.1426f }
                    }
                },
                {
                    {
                        { 0.0645f, 0.0734f, 0.1118f, 0.0981f, 0.1052f, 0.0853f, 0.1085f, 0.1118f, 0.0906f, 0.1509f },
                        { 0.1743f, 0.0901f, 0.0716f, 0.0688f, 0.0947f, 0.1047f, 0.1228f, 0.0745f, 0.1317f, 0.0667f }
                    },
                    {
                        { 0.1164f, 0.0665f, 0.1224f, 0.1075f, 0.0750f, 0.1394f, 0.0925f, 0.0871f, 0.1313f, 0.0620f },
                        { 0.1551f, 0.0877f, 0.1172f, 0.0810f, 0.1283f, 0.0802f, 0.0697f, 0.0656f, 0.0733f, 0.1418f }
                    }
                }
            }
        });

        std::shared_ptr<Node> myShiftMax = ShiftMax();
        auto op = std::static_pointer_cast<OperatorTensor>(myShiftMax -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        float* computedOutput   = new float[output_shiftmax->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * output_shiftmax->size(), cudaMemcpyDeviceToHost);

        //test if forward result are as expected
        for(int i = 0; i < output_shiftmax->size(); i++){
            const float targetOutput = *(static_cast<float*>(output_shiftmax->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        //measure difference between softmax and shiftmax
        float sum = 0.0;
        for(int i = 0; i < output_softmax->size(); i++){
            const float targetOutput = *(static_cast<float*>(output_softmax->getImpl()->rawPtr()) + i);
            sum += fabs(computedOutput[i] - targetOutput);
        }
        sum = sum / output_softmax->size();
        REQUIRE(sum < 4e-2);

        delete[] computedOutput;
    }

}

TEST_CASE("[gpu/operator] ShiftMax(backward)", "[ShiftMax][GPU]")

{

    std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<float,1,1,1,8> { //NCHW
            {
                    {
                        {
                            {1.46650600,  1.24083233, -0.33106008, -0.15137172, 0.06625678, -1.8326609, 0.53444749, -0.05167147},
                        },
                    },
            }
        });

    input0->setBackend("cuda");

    std::shared_ptr<Node> myShiftMax = ShiftMax();
    auto op = std::static_pointer_cast<OperatorTensor>(myShiftMax->getOperator());
    op->associateInput(0, input0);
    op->setDataType(DataType::Float32);
    op->setBackend("cuda");
    myShiftMax->forward();

    std::shared_ptr<Tensor> myOutputGrad = std::make_shared<Tensor>(Array4D<float,1,1,1,8> {
            {
                {
                    {
                        { 1.34347093,  0.90813798, 0.39607167,  1.20428133, 0.16845724,  0.48487359, 0.40748054, -0.21790814},
                    },
                },
            }
        });


    myOutputGrad->setBackend("cuda");
    std::shared_ptr<Tensor> predictedOutput = op->getOutput(0);
    std::shared_ptr<Tensor> input = op->getInput(0);
    predictedOutput->setGrad(myOutputGrad);
    REQUIRE_NOTHROW(myShiftMax->backward());

    //expected output of shiftmax backward operator
    std::shared_ptr<Tensor> expectedInputGradShiftMax = std::make_shared<Tensor>(Array4D<float,1,1,1,8> {
            {
                {
                    {
                        { 0.159378, 0.0249331, -0.0250217, 0.0262418, -0.0514701, -0.00459638, -0.0551896, -0.0739511},
                    },
                },
            }
        });

    //expected output of softmax backward operator (computed with PyTorch)
    std::shared_ptr<Tensor> expectedInputGradSoftmax = std::make_shared<Tensor>(Array4D<float,1,1,1,8> {
            {
                {
                    {
                        { 0.1672,  0.0198, -0.0236,  0.0241, -0.0535, -0.0042, -0.0547, -0.0752},
                    },
                },
            }
        });


    float *computedGradCuda = new float[myOutputGrad->size()]();

    cudaMemcpy(computedGradCuda, input->grad()->getImpl()->rawPtr(), sizeof(float) * myOutputGrad->size(), cudaMemcpyDeviceToHost);

    //test if backward result are as expected
    for(int i = 0; i < expectedInputGradShiftMax->size(); i++){
        const float targetOutput = *(static_cast<float*>(expectedInputGradShiftMax->getImpl()->rawPtr()) + i);
        REQUIRE(fabs(computedGradCuda[i] - targetOutput) < 1e-6);
    }

    //measure difference between softmax and shiftmax
    float sum = 0.0;
        for(int i = 0; i < expectedInputGradSoftmax->size(); i++){
            const float targetOutput = *(static_cast<float*>(expectedInputGradSoftmax->getImpl()->rawPtr()) + i);
            sum += fabs(computedGradCuda[i] - targetOutput);
        }
        sum = sum / expectedInputGradSoftmax->size();
        REQUIRE(sum < 4e-3);

    delete[] computedGradCuda;
}
