/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AndImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] And(forward)", "[And][GPU]") {
    SECTION("Same size inputs") {
        std::shared_ptr<Tensor> input_1 = std::make_shared<Tensor>(Array4D<float,3,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 15},{31, 11},{22, 49}},   //
                    {{41, 10},{24, 51},{27, 52}},   //
                    {{26, 53},{27, 54},{28, 55}}    //
                },                                  //
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{32, 59},{33, 60},{34, 61}},   //
                    {{35, 62},{36, 63},{37, 64}}    //
                },                                  //
                {                                   //
                    {{38, 65},{39, 66},{40, 67}},   //
                    {{41, 68},{42, 69},{43, 70}},   //
                    {{44, 71},{45, 72},{46, 73}}    //
                }                                   //
            }                                       //
        });                                         //
        input_1->setBackend("cuda");
        std::shared_ptr<Tensor> input_2 = std::make_shared<Tensor>(Array4D<float,3,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{17, 53},{27, 26},{14, 33}}    //
                },                                  //
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{72, 44},{33, 20},{27, 55}},   //
                    {{35, 24},{25, 63},{28, 64}}    //
                },                                  //
                {                                   //
                    {{32, 65},{39, 66},{40, 70}},   //
                    {{41, 53},{42, 60},{34, 70}},   //
                    {{44, 71},{30, 12},{46, 73}}    //
                }                                   //
            }                                       //
        });                                         //
        input_2->setBackend("cuda");
        const Tensor myOutput = Tensor(Array4D<float,3,3,3,2> {
            {
                {
                    {{1, 0},{0, 0},{1, 1}},
                    {{0, 0},{1, 1},{0, 1}},
                    {{0, 1},{1, 0},{0, 0}}
                },
                {
                    {{1, 1},{1, 1},{1, 1}},
                    {{0, 0},{1, 0},{0, 0}},
                    {{1, 0},{0, 1},{0, 1}}
                },
                {
                    {{0, 1},{1, 1},{1, 0}},
                    {{1, 0},{1, 0},{0, 1}},
                    {{1, 1},{0, 0},{1, 1}}
                }
            }
        });

        std::shared_ptr<And_Op> op = std::make_shared<And_Op>();
        op->associateInput(0, input_1);
        op->associateInput(1, input_2);
        op->setBackend("cuda");
        op->setDataType(DataType::Float32);
        op->forward();


        std::shared_ptr<Tensor> outputFallback;
        const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
        REQUIRE(approxEq<float>(cudaOutput, myOutput));
    }

    SECTION("Broadcasting") {
        std::shared_ptr<Tensor> input_1 = std::make_shared<Tensor>(Array4D<float,1,3,3,2> {
        {                                       //
            {                                   //
                {{10, 20},{22, 23},{20, 20}},   //
                {{10, 15},{10, 29},{20, 20}},   //
                {{26, 25},{33, 20},{10, 20}}    //
            }                                   //
        }                                       //
        });                                     //
        input_1->setBackend("cuda");
        std::shared_ptr<Tensor> input_2 = std::make_shared<Tensor>(Array1D<float,2> {{10, 20}});
        const Tensor myOutput = Tensor(Array4D<float,1,3,3,2> {
            {                                   //
                {                               //
                    {{ 1, 1},{ 0, 0},{ 0, 1}},  //
                    {{ 1, 0},{ 1, 0},{ 0, 1}},  //
                    {{ 0, 0},{ 0, 1},{ 1, 1}}   //
                }                               //
            }                                   //
        });                                     //
        input_2->setBackend("cuda");
        std::shared_ptr<And_Op> op = std::make_shared<And_Op>();
        op->associateInput(0, input_1);
        op->associateInput(1, input_2);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        std::shared_ptr<Tensor> outputFallback;
        const auto& cudaOutput = op->getOutput(0)->refCastFrom(outputFallback, myOutput);
        REQUIRE(approxEq<float>(cudaOutput, myOutput));
    }
}