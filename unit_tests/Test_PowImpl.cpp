/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cstddef>     // std::size_t
#include <cstdint>     // std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>
#include <cuda.h>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/PowImpl.hpp"
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/PowImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Pow.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

TEST_CASE("[gpu/operator] Pow", "[Pow][GPU]") {
    constexpr std::uint16_t NBTRIALS = 10;
    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> valueDist(0.1f, 1.1f); // Random float distribution between 0 and 1
    std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(2), std::size_t(10));
    std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(5));
    std::uniform_int_distribution<int> boolDist(0,1);

    // To measure execution time of 'MatPow_Op::forward()' member function call
    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> end;
    std::chrono::duration<double, std::micro> duration{};

    SECTION("PowImpl::forward()") {
        SECTION("Scalar / Scalar") {

        }
        SECTION("Scalar / +1-D Tensor") {

        }
        SECTION("+1-D Tensor / +1-D Tensor - same dimensions") {

            // Create Pow Operator
            std::shared_ptr<Node> myPowCUDA = Pow();
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myPowCUDA-> getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            std::shared_ptr<Node> myPowCPU = Pow();
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myPowCPU-> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            std::size_t number_of_operation = 0;

            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                // generate 2 random Tensors
                const std::size_t nbDims = nbDimsDist(gen);
                std::vector<std::size_t> dims;
                for (std::size_t i = 0; i < nbDims; ++i) {
                    dims.push_back(dimSizeDist(gen));
                }
                const std::size_t nb_elements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
                number_of_operation += nb_elements;

                // without broadcasting
                float* array0 = new float[nb_elements];
                float* array1 = new float[nb_elements];

                for (std::size_t i = 0; i < nb_elements; ++i) {
                    array0[i] = valueDist(gen);
                    array1[i] = valueDist(gen);
                }

                // input0 CUDA
                float* array0_d, *array1_d;
                std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
                T0_cuda->setDataType(DataType::Float32);
                T0_cuda->setBackend("cuda");
                T0_cuda->resize(dims);
                op_cuda->associateInput(0, T0_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * nb_elements);
                cudaMemcpy(array0_d, array0, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
                T0_cuda->getImpl()->setRawPtr(array0_d, nb_elements);

                // input0 CPU
                std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
                T0_cpu->setDataType(DataType::Float32);
                T0_cpu->setBackend("cpu");
                T0_cpu->resize(dims);
                op_cpu->associateInput(0,T0_cpu);
                T0_cpu -> getImpl() -> setRawPtr(array0, nb_elements);

                // input1 CUDA
                std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>();
                T1_cuda->setDataType(DataType::Float32);
                T1_cuda->setBackend("cuda");
                T1_cuda->resize(dims);
                op_cuda->associateInput(1, T1_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array1_d), sizeof(float) * nb_elements);
                cudaMemcpy(array1_d, array1, sizeof(float) * nb_elements, cudaMemcpyHostToDevice);
                T1_cuda->getImpl()->setRawPtr(array1_d, nb_elements);

                // input1
                std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
                T1_cpu->setDataType(DataType::Float32);
                T1_cpu->setBackend("cpu");
                T1_cpu->resize(dims);
                op_cpu -> associateInput(1,T1_cpu);
                T1_cpu -> getImpl() -> setRawPtr(array1, nb_elements);

                op_cuda->forwardDims();
                start = std::chrono::system_clock::now();
                myPowCUDA->forward();
                end = std::chrono::system_clock::now();
                duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

                // REQUIRE(false);
                op_cpu->forwardDims();
                myPowCPU->forward();

                std::shared_ptr<Tensor> outputFallback;
                const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
                REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

                delete[] array0;
                delete[] array1;
                cudaFree(array0_d);
                cudaFree(array1_d);
            }
            Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
            Log::info("Total time: {}μs\n", duration.count());
        }

        SECTION("+1-D Tensor / +1-D Tensor - broadcasting") {
            // Create Pow Operator
            std::shared_ptr<Node> myPowCUDA = Pow();
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myPowCUDA-> getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            std::shared_ptr<Node> myPowCPU = Pow();
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myPowCPU-> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            std::size_t number_of_operation = 0;

            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                // generate 2 random Tensors
                // handle dimensions, replace some dimensions with '1' to get broadcasting
                constexpr std::size_t nbDims = 4;
                std::vector<std::size_t> dims;
                for (std::size_t i = 0; i < nbDims; ++i) {
                    dims.push_back(dimSizeDist(gen));
                }
                std::vector<std::size_t> dims0 = dims;
                std::vector<std::size_t> dims1 = dims;
                std::vector<std::size_t> dimsOut = dims;
                for (std::size_t i = 0; i < nbDims; ++i) {
                    if (boolDist(gen)) {
                        dims0[i] = 1;
                    }
                    if (boolDist(gen)) {
                        dims1[i] = 1;
                    }
                    dimsOut[i] = (dims0[i] == 1) ? dims1[i] : dims0[i];
                }

                // create arrays and fill them with random values
                std::size_t array0_size = std::accumulate(dims0.cbegin(), dims0.cend(), std::size_t(1), std::multiplies<std::size_t>());
                std::size_t array1_size = std::accumulate(dims1.cbegin(), dims1.cend(), std::size_t(1), std::multiplies<std::size_t>());
                float* array0 = new float[array0_size];
                float* array1 = new float[array1_size];

                for (std::size_t i = 0; i < array0_size; ++i) {
                    array0[i] = valueDist(gen);
                }
                for (std::size_t i = 0; i < array1_size; ++i) {
                    array1[i] = valueDist(gen);
                }
                // input0 CUDA
                float* array0_d, *array1_d;
                std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
                T0_cuda->setDataType(DataType::Float32);
                T0_cuda->setBackend("cuda");
                T0_cuda->resize(dims0);
                op_cuda->associateInput(0, T0_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * array0_size);
                cudaMemcpy(array0_d, array0, sizeof(float) * array0_size, cudaMemcpyHostToDevice);
                T0_cuda->getImpl()->setRawPtr(array0_d, array0_size);

                // input0 CPU
                std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
                T0_cpu->setDataType(DataType::Float32);
                T0_cpu->setBackend("cpu");
                op_cpu->associateInput(0,T0_cpu);
                T0_cpu->resize(dims0);
                T0_cpu -> getImpl() -> setRawPtr(array0, array0_size);

                // input1 CUDA
                std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>();
                T1_cuda->setDataType(DataType::Float32);
                T1_cuda->setBackend("cuda");
                T1_cuda->resize(dims1);
                op_cuda->associateInput(1, T1_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array1_d), sizeof(float) * array1_size);
                cudaMemcpy(array1_d, array1, sizeof(float) * array1_size, cudaMemcpyHostToDevice);
                T1_cuda->getImpl()->setRawPtr(array1_d, array1_size);

                // input1
                std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
                T1_cpu->setDataType(DataType::Float32);
                T1_cpu->setBackend("cpu");
                T1_cpu->resize(dims1);
                op_cpu -> associateInput(1,T1_cpu);
                T1_cpu -> getImpl() -> setRawPtr(array1, array1_size);

                op_cuda->forwardDims();
                start = std::chrono::system_clock::now();
                myPowCUDA->forward();
                end = std::chrono::system_clock::now();
                duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

                op_cpu->forwardDims();
                myPowCPU->forward();

                std::shared_ptr<Tensor> outputFallback;
                const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
                REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

                delete[] array0;
                delete[] array1;
                cudaFree(array0_d);
                cudaFree(array1_d);

                const std::size_t nb_elements = std::accumulate(dimsOut.cbegin(), dimsOut.cend(), std::size_t(1), std::multiplies<std::size_t>());
                number_of_operation += nb_elements;
            }
            Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
            Log::info("Total time: {}μs\n", duration.count());
        }
        SECTION("+1-D Tensor / 1-D Tensor") {
            // Create Pow Operator
            std::shared_ptr<Node> myPowCUDA = Pow();
            auto op_cuda = std::static_pointer_cast<OperatorTensor>(myPowCUDA-> getOperator());
            op_cuda->setDataType(DataType::Float32);
            op_cuda->setBackend("cuda");
            std::shared_ptr<Node> myPowCPU = Pow();
            auto op_cpu = std::static_pointer_cast<OperatorTensor>(myPowCPU-> getOperator());
            op_cpu->setDataType(DataType::Float32);
            op_cpu->setBackend("cpu");

            std::size_t number_of_operation = 0;
            std::uniform_int_distribution<std::size_t> nbRemovedDimsDist(std::size_t(1), std::size_t(3));

            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                // generate 2 random Tensors
                // handle dimensions
                constexpr std::size_t nbDims = 4;
                std::vector<std::size_t> dims0(4);
                for (std::size_t i = 0; i < nbDims; ++i) {
                    dims0[i] = dimSizeDist(gen);
                }
                std::vector<std::size_t> dimsOut = dims0;
                std::vector<std::size_t> dims1 = dims0;
                for (std::size_t i = 0; i < nbDims; ++i) {
                    if (boolDist(gen)) {
                        dims1[i] = 1;
                    }
                }
                dims1.erase(dims1.cbegin(), dims1.cbegin() + nbRemovedDimsDist(gen));

                // create arrays and fill them with random values
                std::size_t array0_size = std::accumulate(dims0.cbegin(), dims0.cend(), std::size_t(1), std::multiplies<std::size_t>());
                float* array0 = new float[array0_size];
                std::size_t array1_size = std::accumulate(dims1.cbegin(), dims1.cend(), std::size_t(1), std::multiplies<std::size_t>());
                float* array1 = new float[array1_size];

                for (std::size_t i = 0; i < array0_size; ++i) {
                    array0[i] = valueDist(gen);
                }
                for (std::size_t i = 0; i < array1_size; ++i) {
                    array1[i] = valueDist(gen);
                }

                // input0 CUDA
                float* array0_d, *array1_d;
                std::shared_ptr<Tensor> T0_cuda = std::make_shared<Tensor>();
                T0_cuda->setDataType(DataType::Float32);
                T0_cuda->setBackend("cuda");
                T0_cuda->resize(dims0);
                op_cuda->associateInput(0, T0_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array0_d), sizeof(float) * array0_size);
                cudaMemcpy(array0_d, array0, sizeof(float) * array0_size, cudaMemcpyHostToDevice);
                T0_cuda->getImpl()->setRawPtr(array0_d, array0_size);

                // input0 CPU
                std::shared_ptr<Tensor> T0_cpu = std::make_shared<Tensor>();
                T0_cpu->setDataType(DataType::Float32);
                T0_cpu->setBackend("cpu");
                op_cpu->associateInput(0,T0_cpu);
                T0_cpu->resize(dims0);
                T0_cpu -> getImpl() -> setRawPtr(array0, array0_size);

                // input1 CUDA
                std::shared_ptr<Tensor> T1_cuda = std::make_shared<Tensor>();
                T1_cuda->setDataType(DataType::Float32);
                T1_cuda->setBackend("cuda");
                T1_cuda->resize(dims1);
                op_cuda->associateInput(1, T1_cuda);
                cudaMalloc(reinterpret_cast<void **>(&array1_d), sizeof(float) * array1_size);
                cudaMemcpy(array1_d, array1, sizeof(float) * array1_size, cudaMemcpyHostToDevice);
                T1_cuda->getImpl()->setRawPtr(array1_d, array1_size);

                // input1
                std::shared_ptr<Tensor> T1_cpu = std::make_shared<Tensor>();
                T1_cpu->setDataType(DataType::Float32);
                T1_cpu->setBackend("cpu");
                T1_cpu->resize(dims1);
                op_cpu -> associateInput(1,T1_cpu);
                T1_cpu -> getImpl() -> setRawPtr(array1, array1_size);

                op_cuda->forwardDims();
                start = std::chrono::system_clock::now();
                myPowCUDA->forward();
                end = std::chrono::system_clock::now();
                duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

                op_cpu->forwardDims();
                myPowCPU->forward();

                std::shared_ptr<Tensor> outputFallback;
                const auto& cudaOutput = op_cuda->getOutput(0)->refCastFrom(outputFallback, *op_cpu->getOutput(0));
                REQUIRE(approxEq<float>(cudaOutput, *(op_cpu->getOutput(0))));

                delete[] array0;
                delete[] array1;
                cudaFree(array0_d);
                cudaFree(array1_d);

                const std::size_t nb_elements = std::accumulate(dimsOut.cbegin(), dimsOut.cend(), std::size_t(1), std::multiplies<std::size_t>());
                number_of_operation += nb_elements;
            }
            Log::info("Number of elements over time spent: {}\n", number_of_operation / duration.count());
            Log::info("Total time: {}μs\n", duration.count());
        }
    }
}
} // namespace Aidge
